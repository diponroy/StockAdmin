﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class Supplier : IPrimaryKeyTrack, IAddTrack, IReplaceTrack, IDateOfCreationTrack
    {
        public long Id { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }


        public DateTime DateOfCreation { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual User Replacer { get; set; }
        public virtual ICollection<MarketQuotationInspection> MarketQuotationInspection { get; set; }
        public virtual ICollection<Purchase> Purchases { get; set; } 
    }
}
