﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class Sale: IPrimaryKeyTrack, IAddTrack, IDateOfCreationTrack
    {
        public long Id { get; set; }


        public long CustomerId { get; set; }


        public DateTime DateOfCreation { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<SoldProducts> SoldProducts { get; set; } 
    }
}
