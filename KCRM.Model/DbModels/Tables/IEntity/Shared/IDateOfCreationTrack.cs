﻿using System;

namespace KCRM.Model.DbModels.Tables.IEntity.Shared
{
    public interface IDateOfCreationTrack
    {
        DateTime DateOfCreation { get; set; }
    }
}
