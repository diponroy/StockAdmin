﻿using System;

namespace KCRM.Model.DbModels.Tables.IEntity.Shared
{
    public interface IAddTrack
    {    
        DateTime? AddedDateTime { get; set; }
        long? AddedByUserId { get; set; }
    }
}
