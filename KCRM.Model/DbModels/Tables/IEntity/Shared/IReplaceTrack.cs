﻿using System;

namespace KCRM.Model.DbModels.Tables.IEntity.Shared
{
    public interface IReplaceTrack
    {
        DateTime? ReplacedDateTime { get; set; }
        long? ReplacedByUserId { get; set; }
    }
}
