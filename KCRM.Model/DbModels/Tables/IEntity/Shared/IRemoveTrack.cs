﻿namespace KCRM.Model.DbModels.Tables.IEntity.Shared
{
    public interface IRemoveTrack : IReplaceTrack
    {
        bool? IsRemoved { get; set; }
    }
}
