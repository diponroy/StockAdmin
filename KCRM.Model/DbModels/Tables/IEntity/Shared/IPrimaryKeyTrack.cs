﻿namespace KCRM.Model.DbModels.Tables.IEntity.Shared
{
    public interface IPrimaryKeyTrack
    {
        long Id { get; set; }
    }
}
