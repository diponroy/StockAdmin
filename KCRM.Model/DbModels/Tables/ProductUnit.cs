﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class ProductUnit : IPrimaryKeyTrack, IAddTrack, IReplaceTrack
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual User  Adder { get; set; }
        public virtual User Replacer { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}