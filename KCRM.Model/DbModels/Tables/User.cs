﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class User : IPrimaryKeyTrack, IAddTrack, IRemoveTrack, IDateOfCreationTrack
    {
        public long Id { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public UserTypeEnum UserType { get; set; }
        public string Address { get; set; }


        public DateTime DateOfCreation { get; set; }
        public bool? IsRemoved { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual User Replacer { get; set; }

        public virtual ICollection<User> AddedUsers { get; set; }   
        public virtual ICollection<User> ReplacedUsers { get; set; }

        public virtual ICollection<UserPermission> Permissions { get; set; }
        public virtual ICollection<UserPermission> AddedPermissions { get; set; }
        public virtual ICollection<UserPermission> ReplacedPermissions { get; set; }

        public virtual ICollection<Customer> AddedCustomers { get; set; }
        public virtual ICollection<Customer> ReplacedCustomers { get; set; }

        public virtual ICollection<CustomerContactPerson> AddedCustomerContactPersons { get; set; }
        public virtual ICollection<CustomerContactPerson> ReplacedCustomerContactPersons { get; set; }

        public virtual ICollection<ProductUnit> AddedProductUnits { get; set; }
        public virtual ICollection<ProductUnit> ReplacedProductUnits { get; set; }


        public virtual ICollection<Product> AddedProducts { get; set; }
        public virtual ICollection<Product> ReplacedProducts { get; set; }

        public virtual ICollection<Supplier> AddedSuppliers { get; set; }
        public virtual ICollection<Supplier> ReplacedSuppliers { get; set; }

        public virtual ICollection<MarketQuotation> CollectedMarketQuotations { get; set; }

        public virtual ICollection<MarketQuotation> AddedMarketQuotations { get; set; }

        public virtual ICollection<ClientQuotation> AddedClientQuotations { get; set; }

        public virtual ICollection<Purchase> AddedPurchaces { get; set; }
        public virtual ICollection<Purchase> PurchasedPurchases { get; set; }

        public virtual ICollection<Sale> AddedSales { get; set; }

        public virtual ICollection<Stock> AddedStocks { get; set; }

        public virtual ICollection<MarketPriceList> AddedMarketPriceLists { get; set; }
        public virtual ICollection<MarketPriceList> ReplacedMarketPriceLists { get; set; }

        public virtual ICollection<StockPriceList> AddedStockPriceLists { get; set; }
        public virtual ICollection<StockPriceList> ReplacedStockPriceLists { get; set; }
    }
}
