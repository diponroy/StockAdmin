﻿using System;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class UserPermission : IPrimaryKeyTrack, IAddTrack, IRemoveTrack
    {
        public long Id { get; set; }


        public long UserId { get; set; }
        public PermissionEmun Permission { get; set; }


        public bool? IsRemoved { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual User User { get; set; }
        public virtual User Adder { get; set; }
        public virtual User Replacer { get; set; }
    }
}
