﻿using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class StockConsumptions : IPrimaryKeyTrack
    {
        public long Id { get; set; }


        public long SoldProductId { get; set; }
        public long Quantity { get; set; }


        public virtual SoldProducts SoldProducts { get; set; }
    }
}
