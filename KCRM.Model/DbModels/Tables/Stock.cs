﻿using System;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class Stock : IPrimaryKeyTrack, IAddTrack, IDateOfCreationTrack
    {
        public long Id { get; set; }


        public long ProductId { get; set; }
        public long Quantity { get; set; }


        public DateTime DateOfCreation { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual Product Product { get; set; }
    }
}
