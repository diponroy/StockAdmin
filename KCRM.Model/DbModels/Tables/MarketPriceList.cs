﻿using System;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class MarketPriceList : IPrimaryKeyTrack,IAddTrack,IReplaceTrack
    {
        public long Id { get; set; }


        public long ProductId { get; set; }
        public decimal Price { get; set; }


        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual User Replacer { get; set; }
        public virtual Product Product { get; set; }
    }
}
