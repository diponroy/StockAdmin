﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class ClientQuotation : IPrimaryKeyTrack, IAddTrack, IDateOfCreationTrack
    {
        public long Id { get; set; }

        public long CustomerId { get; set; }
        public long CustomerContactPersonId { get; set; }
        public long MarketQuotationId { get; set; }


        public DateTime DateOfCreation { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual MarketQuotation MarketQuotation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual CustomerContactPerson CustomerContactPerson { get; set; }
        public virtual ICollection<ClientQuotationOrder> ClientQuotationOrders { get; set; }
    }
}
