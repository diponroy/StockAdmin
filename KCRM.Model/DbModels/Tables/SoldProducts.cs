﻿using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class SoldProducts : IPrimaryKeyTrack
    {
        public long Id { get; set; }


        public long SaleId { get; set; }
        public long ProductId { get; set; }
        public bool HasStockConsumption { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }


        public virtual Sale Sale { get; set; }
        public virtual Product Product { get; set; }

        public virtual ICollection<StockConsumptions> StockConsumptions { get; set; }
    }
}