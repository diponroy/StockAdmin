﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class CustomerContactPerson : IPrimaryKeyTrack, IAddTrack, IRemoveTrack
    {
        public long Id { get; set; }


        public long CustomerId { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }


        public bool? IsRemoved { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual User Adder { get; set; }
        public virtual User Replacer { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ICollection<ClientQuotation> ClientQuotations { get; set; }
    }
}
