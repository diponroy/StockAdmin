﻿using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class ClientQuotationOrder : IPrimaryKeyTrack
    {
        public long Id { get; set; }

        public long ClientQuotationId { get; set; }
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Vat { get; set; }

        public virtual ClientQuotation ClientQuotation { get; set; }
        public virtual Product Product { get; set; }

    }
}
