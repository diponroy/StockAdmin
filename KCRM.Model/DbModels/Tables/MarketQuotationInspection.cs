﻿using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class MarketQuotationInspection : IPrimaryKeyTrack
    {
        public long Id { get; set; }


        public long MarketQuotationId { get; set; }
        public long ProductId { get; set; }
        public long SupplierId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }


        public virtual MarketQuotation MarketQuotation { get; set; }
        public virtual Product Product { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}
