﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class Product : IPrimaryKeyTrack, IAddTrack, IReplaceTrack, IDateOfCreationTrack
    {
        public long Id { get; set; }


        public string Name { get; set; }
        public ProductCategoryEnum Category { get; set; }
        public long ProductUnitId { get; set; }
        public string Description { get; set; }


        public DateTime DateOfCreation { get; set; }
        public DateTime? AddedDateTime { get; set; }
        public long? AddedByUserId { get; set; }
        public DateTime? ReplacedDateTime { get; set; }
        public long? ReplacedByUserId { get; set; }


        public virtual ProductUnit Unit { get; set; }
        public virtual User Adder { get; set; }
        public virtual User Replacer { get; set; }
        public virtual ICollection<MarketQuotationInspection> MarketQuotationInspection { get; set; }
        public virtual ICollection<ClientQuotationOrder> ClientQuotationOrders { get; set; }
        public virtual ICollection<PurchasedProducts> PurchasedProducts { get; set; }
        public virtual ICollection<SoldProducts> SoldProducts { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }
        public virtual ICollection<MarketPriceList> MarketPriceLists { get; set; }
        public virtual ICollection<StockPriceList> StockPriceLists { get; set; } 
    }
}
