﻿namespace KCRM.Model.DbModels.Tables.Enums
{
    /*remember: don't change the assigned values, 
    * add new if needed with individul value*/
    public enum ProductCategoryEnum
    {
        Safety = 1,             //1
        HardwareTools = 2,      //2
        Electric = 3            //3
    }
}
