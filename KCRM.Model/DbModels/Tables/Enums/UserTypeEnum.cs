﻿namespace KCRM.Model.DbModels.Tables.Enums
{
    /*remember: don't change the assigned values, 
     * add new if needed with individul value*/
    public enum UserTypeEnum
    {
        Administrator = 1,      //1
        ApplicationUser = 2     //2
    }
}
