﻿namespace KCRM.Model.DbModels.Tables.Enums
{

    /*remember: don't change the assigned values, 
    * add new if needed with individul value*/

    public enum PermissionEmun
    {
        /*Forms*/
        UserSetup = 1,              //1
        CustomerSetup = 2,          //2  
        SupplierSetup = 3,          //3      
        UserPermissionSetup = 4,    //4
        ProductSetup = 5,           //5
        ClientQuotation = 6,        //6
        MarketQuotation = 7,        //7		
		Purchase = 8,               //8
        Stock = 9,                  //9
        MarketPrice = 10,           //10
        StockPrice = 11,            //11	
        Sale = 12,                  //12
        
        /*Reports*/
        MarketPriceReport = 13,      //13      
        StockPriceReport = 14,       //14 
        CurrentStockReport = 15,            //15
        SalesReport = 16,            //16
        PurchaseReport = 17,         //17
        StockEntryReport= 18,         //18
        MarketQuotationReport = 19,  //19
		ClientQuotationReport = 20  //20

    }
}
