﻿using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.DbModels.Tables
{
    public class PurchasedProducts : IPrimaryKeyTrack
    {
        public long Id { get; set; }


        public long PurchaseId { get; set; }
        public long ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }


        public virtual Purchase Purchase { get; set; }
        public virtual Product Product { get; set; }
    }
}
