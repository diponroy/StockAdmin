﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.DbModels.Tables.Mimics
{
    public class ProductCategory
    {
        public string Name { get; set; }
        public ProductCategoryEnum ProductCategoryEnum { get; set; }
        public int Value { get; set; }

        public ProductCategory()
        {
            
        }
        public ProductCategory(ProductCategoryEnum productTypeEnum, string name = null)
        {
            ProductCategoryEnum = productTypeEnum;
            Value = (int)productTypeEnum;
            Name = (String.IsNullOrEmpty(name)) ? productTypeEnum.ToString() : name.Trim();
        }
    }
}