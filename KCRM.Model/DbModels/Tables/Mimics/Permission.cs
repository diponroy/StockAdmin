﻿using System;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.DbModels.Tables.Mimics
{
    public class Permission
    {
        public string Name { get; set; }
        public PermissionEmun PermissionEmun { get; set; }
        public int Value { get; set; }

        public Permission()
        {
            
        }
        public Permission(PermissionEmun permissionEmunEnum, string name = null)
        {
            PermissionEmun = permissionEmunEnum;
            Value = (int)permissionEmunEnum;
            Name = (String.IsNullOrEmpty(name)) ? permissionEmunEnum.ToString() : name.Trim();
        }
    }
}
