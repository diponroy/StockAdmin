﻿using System;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.DbModels.Tables.Mimics
{
    public class UserType
    {
        public string Name { get; set; }
        public UserTypeEnum UserTypeEnum { get; set; }
        public int Value { get; set; }

        public UserType()
        {
            
        }
        public UserType(UserTypeEnum userTypeEnum, string name = null)
        {
            UserTypeEnum = userTypeEnum;
            Value = (int)userTypeEnum;
            Name = (String.IsNullOrEmpty(name)) ? userTypeEnum.ToString() : name.Trim();
        }
    }
}
