﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.DbModels.Views
{
    public class CurrentStock
    {
        public long ProductId { get; set; }

        public ProductCategoryEnum Category { get; set; } 
        public String Name { get; set; }
        public long UnitId { get; set; }
        public String UnitName { get; set; }
        public long AvailableQutity { get; set; }
    }
}
