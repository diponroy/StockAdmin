﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCRM.Model.Models
{
    public class MarketQuotationModel
    {
        public long Id { get; set; }
        public string QuotationCode { get; set; }
        public long CustomerId { get; set; }
        public string Customer { get; set; }
        public long PriceInspectorUserId { get; set; }
        public string PriceInspector { get; set; }
        public DateTime DateOfCreation { get; set; }
        public virtual UserModel PriceInspectorUser { get; set; }
        public IEnumerable<MarketQuotationInspectionModel> Inspections { get; set; } 
    }
}
