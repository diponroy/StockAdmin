﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.Models
{
    [Serializable]
    public class MenuSessionModel
    {
        private readonly List<PermissionEmun> _permissions;

        /*Setup menus*/
        public bool SetUpMenu { get; set; }
        public bool UserSetup { get; set; }
        public bool UserPermissionSetup { get; set; }
        public bool CustomerSetup { get; set; }
        public bool ProductUnitSetup { get; set; }
        public bool ProductSetup { get; set; }
        public bool SupplierSetup { get; set; }


        /*Qutation menus*/
        public bool QutationMenu { get; set; }
        public bool MarketQutation { get; set; }
        public bool ClientQutation { get; set; }


        /*Purchase menus*/
        public bool PurchaseMenu { get; set; }
        public bool Purchase { get; set; }


        /*Stock menus*/
        public bool StockMenu { get; set; }
        public bool Stock { get; set; }


        /*Sales menus*/
        public bool SalesMenu { get; set; }
        public bool Sale { get; set; }


        /*Price List menus*/
        public bool PriceListMenu { get; set; }
        public bool MarketPrice { get; set; }
        public bool StockPrice { get; set; }


        /*Report menus*/
        public bool ReportMenu { get; set; }
        public bool MarketQuotationReport { get; set; }
        public bool ClientQuotationReport { get; set; }
        public bool PurchaseReport { get; set; }
        public bool StockEntryReport { get; set; }
        public bool CurrentStockReport { get; set; }
        public bool SalesReport { get; set; }
        public bool MarketPriceReport { get; set; }
        public bool StockPriceReport { get; set; }

        public MenuSessionModel(List<PermissionEmun> permissions)
        {
            _permissions = permissions ?? new List<PermissionEmun>();
            InitializeEndMenus();
            InitializeSubMenus();
        }

        public bool Has(PermissionEmun permission)
        {
            return _permissions.Contains(permission);
        }

        private void InitializeEndMenus()
        {
            /*Setup menus*/
            UserSetup = Has(PermissionEmun.UserSetup);
            UserPermissionSetup = Has(PermissionEmun.UserPermissionSetup);
            CustomerSetup = Has(PermissionEmun.CustomerSetup);
            ProductUnitSetup = Has(PermissionEmun.ProductSetup);
            ProductSetup = Has(PermissionEmun.ProductSetup);
            SupplierSetup = Has(PermissionEmun.SupplierSetup);

            /*Qutation menus*/
            MarketQutation = Has(PermissionEmun.MarketQuotation);
            ClientQutation = Has(PermissionEmun.ClientQuotation);

            /*Purchase menus*/
            Purchase = Has(PermissionEmun.Purchase);

            /*Stock menus*/
            Stock = Has(PermissionEmun.Stock);

            /*Sales menus*/
            Sale = Has(PermissionEmun.Sale);

            /*Price List menus*/
            MarketPrice = Has(PermissionEmun.MarketPrice);
            StockPrice = Has(PermissionEmun.StockPrice);

            /*Report menus*/
            MarketQuotationReport = Has(PermissionEmun.MarketQuotationReport);
            ClientQuotationReport = Has(PermissionEmun.ClientQuotationReport);
            PurchaseReport = Has(PermissionEmun.PurchaseReport);
            StockEntryReport = Has(PermissionEmun.StockEntryReport);
            CurrentStockReport = Has(PermissionEmun.CurrentStockReport);
            SalesReport = Has(PermissionEmun.SalesReport);
            MarketPriceReport = Has(PermissionEmun.MarketPriceReport);
            StockPriceReport = Has(PermissionEmun.StockPriceReport);
        }


        private void InitializeSubMenus()
        {
            /*Setup menus*/
            SetUpMenu = UserSetup 
                        || UserPermissionSetup 
                        || CustomerSetup 
                        || ProductUnitSetup 
                        || ProductSetup 
                        || SupplierSetup 
                        || ClientQutation;


            /*Qutation menus*/
            QutationMenu = MarketQutation 
                            || ClientQutation;

            /*Purchase menus*/
            PurchaseMenu = Purchase;

            /*Stock menus*/
            StockMenu = Stock;

            /*Sales menus*/
            SalesMenu = Sale;

            /*Price List menus*/
            PriceListMenu = MarketPrice
                            || StockPrice;

            /*Report menus*/
            ReportMenu = MarketQuotationReport
                         || ClientQuotationReport
                         || PurchaseReport
                         || StockEntryReport
                         || CurrentStockReport
                         || SalesReport
                         || MarketPriceReport
                         || StockPriceReport;
        }    
    }
}
