﻿using System;
using System.Collections.Generic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.Models
{
    public class UserPermissionModel
    {
        public long UserId { get; set; }
        public List<int> Permissions{ get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public long? UpddatedByUserId { get; set; }
    }
}
