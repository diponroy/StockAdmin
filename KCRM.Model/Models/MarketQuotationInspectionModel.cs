﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models
{
    public class MarketQuotationInspectionModel
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string ProductUnit { get; set; }
        public long ProductQuantity { get; set; }
        public decimal ProductPrice { get; set; }
        public string SupplierName { get; set; }
    }
}
