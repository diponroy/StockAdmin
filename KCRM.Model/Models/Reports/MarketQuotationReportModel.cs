﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models.Reports
{
    public class MarketQuotationReportModel
    {
        public long QuotationId { get; set; }
        public long CustomerId { get; set; }
        public long InspectorId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public DateTime ReportDate { get; set; }

        public ICollection<MarketQuotationModel> Quotations { get; set; }
    }
}
