﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Model.DbModels.Views;

namespace KCRM.Model.Models.Reports
{
    public class MarketPriceReportModel
    {
        public int ProductCategory { get; set; }
        public long ProductId { get; set; }
        public DateTime ReportDate { get; set; }

        public ICollection<ProductMarketPrice> MarketPrices { get; set; } 
    }
}
