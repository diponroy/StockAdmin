﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models.Reports
{
    public class PurchaseReportModel
    {
        public long SupplierId { get; set; }
        public long CustomerId { get; set; }
        public long UserId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime ReportDate { get; set; }

        public ICollection<PurchaseModel> Purchases { get; set; } 
    }
}
