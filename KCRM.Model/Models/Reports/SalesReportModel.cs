﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Model.Models.Reports
{
    public class SalesReportModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long CustomerId { get; set; }
        public DateTime ReportDate { get; set; }

        public ICollection<SaleModel> Sales { get; set; } 
    }
}
