﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models.Reports
{
    public class StockEntryReportModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int ProductCategoryId { get; set; }
        public long ProductId { get; set; }
        public DateTime ReportDate { get; set; }

        public ICollection<StockModel> Stocks { get; set; } 
    }
}
