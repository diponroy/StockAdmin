﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Model.DbModels.Views;

namespace KCRM.Model.Models.Reports
{
     public class StockReportModel
    {
        public int CategoryId { get; set; }
        public long ProductId { get; set; }
        public DateTime ReportDate { get; set; }


        public ICollection<CurrentStock> CurrentStocks { get; set; } 
    }
}
