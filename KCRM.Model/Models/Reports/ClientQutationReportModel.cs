﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models.Reports
{
    public class ClientQutationReportModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long CustomerId { get; set; }
        public long ContactPersonId { get; set; }
        public long PriceInspectorId { get; set; }
        public long MarketQuotationId { get; set; }
        public DateTime ReportDate { get; set; }

        public virtual ClientQuotationModel ClientQuotation { get; set; }
        public virtual ICollection<ClientQuotationModel> ClientQuotations { get; set; }
    }
}
