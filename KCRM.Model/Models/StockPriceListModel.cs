﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCRM.Model.Models
{
    public class StockPriceListModel
    {
        public long Id { get; set; }
        public decimal Price { get; set; }
        public string ProductName { get; set; }
    }
}
