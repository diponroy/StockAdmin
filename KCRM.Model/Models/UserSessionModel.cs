﻿using System;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.Models
{
    [Serializable]
    public class UserSessionModel
    {
        public long Id { get; set; }
        public string LoginName { get; set; }
        public UserTypeEnum UserType { get; set; }
    }
}