﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.Models
{
    public class ProductModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ProductCategoryEnum Category { get; set; }
        public string CategoryName { get; set; }
        public string UnitName { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime DateOfCreation { get; set; }
    }
}
