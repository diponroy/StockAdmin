﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Model.Models
{
    public class SaleModel
    {
        public long CustomerId { get; set; }
        public DateTime DateOfCreation { get; set; }

        public virtual CustomerModel Customer { get; set; }
        public virtual ICollection<SoldProductModel> SoldProducts { get; set; } 
    }
}
