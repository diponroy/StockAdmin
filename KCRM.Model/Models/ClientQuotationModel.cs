﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Model.Models
{
    public class ClientQuotationModel
    {
        public DateTime DateOfCreation { get; set; }
        public virtual MarketQuotationModel MarketQuotation { get; set; }
        public virtual CustomerModel Customer { get; set; }
        public virtual CustomerContactPersonModel CustomerContactPerson { get; set; }
        public virtual List<ClientQuotationOrderModel> ClientQuotationOrders { get; set; }
    }
}
