﻿using System.Collections.Generic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.Models
{
    public class SoldProductModel : IPrimaryKeyTrack
    {
        public long Id { get; set; }
        public long ProductId { get; set; }
        public bool HasStockConsumption { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public virtual ProductModel Product { get; set; }
        public virtual ICollection<StockConsumptionModel> StockConsumptions { get; set; }
    }
}