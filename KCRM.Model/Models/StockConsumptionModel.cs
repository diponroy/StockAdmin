﻿using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Model.Models
{
    public class StockConsumptionModel
    {
        public long SoldProductId { get; set; }
        public long Quantity { get; set; }
    }
}
