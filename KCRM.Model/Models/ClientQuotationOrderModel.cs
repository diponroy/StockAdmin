﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models
{
    public class ClientQuotationOrderModel
    {
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Vat { get; set; }
        public decimal Total { get; set; }
        public virtual ProductModel Product { get; set; }
    }
}
