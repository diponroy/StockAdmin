﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Model.Models
{
    public class UserModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public UserTypeEnum UserType { get; set; }
        public string UserTypeString { get; set; }
        public string Address { get; set; }
        public string DateOfCreation { get; set; }
        public string FullName { get; set; }
        public string FullNameWithLoginName { get; set; }
    }
}
