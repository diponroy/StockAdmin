﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Model.Models
{
    public class PurchaseModel
    {
        public long Id { get; set; }
        public string Supplier { get; set; }
        public string Customer { get; set; }
        public string Purchaser { get; set; }
        public DateTime DateOfCreation { get; set; }
        public IEnumerable<ProductModel> Products { get; set; }
    }
}
