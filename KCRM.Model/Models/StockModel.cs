﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KCRM.Model.Models
{
    
    public class StockModel
    {
        public long Quantity { get; set; }
        public DateTime DateOfCreation { get; set; }
        public virtual ProductModel Product { get; set; }
    }
}
