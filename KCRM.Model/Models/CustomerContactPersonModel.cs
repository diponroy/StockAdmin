﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCRM.Model.Models
{
    public class CustomerContactPersonModel
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
    }
}
