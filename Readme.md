# Stock Admin #
With the application, user will be able to manage 

1. Market quotation for a particular client
1. Client quotation as a proposal to a particular client
1. Purchase product from particular supplier
1. Sales to a particular client
1. Stock and price of products
1. Market current price list of products 
1. Reports view and PDF download


## Db Connection String: ##
Change connection string at Web project. Web.config

    <connectionStrings>
    <add name="DbStockAdmin" connectionString="Data Source=(local);Initial Catalog=StockAdmin;Integrated Security=True" providerName="System.Data.SqlClient" />
    </connectionStrings>
same connection string at Test project. App.config (only to run tests and create local db)

## Module Specification: ##
- **Setup/Settings**
	1. User Setup
	1. Permission Setup
	1. Customer Setup
	1. Product Unit Setup
	1. Product Setup
	1. Supplier Setup
- **Quotation Setup**
	1. Market Quotation
	1. Client Quotation
- **Purchases**
	1. Purchase
- **Sales**
	1. Sale
- **Stocks**
	1. Stock
- **Price Lists**
	1. Stock Price
	1. Market Price
- **Reports**
	1. Market Quotation Report
	1. Client Quotation Report
	1. Purchase Report
	1. Stock Entry Report
	1. Current Stock Report
	1. Sales Report
	1. Market Price Report
	1. Stock Price Report




