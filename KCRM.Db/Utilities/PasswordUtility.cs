﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace KCRM.Logic.Utilities
{
    internal class PasswordUtility
    {
        public static string Encode(string originalPassword)
        {
            Byte[] encodedBytes = null;
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] originalBytes = Encoding.Default.GetBytes(originalPassword);
            encodedBytes = md5.ComputeHash(originalBytes);
            string encodedPassword = BitConverter.ToString(encodedBytes);
            return encodedPassword.Replace("-", "");
        }
    }
}
