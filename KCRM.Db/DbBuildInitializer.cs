﻿using System.Linq;
using KCRM.Db.Contexts;

namespace KCRM.Db
{
    public class DbBuildInitializer
    {
        public void DropCreateDatabaseAlwaysWithSeedData()
        {
            IStockAdminContext dbContext = new BuildContext();
            if (dbContext.Database.Exists())
            {
                dbContext.Database.Delete();
            }

            using (dbContext = new BuildContext())
            {
                LoadTables(dbContext);
            }
        }

        public void CreateDatabaseIfNotExistsWithSeedData()
        {
            using (IStockAdminContext dbContext = new BuildContext())
            {
                LoadTables(dbContext);
            }
        }
        

        private void LoadTables(IStockAdminContext dbContext)
        {
            var users = dbContext.Users.ToList();
        }
    }
}
