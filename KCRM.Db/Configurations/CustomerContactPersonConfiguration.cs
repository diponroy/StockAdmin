﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class CustomerContactPersonConfiguration: EntityTypeConfiguration<CustomerContactPerson>
    {
        public CustomerContactPersonConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Name)
                .IsRequired();

            Property(x => x.IsRemoved)
                .IsRequired();

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.Customer)
                .WithMany(l => l.ContactPersons)
                .HasForeignKey(x => x.CustomerId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedCustomerContactPersons)
                .HasForeignKey(x => x.AddedByUserId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Replacer)
                .WithMany(l => l.ReplacedCustomerContactPersons)
                .HasForeignKey(x => x.ReplacedByUserId);
        }
    }
}