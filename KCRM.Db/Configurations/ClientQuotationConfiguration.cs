﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class ClientQuotationConfiguration : EntityTypeConfiguration<ClientQuotation>
    {
        public ClientQuotationConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.MarketQuotation)
                .WithMany(l => l.ClientQuotations)
                .HasForeignKey(x => x.MarketQuotationId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Customer)
                .WithMany(l => l.ClientQuotations)
                .HasForeignKey(x => x.CustomerId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.CustomerContactPerson)
                .WithMany(l => l.ClientQuotations)
                .HasForeignKey(x => x.CustomerContactPersonId);

            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedClientQuotations)
                .HasForeignKey(x => x.AddedByUserId);
        }
    }
}