﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Net.Mail;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class MarketQuotationInspectionConfiguration : EntityTypeConfiguration<MarketQuotationInspection>
    {
        public MarketQuotationInspectionConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(x => x.MarketQuotation)
                .WithMany(l => l.Inspections)
                .HasForeignKey(x => x.MarketQuotationId);

            HasRequired(x => x.Product)
                .WithMany(l => l.MarketQuotationInspection)
                .HasForeignKey(x => x.ProductId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Supplier)
                .WithMany(l => l.MarketQuotationInspection)
                .HasForeignKey(x => x.SupplierId)
                .WillCascadeOnDelete(false);
        }
    }
}