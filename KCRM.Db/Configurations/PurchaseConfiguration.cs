﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class PurchaseConfiguration : EntityTypeConfiguration<Purchase>
    {
        public PurchaseConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.SupplierId)
                .IsRequired();

            Property(x => x.CustomerId)
                .IsRequired();

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.Supplier)
                .WithMany(m => m.Purchases)
                .HasForeignKey(f => f.SupplierId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Customer)
                .WithMany(m => m.Purchases)
                .HasForeignKey(f => f.CustomerId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Adder)
                .WithMany(m => m.AddedPurchaces)
                .HasForeignKey(f => f.AddedByUserId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Purchaser)
                .WithMany(m => m.PurchasedPurchases)
                .HasForeignKey(f => f.PurchaseByUserId)
                .WillCascadeOnDelete(false);
        }
    }
}
