﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.UserType)
                .HasColumnName("UserType");

            Property(x => x.LoginName)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_LoginName", 1) { IsUnique = true }));

            Property(x => x.Email)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_Email", 2) { IsUnique = true }));

            Property(x => x.Password)
                .IsRequired();

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.IsRemoved)
                .IsRequired();

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedUsers)
                .HasForeignKey(x => x.AddedByUserId);

            HasOptional(x => x.Replacer)
                .WithMany(l => l.ReplacedUsers)
                .HasForeignKey(x => x.ReplacedByUserId);
        }
    }
}
