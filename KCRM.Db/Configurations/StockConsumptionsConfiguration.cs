﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class StockConsumptionsConfiguration : EntityTypeConfiguration<StockConsumptions>
    {
        public StockConsumptionsConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Quantity)
                .IsRequired();

            HasRequired(x=>x.SoldProducts)
                .WithMany(l=>l.StockConsumptions)
                .HasForeignKey(f=>f.SoldProductId)
                .WillCascadeOnDelete(false);
        }
    }
}
