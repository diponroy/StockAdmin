﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class SoldProductsConfiguration : EntityTypeConfiguration<SoldProducts>
    {
        public SoldProductsConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.HasStockConsumption)
                .IsRequired();

            Property(x => x.Quantity)
                .IsRequired();

            Property(x => x.Price)
                .IsRequired();

            HasRequired(x=>x.Sale)
                .WithMany(l=>l.SoldProducts)
                .HasForeignKey(f=>f.SaleId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.Product)
                .WithMany(l=>l.SoldProducts)
                .HasForeignKey(f=>f.ProductId)
                .WillCascadeOnDelete(false);
        }
    }
}