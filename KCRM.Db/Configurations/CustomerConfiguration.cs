﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class CustomerConfiguration: EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Name)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_Name", 1) { IsUnique = true }));

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");


            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedCustomers)
                .HasForeignKey(x => x.AddedByUserId);

            HasOptional(x => x.Replacer)
                .WithMany(l => l.ReplacedCustomers)
                .HasForeignKey(x => x.ReplacedByUserId);
        }
    }
}