﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Security.Cryptography.X509Certificates;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class PurchasedProductsConfiguration : EntityTypeConfiguration<PurchasedProducts>
    {
        public PurchasedProductsConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Quantity)
                .IsRequired();

            Property(x => x.Price)
                .IsRequired();

            HasRequired(x=>x.Purchase)
                .WithMany(m=>m.PurchasedProductList)
                .HasForeignKey(f=>f.PurchaseId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.Product)
                .WithMany(m=>m.PurchasedProducts)
                .HasForeignKey(f=>f.ProductId)
                .WillCascadeOnDelete(false);
        }
    }
}