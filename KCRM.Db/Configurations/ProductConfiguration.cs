﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Category)
                .HasColumnName("Category");

            Property(x => x.Name)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_Name", 1) { IsUnique = true }));

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();


            HasRequired(x => x.Unit)
                .WithMany(l => l.Products)
                .HasForeignKey(x => x.ProductUnitId);


            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedProducts)
                .HasForeignKey(x => x.AddedByUserId);

            HasOptional(x => x.Replacer)
                .WithMany(l => l.ReplacedProducts)
                .HasForeignKey(x => x.ReplacedByUserId);
        }
    }
}