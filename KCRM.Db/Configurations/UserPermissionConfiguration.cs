﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    class UserPermissionConfiguration : EntityTypeConfiguration<UserPermission>
    {
        public UserPermissionConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Permission)
                .HasColumnName("Permission");

            Property(x => x.IsRemoved)
                .IsRequired();

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.User)
                .WithMany(l => l.Permissions)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedPermissions)
                .HasForeignKey(x => x.AddedByUserId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Replacer)
                .WithMany(l => l.ReplacedPermissions)
                .HasForeignKey(x => x.ReplacedByUserId);
        }
    }
}
