﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class MarketPriceListConfiguration : EntityTypeConfiguration<MarketPriceList>
    {
        public MarketPriceListConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Price)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();

            HasRequired(x => x.Product)
                .WithMany(l => l.MarketPriceLists)
                .HasForeignKey(m => m.ProductId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.Adder)
                .WithMany(l=>l.AddedMarketPriceLists)
                .HasForeignKey(f=>f.AddedByUserId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Replacer)
                .WithMany(l=>l.ReplacedMarketPriceLists)
                .HasForeignKey(f=>f.ReplacedByUserId)
                .WillCascadeOnDelete(false);

        }
    }
}
