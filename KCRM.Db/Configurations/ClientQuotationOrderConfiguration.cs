﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class ClientQuotationOrderConfiguration : EntityTypeConfiguration<ClientQuotationOrder>
    {
        public ClientQuotationOrderConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Quantity)
                .IsRequired();

            Property(x => x.Price)
                .IsRequired();

            HasRequired(x => x.ClientQuotation)
                .WithMany(l=>l.ClientQuotationOrders)
                .HasForeignKey(x=>x.ClientQuotationId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Product)
                .WithMany(l => l.ClientQuotationOrders)
                .HasForeignKey(x => x.ProductId)
                .WillCascadeOnDelete(false);
        }
    }
}
