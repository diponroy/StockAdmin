﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class StockConfiguration : EntityTypeConfiguration<Stock>
    {
        public StockConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Quantity)
                .IsRequired();

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            HasRequired(x=>x.Adder)
                .WithMany(l => l.AddedStocks)
                .HasForeignKey(f=>f.AddedByUserId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.Product)
                .WithMany(l=>l.Stocks)
                .HasForeignKey(f=>f.ProductId)
                .WillCascadeOnDelete(false);
        }
    }
}
