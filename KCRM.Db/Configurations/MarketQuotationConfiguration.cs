﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class MarketQuotationConfiguration : EntityTypeConfiguration<MarketQuotation>
    {
        public MarketQuotationConfiguration()
        {

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.QuotationCode)
                .HasMaxLength(50)
                .IsRequired()
                .HasColumnAnnotation("Index", new IndexAnnotation(
                    new IndexAttribute("UK_QuotationCode", 1) { IsUnique = true }));

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            Property(x => x.AddedByUserId)
                .IsRequired();

            Property(x => x.AddedDateTime)
                .IsRequired();


            HasRequired(x => x.Customer)
                .WithMany(l => l.MarketQuotations)
                .HasForeignKey(x => x.CustomerId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.PriceInspectorUser)
                .WithMany(l => l.CollectedMarketQuotations)
                .HasForeignKey(x => x.PriceInspectorUserId);

            HasRequired(x => x.Adder)
                .WithMany(l => l.AddedMarketQuotations)
                .HasForeignKey(x => x.AddedByUserId)
                .WillCascadeOnDelete(false);

        }
    }
}
