﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Configurations
{
    public class SaleConfiguration : EntityTypeConfiguration<Sale>
    {
        public SaleConfiguration()
        {

            HasKey(x => x.Id);
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.DateOfCreation)
                .IsRequired()
                .HasColumnType("DATE");

            HasRequired(x=>x.Adder)
                .WithMany(m=>m.AddedSales)
                .HasForeignKey(f=>f.AddedByUserId)
                .WillCascadeOnDelete(false);

            HasRequired(x=>x.Customer)
                .WithMany(l=>l.Sales)
                .HasForeignKey(f=>f.CustomerId)
                .WillCascadeOnDelete(false);
        }
    }
}
