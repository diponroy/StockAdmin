﻿using System;
using System.Data.Entity;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Db.Contexts
{
    internal interface IStockAdminContext : IDisposable
    {
        DbSet<User> Users { get; set; }
        DbSet<UserPermission> UserPermissions { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<CustomerContactPerson> CustomerContactPersons { get; set; }
        DbSet<ProductUnit> ProductUnits { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<Supplier> Suppliers { get; set; }

        DbSet<MarketQuotation> MarketQuotations { get; set; }
        DbSet<MarketQuotationInspection> MarketQuotationInspections { get; set; }

        DbSet<ClientQuotation> ClientQuotations { get; set; }
        DbSet<ClientQuotationOrder> ClientQuotationOrders { get; set; }

        DbSet<Purchase> Purchases { get; set; }
        DbSet<PurchasedProducts> PurchasedProductses { get; set; }

        DbSet<Sale> Sales { get; set; }
        DbSet<SoldProducts> SoldProducts { get; set; }

        DbSet<Stock> Stocks { get; set; }

        DbSet<StockConsumptions> StockConsumptions { get; set; }

        DbSet<MarketPriceList> MarketPriceLists { get; set; }

        DbSet<StockPriceList> StockPriceLists { get; set; }

        Database Database { get; }
    }
}
