﻿using System.Data.Entity;
using KCRM.Db.ContextInitializer;

namespace KCRM.Db.Contexts
{
    public class StockAdminContext : Context, IStockAdminContext
    {
        public StockAdminContext()
            : base(nameOrConnectionString: "DbStockAdmin")
        {
        }

        static StockAdminContext()
        {
            Database.SetInitializer(new KcrmContextInitializer());
        }
    }
}
