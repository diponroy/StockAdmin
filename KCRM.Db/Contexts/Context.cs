﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using KCRM.Db.Configurations;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Db.Contexts
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserPermission> UserPermissions { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerContactPerson> CustomerContactPersons { get; set; }
        public DbSet<ProductUnit> ProductUnits { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<MarketQuotation> MarketQuotations { get; set; }
        public DbSet<MarketQuotationInspection> MarketQuotationInspections { get; set; }

        public DbSet<ClientQuotation> ClientQuotations { get; set; }
        public DbSet<ClientQuotationOrder> ClientQuotationOrders { get; set; }

        public DbSet<Purchase> Purchases { get; set; } 
        public DbSet<PurchasedProducts> PurchasedProductses { get; set; }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<SoldProducts> SoldProducts { get; set; }

        public DbSet<Stock> Stocks { get; set; }

        public DbSet<StockConsumptions> StockConsumptions { get; set; }

        public DbSet<MarketPriceList> MarketPriceLists { get; set; }

        public DbSet<StockPriceList> StockPriceLists { get; set; } 


        public DbSet<TType> GetTable<TType>() where TType : class
        {
            return null;
        }

        protected Context(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserPermissionConfiguration());

            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new CustomerContactPersonConfiguration());

            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new ProductUnitConfiguration());

            modelBuilder.Configurations.Add(new SupplierConfiguration());

            modelBuilder.Configurations.Add(new MarketQuotationConfiguration());
            modelBuilder.Configurations.Add(new MarketQuotationInspectionConfiguration());

            modelBuilder.Configurations.Add(new ClientQuotationConfiguration());
            modelBuilder.Configurations.Add(new ClientQuotationOrderConfiguration());

            modelBuilder.Configurations.Add(new PurchaseConfiguration());
            modelBuilder.Configurations.Add(new PurchasedProductsConfiguration());

            modelBuilder.Configurations.Add(new SaleConfiguration());
            modelBuilder.Configurations.Add(new SoldProductsConfiguration());

            modelBuilder.Configurations.Add(new StockConfiguration());

            modelBuilder.Configurations.Add(new StockConsumptionsConfiguration());

            modelBuilder.Configurations.Add(new MarketPriceListConfiguration());

            modelBuilder.Configurations.Add(new StockPriceListConfiguration());
        }
    }
}
