﻿using System.Data.Entity;
using KCRM.Db.ContextInitializer;

namespace KCRM.Db.Contexts
{
    class BuildContext : Context, IStockAdminContext
    {
        public BuildContext()
            : base(nameOrConnectionString: "DbStockAdmin")
        {
        }

        static BuildContext()
        {
            Database.SetInitializer(new BuildContextInitializer());
        }
    }
}
