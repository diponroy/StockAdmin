namespace KCRM.Db.SeedSerializer
{
    internal class DbSeedSerializer
    {
        public void Selialize<TContext, TEntity>(TContext context, EntitySeedSerializer<TEntity> entitySeedSerializer)
        {
            entitySeedSerializer.SerializeInto(context);
        }
    }
}