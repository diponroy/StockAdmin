using System;
using System.Collections.Generic;

namespace KCRM.Db.SeedSerializer
{
    class EntitySeedSerializer<TEntity>
    {
        public List<TEntity> Entities { get; private set; } 

        public EntitySeedSerializer()
        {
            Entities = new List<TEntity>();
        }

        protected void Seed(TEntity entity)
        {
            Entities.Add(entity);
        }

        protected void Seed(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Seed(entity);
            }
        }

        public void SerializeInto<TDbContext>(TDbContext context)
        {
            try
            {
                dynamic dbContext = context;
                foreach (var aEntity in Entities)
                {
                    dbContext.Set<TEntity>().Add(aEntity);
                    dbContext.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error to serialize seeds", exception);
            }        
        }
    }
}