﻿using System;
using KCRM.Db.SeedSerializer;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Db.Seeds
{
    class UserSeed: EntitySeedSerializer<User>
    {
        public UserSeed()
        {
            User aUser = new User()
            {
                LoginName = "admin",
                Password = "21232F297A57A5A743894A0E4A801FC3", //"admin"
                Email = "diponsust.kcrm@gmail.com",
                UserType = UserTypeEnum.Administrator,
                DateOfCreation = DateTime.Now,
                FirstName = "Pearl",
                LastName = "Tech",
                ContactNumber = "+88112233",

                AddedByUserId = 1,
                AddedDateTime = DateTime.Now,
                IsRemoved = false
            };
            Seed(aUser);
        }
    }
}