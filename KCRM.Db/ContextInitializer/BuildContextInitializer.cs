﻿using System.Data.Entity;
using KCRM.Db.Contexts;
using KCRM.Db.Seeds;
using KCRM.Db.SeedSerializer;

namespace KCRM.Db.ContextInitializer
{
    internal class BuildContextInitializer : CreateDatabaseIfNotExists<BuildContext>
    {
        private readonly DbSeedSerializer _dbSeedSerializer;

        public BuildContextInitializer()
        {
            this._dbSeedSerializer = new DbSeedSerializer();
        }

        protected override void Seed(BuildContext context)
        {
            _dbSeedSerializer.Selialize(context, new UserSeed());
            context.SaveChanges();
        }
    }
}