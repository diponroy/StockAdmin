﻿using System.Data.Entity;
using KCRM.Db.Contexts;
using KCRM.Db.SeedSerializer;

namespace KCRM.Db.ContextInitializer
{
    internal class KcrmContextInitializer : CreateDatabaseIfNotExists<StockAdminContext>
    {
        private readonly DbSeedSerializer _dbSeedSerializer;

        public KcrmContextInitializer()
        {
            this._dbSeedSerializer = new DbSeedSerializer();
        }

        protected override void Seed(StockAdminContext context)
        {
        }

    }
}