﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Views;

namespace KCRM.Data.Views
{
    public class ProductStockPriceData
    {
        public DbRawSqlQuery<ProductStockPrice> Get()
        {
            return new StockAdminContext().Database.SqlQuery<ProductStockPrice>(@"
                WITH ProductStockPrice(Id, Name, Category, [Description], DateOfCreation, UnitName, Price)
                AS
                (
	                SELECT 		                
		                Product.Id, 
		                Product.Name, 
		                Product.Category, 
		                Product.[Description], 
		                Product.DateOfCreation, 
		                ProductUnit.Name AS UnitName, 
		                COALESCE(StockPriceList.Price, 0) AS Price
		
		                FROM Product 
		                LEFT JOIN ProductUnit ON Product.ProductUnitId = ProductUnit.Id
		                LEFT JOIN StockPriceList ON Product.Id = StockPriceList.ProductId
                )
                SElECT *
	                FROM ProductStockPrice
            ");
        }
    }
}
