﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Db.Contexts;
using System.Data.Entity;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Views
{
    public class MarketQutationDetailData
    {
        public IQueryable<MarketQuotation> Get()
        {
            var context = new StockAdminContext();
            var list = context.MarketQuotations
                .Include(x => x.Customer)
                .Include(x => x.PriceInspectorUser);
            return list;
        } 
    }
}
