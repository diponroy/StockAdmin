﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Views
{
    public class PurchaseDetailData
    {
        public IQueryable<Purchase> Get()
        {
            var context = new StockAdminContext();
            var list = context.Purchases
                .Include(x => x.Customer)
                .Include(x => x.Purchaser);
            return list;
        } 
    }
}
