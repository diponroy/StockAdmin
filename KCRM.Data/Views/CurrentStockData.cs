﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Views;

namespace KCRM.Data.Views
{
    public class CurrentStockData
    {
        public DbRawSqlQuery<CurrentStock> Get()
        {
            return new StockAdminContext().Database.SqlQuery<CurrentStock>(@"
                WITH CurrentStock(ProductId, Category, Name, UnitId, UnitName, AvailableQutity)
                AS
                (
                    SELECT  Product.Id AS ProductId, 
                            Product.Category AS Category,
                            Product.Name AS Name,
			                Product.ProductUnitId AS UnitId, 
			                ProductUnit.Name AS UnitName, 
			                COALESCE(AvailableQutity, 0)
                        FROM Product
                        JOIN ProductUnit ON Product.ProductUnitId = ProductUnit.Id
                        LEFT JOIN 
                        (
                            SELECT Stock.ProductId, Stock.Quantity - COALESCE(Consumption.Quantiy, 0) AS AvailableQutity
                            FROM 
                            (
                                -- Product added to stock
                                SELECT ProductId, SUM(Quantity) AS Quantity
                                FROM Stock
                                GROUP BY ProductId
                            ) AS Stock	
                            LEFT JOIN
                            (	
                                -- Product consumption from stock
                                SELECT SoldProducts.ProductId, SUM(StockConsumptions.Quantity) AS Quantiy
                                FROM StockConsumptions
                                JOIN SoldProducts ON SoldProducts.Id = StockConsumptions.SoldProductId
                                GROUP BY SoldProducts.ProductId
                            ) AS Consumption
                            ON Stock.ProductId = Consumption.ProductId

                        ) AS AvailableStock
                        ON Product.Id = AvailableStock.ProductId
                )
                SELECT *
                    FROM CurrentStock
            ");
        }

        public CurrentStock Get(long productId)
        {
            return Get().Single(x => x.ProductId == productId);
        }

        public bool AnyAvailableStock(long productId, long quantity)
        {
            return Get().Any(x => x.ProductId == productId && x.AvailableQutity >= quantity);
        }
    }
}
