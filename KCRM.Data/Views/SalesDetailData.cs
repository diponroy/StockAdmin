﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Db.Contexts;
using System.Data.Entity;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Views
{
    public class SalesDetailData
    {
        public IQueryable<Sale> Get()
        {
            var context = new StockAdminContext();
            var list = context.Sales
                .Include(x => x.Customer)
                .Include(x => x.SoldProducts)
                .Include(x => x.SoldProducts.Select(y => y.Product))
                .Include(x => x.SoldProducts.Select(y => y.Product.Unit))
                .Include(x => x.SoldProducts.Select(y => y.StockConsumptions));
            return list;
        } 
    }
}
