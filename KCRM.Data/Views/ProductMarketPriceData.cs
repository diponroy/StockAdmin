﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Views;

namespace KCRM.Data.Views
{
    public class ProductMarketPriceData
    {
        public DbRawSqlQuery<ProductMarketPrice> Get()
        {
            return new StockAdminContext().Database.SqlQuery<ProductMarketPrice>(@"
                WITH ProductMarketPrice(Id, Name, Category, [Description], DateOfCreation, UnitName, Price)
                AS
                (
	                SELECT 
		                Product.Id, 
		                Product.Name, 
		                Product.Category, 
		                Product.[Description], 
		                Product.DateOfCreation, 
		                ProductUnit.Name AS UnitName,
		                COALESCE(MarketPriceList.Price, 0) AS Price
		
		                FROM Product
		                LEFT JOIN ProductUnit ON Product.ProductUnitId = ProductUnit.Id
		                LEFT JOIN MarketPriceList ON Product.Id = MarketPriceList.ProductId
                )
                SELECT *
	                FROM ProductMarketPrice
            ");
        }
    }
}
