﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Db.Contexts;
using System.Data.Entity;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Views
{
    public class StockDetailData
    {
        public IQueryable<Stock> Get()
        {
            var context = new StockAdminContext();
            var list = context.Stocks
                .Include(x => x.Product);
            return list;
        } 
    }
}
