﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Db.Contexts;
using System.Data.Entity;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Views
{
    public class ClientQutationDetailData
    {
        public IQueryable<ClientQuotation> Get()
        {
            var context = new StockAdminContext();
            var list = context.ClientQuotations
                .Include(x => x.Customer)
                .Include(x => x.CustomerContactPerson)
                .Include(x => x.MarketQuotation)
                .Include(x => x.MarketQuotation.PriceInspectorUser)
                .Include(x => x.ClientQuotationOrders)
                .Include(x => x.ClientQuotationOrders.Select(y => y.Product))
                .Include(x => x.ClientQuotationOrders.Select(y => y.Product.Unit));
            return list;
        } 
    }
}
