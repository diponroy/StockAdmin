﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Data.Shared
{
    public class SetupWithReplace<TSource> : SetupWithAddTrack<TSource> where TSource : class, IPrimaryKeyTrack, IAddTrack, IReplaceTrack
    {
        protected SetupWithReplace()
            : base()
        {
            
        }
        protected SetupWithReplace(StockAdminContext context)
            : base(context)
        {
            
        }

        public virtual void ValidateToReplace(TSource entity)
        {
            if (InvalidAssosiatorValue(entity.ReplacedByUserId))
            {
                throw new NullReferenceException("ReplacedByUserId");
            }

            if (InvalidDateTime(entity.ReplacedDateTime))
            {
                throw new NullReferenceException("ReplacedDateTime");
            }
        }

        public virtual void Replace(TSource entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            dbEntityEntry.State = EntityState.Modified;
        }
    }
}
