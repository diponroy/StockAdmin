﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Data.Shared
{
    public class SetupWithPrimaryKey<TSource> : Context<TSource> where TSource : class, IPrimaryKeyTrack
    {
        protected SetupWithPrimaryKey()
            : base()
        {
            
        }
        protected SetupWithPrimaryKey(StockAdminContext context)
            : base(context)
        {
            
        }

        public virtual TSource Find(long id)
        {
            return DbSet.Find(id);
        }

        public virtual TSource Get(long id)
        {
            return DbSet.Single(x => x.Id == id);
        }

        public virtual IQueryable<TSource> Get()
        {
            return DbSet;
        }

        public virtual void Add(TSource entity)
        {
            DbEntityEntry dbEntityEntry = DbContext.Entry(entity);
            if (dbEntityEntry.State != EntityState.Detached)
            {
                dbEntityEntry.State = EntityState.Added;
            }
            else
            {
                DbSet.Add(entity);
            }
        }
    }
}
