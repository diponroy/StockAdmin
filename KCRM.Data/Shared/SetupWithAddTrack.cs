﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Data.Shared
{
    public class SetupWithAddTrack<TSource> : SetupWithPrimaryKey<TSource> where TSource : class, IPrimaryKeyTrack, IAddTrack
    {
        protected SetupWithAddTrack()
            : base()
        {
            
        }
        protected SetupWithAddTrack(StockAdminContext context)
            : base(context)
        {
            
        }

        protected bool InvalidDateTime(DateTime? dateTime)
        {
            return dateTime == null || (dateTime.HasValue && dateTime == DateTime.MinValue);
        }

        protected bool InvalidAssosiatorValue(long? userId)
        {
            return userId == 0 || userId == null;
        }

        public virtual void ValidateToAdd(TSource entity)
        {
            if (InvalidAssosiatorValue(entity.AddedByUserId))
            {
                throw new NullReferenceException("AddedByUserId");
            }

            if (InvalidDateTime(entity.AddedDateTime))
            {
                throw new NullReferenceException("AddedDateTime");               
            }
        }
    }
}
