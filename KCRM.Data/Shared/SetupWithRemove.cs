﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Data.Shared
{
    public class SetupWithRemove<TSource> : SetupWithReplace<TSource> where TSource : class, IRemoveTrack, IPrimaryKeyTrack, IAddTrack
    {
        protected SetupWithRemove()
            : base()
        {
            
        }

        protected SetupWithRemove(StockAdminContext context)
            : base(context)
        {
            
        }

        public virtual IQueryable<TSource> GetNotRemoved()
        {
            return base.Get().Where(x => x.IsRemoved == false);
        }

        public override void Add(TSource entity)
        {
            entity.IsRemoved = false;
            base.Add(entity);
        }

        public virtual void ValidateToRemove(TSource entity)
        {
            base.ValidateToReplace(entity);
        }

        public virtual void Remove(TSource entity)
        {
            entity.IsRemoved = true;
            base.Replace(entity);
        }
    }
}
