﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using KCRM.Db.Contexts;

namespace KCRM.Data.Shared
{
    public class Context<TEntity> : IDisposable where TEntity : class
    {
        protected Context()
        {
            DbContext = new StockAdminContext();
            DbSet = DbContext.Set<TEntity>();
            Connection = DbContext.Database.Connection;

            DbContext.Configuration.ProxyCreationEnabled = false;
            DbContext.Configuration.LazyLoadingEnabled = false;
            // Because Web API will perform validation, we don't need/want EF to do so
            //DbContext.Configuration.ValidateOnSaveEnabled = false;
        }

        protected Context(StockAdminContext dbContext)
        {
            if (dbContext == null)
            {
                throw new NullReferenceException("dbContext");
            }
            DbContext = dbContext;
            DbSet = DbContext.Set<TEntity>();
            Connection = DbContext.Database.Connection;
        }

        public StockAdminContext DbContext { get; private set; }

        public DbSet<TEntity> DbSet { get; private set; }

        public DbConnection Connection { get; private set; }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            }
            DbContext.Dispose();
        }

        ~Context()
        {
            Dispose();
        }
    }
}
