﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Data.Shared
{
    public class SetupWithReplaceAndCreatedDate<TSource> : SetupWithReplace<TSource> where TSource : class, IDateOfCreationTrack, IPrimaryKeyTrack, IAddTrack, IReplaceTrack
    {
        protected SetupWithReplaceAndCreatedDate()
            : base()
        {
            
        }
        protected SetupWithReplaceAndCreatedDate(StockAdminContext context)
            : base(context)
        {
            
        }

        public override void ValidateToAdd(TSource entity)
        {
            base.ValidateToAdd(entity);
            if (InvalidDateTime(entity.DateOfCreation))
            {
                throw new NullReferenceException("DateOfCreation");
            }
        }
    }
}
