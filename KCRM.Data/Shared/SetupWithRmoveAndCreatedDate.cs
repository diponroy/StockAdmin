﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables.IEntity.Shared;

namespace KCRM.Data.Shared
{
    public class SetupWithRemoveAndCreatedDate<TSource> : SetupWithRemove<TSource> where TSource : class, IDateOfCreationTrack, IPrimaryKeyTrack, IAddTrack, IRemoveTrack
    {
        protected SetupWithRemoveAndCreatedDate()
            : base()
        {
            
        }
        protected SetupWithRemoveAndCreatedDate(StockAdminContext context)
            : base(context)
        {
            
        }

        public override void ValidateToAdd(TSource entity)
        {
            if (entity.DateOfCreation == null)
            {
                throw new NullReferenceException("DateOfCreation");
            }
        }
    }
}
