﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class SaleData : SetupWithAddTrackAndCreatedDate<Sale>
    {
        public SaleData()
            : base()
        {
            
        }
        public SaleData(StockAdminContext context)
            : base(context)
        {
            
        }

        public override void Add(Sale entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }
    }
}
