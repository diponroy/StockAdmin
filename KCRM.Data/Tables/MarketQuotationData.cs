﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class MarketQuotationData : SetupWithAddTrackAndCreatedDate<MarketQuotation>
    {
        public MarketQuotationData() : base()
        {
            
        }
        public MarketQuotationData(StockAdminContext dbContext)
            : base(dbContext)
        {

        }
        public bool AnyCode(string code)
        {
            return DbSet.Any(x => x.QuotationCode.Equals(code.Trim()));
        }

        public IQueryable<MarketQuotation> GetByCustomerId(long id)
        {
            return Get().Include(x => x.PriceInspectorUser).Where(x => x.CustomerId == id);
        }

        public IQueryable<MarketQuotation> GetByPriceInspectorId(long id)
        {
            return Get().Where(x => x.PriceInspectorUserId == id);
        }
        public override void Add(MarketQuotation entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }

        public bool AnyName(string code)
        {
            return DbSet.Any(x => x.QuotationCode.Equals(code));
        }

        public string GetNextCode()
        {
            return new StockAdminContext().Database.SqlQuery<string>(@"
                /*Current year*/
                DECLARE @currentYear VARCHAR(4);
                SET @currentYear = YEAR(GETDATE());

                /*Code prefix with year*/
                DECLARE @prefix VARCHAR(13);
                SET @prefix = 'QUT-MKT-' + @currentYear +'-'

                /*Curent code count + 1 with the prefix*/
                DECLARE @topCodeCount VARCHAR(100)
                SET @topCodeCount = (SELECT COUNT(QuotationCode)
						                FROM MarketQuotation
						                WHERE QuotationCode LIKE @prefix + '%') + 1		/**/
						
                DECLARE @code VARCHAR(100)
                SET @code = @prefix + @topCodeCount
                SELECT @code
            ").FirstOrDefault();
        }
    }
}
