﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Data.Views;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class StockConsumptionData : SetupWithPrimaryKey<StockConsumptions>
    {
        public StockConsumptionData() : base()
        {
        }

        public StockConsumptionData(StockAdminContext dbContext)
            : base(dbContext)
        {
        }
    }
}
