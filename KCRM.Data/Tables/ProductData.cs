﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Tables
{
    public class ProductData : SetupWithReplaceAndCreatedDate<Product>
    {
        public ProductData() : base()
        {
            
        }

        public ProductData(StockAdminContext dbContext)
            : base(dbContext)
        {

        }

        public ICollection<ProductModel> GetAll()
        {
            var products = base.Get()
                .Select(x => new ProductModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Category = x.Category,
                    CategoryName = x.Category.ToString(),
                    UnitName = x.Unit.Name,
                    Description = x.Description,
                    DateOfCreation = x.DateOfCreation            
                }).ToList();

            return products;
        }

        public bool AnyName(string name)
        {
            return DbSet.Any(x => x.Name.Equals(name));
        }

        public bool AnyNameExceptProduct(string name, long id)
        {
            return DbSet.Any(x => x.Id != id && x.Name.Equals(name));
        }

        public Product GetWithUnit(long id)
        {
            return DbSet.Include("Unit").Single(x => x.Id == id);
        }

        public IQueryable<Product> GetWithUnit()
        {
            return DbSet.Include("Unit");        }

        public override void Add(Product product)
        {
            base.ValidateToAdd(product);
            base.Add(product);
        }

        public override void Replace(Product product)
        {
            base.ValidateToReplace(product);
            var oldProduct = Get(product.Id);
            oldProduct.Name = product.Name;
            oldProduct.Category = product.Category;
            oldProduct.Description = product.Description;
            oldProduct.DateOfCreation = product.DateOfCreation;
            oldProduct.ReplacedDateTime = product.ReplacedDateTime;
            oldProduct.ReplacedByUserId = product.ReplacedByUserId;
            base.Replace(oldProduct);
        }

    }
}
