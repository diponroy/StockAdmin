﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Data.Tables
{
    public class UserData : SetupWithRemoveAndCreatedDate<User>
    {
        public UserData() : base()
        {
        }

        public UserData(StockAdminContext dbContext)
            : base(dbContext)
        {
        }

        public User FindNotDeleted(string loginName, string password)
        {
            return GetNotRemoved().FirstOrDefault(x => x.LoginName.Equals(loginName) && x.Password.Equals(password));
        }

        public User FindNotDeletedByLonginNameAndEmail(string loginName, string email)
        {
            return GetNotRemoved().FirstOrDefault(x => x.LoginName.Equals(loginName) && x.Email.Equals(email));
        }

        public bool AnyLoginName(string loginName)
        {
            return DbSet.Any(x => x.LoginName.Equals(loginName));
        }

        public bool AnyEmail(string email)
        {
            return DbSet.Any(x => x.Email.Equals(email));
        }

        public bool AnyLoginNameExceptUser(string loginName, long id)
        {
            return DbSet.Any(x => x.Id != id && x.LoginName.Equals(loginName));
        }

        public bool AnyEmailExceptUser(string email, long id)
        {
            return DbSet.Any(x => x.Id != id && x.Email.Equals(email));
        }

        public IQueryable<User> GetNotRemodedNonAdministrators()
        {
            return GetNotRemoved().Where(x => x.UserType != UserTypeEnum.Administrator);
        }

        public override void Add(User user)
        {
            base.ValidateToAdd(user);
            base.Add(user);
        }

        public override void Replace(User user)
        {
            base.ValidateToReplace(user);
            var oldUser = Get(user.Id);
            oldUser.FirstName = user.FirstName;
            oldUser.LastName = user.LastName;
            oldUser.LoginName = user.LoginName;
            oldUser.Email = user.Email;
            oldUser.Password = (String.IsNullOrEmpty(user.Password)) ? oldUser.Password : user.Password;
            oldUser.ContactNumber = user.ContactNumber;
            oldUser.UserType = user.UserType;
            oldUser.Address = user.Address;
            oldUser.DateOfCreation = user.DateOfCreation;
            oldUser.ReplacedDateTime = user.ReplacedDateTime;
            oldUser.ReplacedByUserId = user.ReplacedByUserId;
            base.Replace(oldUser);
        }

        public void RemoveById(User user)
        {
            base.ValidateToRemove(user);
            var oldUser = Get(user.Id);
            oldUser.ReplacedDateTime = user.ReplacedDateTime;
            oldUser.ReplacedByUserId = user.ReplacedByUserId;
            base.Remove(oldUser);
        }
    }
}
