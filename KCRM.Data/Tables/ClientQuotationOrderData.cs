﻿using System.Collections.Generic;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class ClientQuotationOrderData : SetupWithPrimaryKey<ClientQuotationOrder>
    {
        public ClientQuotationOrderData()
            : base()
        {
            
        }
        public ClientQuotationOrderData(StockAdminContext context)
            : base(context)
        {
        }
    }
}
