﻿using System.Linq;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class UserPermissionData : SetupWithRemove<UserPermission>
    {
        public UserPermissionData()
            : base()
        {
            
        }
        public UserPermissionData(StockAdminContext context)
            : base(context)
        {
            
        }

        public IQueryable<UserPermission> GetNotRemoved(long userId)
        {
            return GetNotRemoved().Where(x => x.UserId == userId);
        }
        public override void Add(UserPermission entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }
        public override void Remove(UserPermission entity)
        {
            base.ValidateToRemove(entity);
            var oldEntity = Get(entity.Id);
            oldEntity.ReplacedDateTime = entity.ReplacedDateTime;
            oldEntity.ReplacedByUserId = entity.ReplacedByUserId;
            base.Remove(oldEntity);
        }
    }
}
