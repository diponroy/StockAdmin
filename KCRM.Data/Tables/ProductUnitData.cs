﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class ProductUnitData : Context<ProductUnit>
    {
        public ProductUnitData() : base()
        {
            
        }

        public ProductUnitData(StockAdminContext dbContext)
            : base(dbContext)
        {

        }

        public ProductUnit Find(long id)
        {
            return DbSet.Find(id);
        }
        public ProductUnit Get(long id)
        {
            return DbSet.Single(x => x.Id == id);
        }

        public ICollection<ProductUnit> GetAll()
        {
            var productUnits = DbSet.ToList()
                .Select(x => new ProductUnit()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    AddedDateTime = x.AddedDateTime,
                    AddedByUserId = x.AddedByUserId,
                    ReplacedDateTime = x.ReplacedDateTime,
                    ReplacedByUserId = x.ReplacedByUserId

                })
                .OrderBy(x=>x.Name)
                .ToList();
            
            return productUnits;
        }

        public ProductUnit Find(string name)
        {
            return DbSet.SingleOrDefault(x => x.Name.Equals(name));
        }

        public ProductUnit FindExceptProductUnit(string name, long id)
        {
            return DbSet.SingleOrDefault(x => x.Id != id && x.Name.Equals(name));
        }

        public bool AnyName(string name)
        {
            return Find(name) != null;
        }

        public bool AnyNameExceptProductUnit(string name, long id)
        {
            return FindExceptProductUnit(name, id) != null;
        }

        public void Add(ProductUnit productUnit)
        {
            try
            {
                DbSet.Add(productUnit);
                DbContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }         
        }

        public void Replace(ProductUnit productUnit)
        {
            if (productUnit.ReplacedDateTime == null || productUnit.ReplacedByUserId == null)
            {
                throw new NullReferenceException("ReplacedDateTime and ReplacedByUserId required to update ProductUnit");
            }

            var oldProductUnit = Get(productUnit.Id);
            oldProductUnit.Name = productUnit.Name;
            oldProductUnit.Description = productUnit.Description;
            oldProductUnit.ReplacedDateTime = productUnit.ReplacedDateTime;
            oldProductUnit.ReplacedByUserId = productUnit.ReplacedByUserId;

            DbContext.SaveChanges();
        }

        public void Remove(ProductUnit productUnit)
        {
            if (productUnit.ReplacedDateTime == null || productUnit.ReplacedByUserId == null)
            {
                throw new NullReferenceException("ReplacedDateTime and ReplacedByUserId requered to delete ProductUnit");
            }
            var oldProductUnit = Get(productUnit.Id);
            oldProductUnit.ReplacedDateTime = productUnit.ReplacedDateTime;
            oldProductUnit.ReplacedByUserId = productUnit.ReplacedByUserId;
        }
    }
}
