﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Data.Views;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Tables
{
    public class CustomerData : SetupWithReplaceAndCreatedDate<Customer>
    {
        public CustomerData() : base()
        {
            
        }

        public CustomerData(StockAdminContext dbContext) : base(dbContext)
        {

        }
        public ICollection<Customer> GetAll()
        {
            var customers = Get().ToList()
                .Select(x => new Customer()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Email = x.Email,
                    ContactNumber = x.ContactNumber,
                    Address = x.Address,
                    DateOfCreation = x.DateOfCreation,
                    AddedDateTime = x.AddedDateTime,
                    AddedByUserId = x.AddedByUserId,
                    ReplacedDateTime = x.ReplacedDateTime,
                    ReplacedByUserId = x.ReplacedByUserId
                }).ToList();

            return customers;
        }

        public bool AnyName(string name)
        {
            return DbSet.Any(x => x.Name.Equals(name));
        }
        public bool AnyEmail(string email)
        {
            return DbSet.Any(x => x.Email.Equals(email.Trim()));
        }

        public bool AnyNameExceptCustomer(string name, long id)
        {
            return DbSet.Any(x => x.Id != id && x.Name.Equals(name));
        }

        public override void Add(Customer customer)
        {
            base.ValidateToAdd(customer);
            base.Add(customer);
        }

        public override void Replace(Customer customer)
        {
            base.ValidateToReplace(customer);
            var oldCustomer = Get(customer.Id);
            oldCustomer.Name = customer.Name;
            oldCustomer.Email = customer.Email;
            oldCustomer.ContactNumber = customer.ContactNumber;
            oldCustomer.Address = customer.Address;
            oldCustomer.DateOfCreation = customer.DateOfCreation;
            oldCustomer.ReplacedDateTime = customer.ReplacedDateTime;
            oldCustomer.ReplacedByUserId = customer.ReplacedByUserId;
            base.Replace(oldCustomer);
        }

        public bool AnyEmailExceptCustomer(string email, long id)
        {
            return DbSet.Any(x => x.Id != id && x.Email.Equals(email));
        }
    }
}
