﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class MarketPriceListData : SetupWithReplace<MarketPriceList>
    {
        public MarketPriceListData()
            : base()
        {

        }
        public MarketPriceListData(StockAdminContext context)
            : base(context)
        {
        }

        public MarketPriceList FindByProductId(long id)
        {
            return DbSet.FirstOrDefault(x => x.ProductId == id);
        }
        public bool AnyByProductId(long id)
        {
            return DbSet.Any(x => x.ProductId == id);
        }
        public MarketPriceList GetByProductId(long id)
        {
            return DbSet.Single(x => x.ProductId == id);
        }
        public override void Add(MarketPriceList entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }
        public override void Replace(MarketPriceList entity)
        {
            base.ValidateToReplace(entity);
            var oldEntity = GetByProductId(entity.ProductId);
            oldEntity.Price = entity.Price;
            oldEntity.ReplacedDateTime = entity.ReplacedDateTime;
            oldEntity.ReplacedByUserId = entity.ReplacedByUserId;
            base.Replace(oldEntity);
        }
        public IQueryable<MarketPriceList> GetWithProduct()
        {
            return DbSet.Include("Product");
        }
    }
}
