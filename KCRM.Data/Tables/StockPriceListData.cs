﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Data.Tables 
{
    public class StockPriceListData : SetupWithReplace<StockPriceList>
    {
        public StockPriceListData()
            : base()
        {

        }
        public StockPriceListData(StockAdminContext context)
            : base(context)
        {
        }

        public StockPriceList FindByProductId(long id)
        {
            return DbSet.FirstOrDefault(x => x.ProductId == id);
        }

        public bool AnyByProductId(long id)
        {
            return DbSet.Any(x => x.ProductId == id);
        }
        public StockPriceList GetByProductId(long id)
        {
            return DbSet.Single(x => x.ProductId == id);
        }
        
        public override void Add(StockPriceList entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }
        public override void Replace(StockPriceList entity)
        {
            base.ValidateToReplace(entity);
            var oldEntity = GetByProductId(entity.ProductId);
            oldEntity.Price = entity.Price;
            oldEntity.ReplacedDateTime = entity.ReplacedDateTime;
            oldEntity.ReplacedByUserId = entity.ReplacedByUserId;
            base.Replace(oldEntity);
        }
        public IQueryable<StockPriceList> GetWithProduct()
        {
            return DbSet.Include("Product");
        }
    }
}
