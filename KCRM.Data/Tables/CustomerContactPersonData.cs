﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class CustomerContactPersonData : SetupWithRemove<CustomerContactPerson>
    {
        public CustomerContactPersonData() : base()
        {
            
        }
        public CustomerContactPersonData(StockAdminContext dbContext)
            : base(dbContext)
        {
        }

        public IQueryable<CustomerContactPerson> GetNotRemovedByCustomerId(long id)
        {
            return GetNotRemoved().Where(x => x.CustomerId == id);
        }

        public override void Remove(CustomerContactPerson removedPerson)
        {
            base.ValidateToReplace(removedPerson);
            var aPerson = Get(removedPerson.Id);
            aPerson.ReplacedByUserId = removedPerson.ReplacedByUserId;
            aPerson.ReplacedDateTime = removedPerson.ReplacedDateTime;
            base.Remove(aPerson);
        }

        public override void Replace(CustomerContactPerson updadtedPerson)
        {
            base.ValidateToReplace(updadtedPerson);
            var oldPerson = Get(updadtedPerson.Id);
            oldPerson.Name = updadtedPerson.Name;
            oldPerson.ContactNumber = updadtedPerson.ContactNumber;
            oldPerson.ReplacedDateTime = updadtedPerson.ReplacedDateTime;
            oldPerson.ReplacedByUserId = updadtedPerson.ReplacedByUserId;
            base.Replace(oldPerson);         
        }
    }
}
