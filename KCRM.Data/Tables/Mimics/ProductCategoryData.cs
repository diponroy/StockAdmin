﻿using System.Collections.Generic;
using System.Linq;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;

namespace KCRM.Data.Tables.Mimics
{
    public class ProductCategoryData
    {
        public ICollection<ProductCategory> GetAll()
        {
            var productCategories = new List<ProductCategory>()
            {
                new ProductCategory(ProductCategoryEnum.Safety),
                new ProductCategory(ProductCategoryEnum.HardwareTools, "Hardware Tools"),
                new ProductCategory(ProductCategoryEnum.Electric)
            };
            return productCategories;
        }

        public string GetName(ProductCategoryEnum category)
        {
            return GetAll().Single(x => x.ProductCategoryEnum == category).Name;
        }
    }
}
