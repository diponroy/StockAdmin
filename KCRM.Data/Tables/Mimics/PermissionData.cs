﻿using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;

namespace KCRM.Data.Tables.Mimics
{
    public class PermissionData
    {
        public ICollection<Permission> GetAll()
        {
            var permissions = new List<Permission>()
            {
                /*setups*/
                new Permission(PermissionEmun.UserSetup, "User Setup"),
                new Permission(PermissionEmun.UserPermissionSetup, "Permission Setup"),
                new Permission(PermissionEmun.CustomerSetup, "Customer Setup"),
                new Permission(PermissionEmun.ProductSetup, "Product Setup"),
                new Permission(PermissionEmun.SupplierSetup, "Supplier Setup"),

                new Permission(PermissionEmun.MarketQuotation, "Market Quotation"),
                new Permission(PermissionEmun.ClientQuotation, "Client Quotation"),

                new Permission(PermissionEmun.Purchase, "Purchase"),

                new Permission(PermissionEmun.Stock, "Stock"),

                new Permission(PermissionEmun.Sale, "Sale"),

				new Permission(PermissionEmun.MarketPrice, "Market Price List"),
                new Permission(PermissionEmun.StockPrice, "Stock Price List"),


                /*Reports*/
                new Permission(PermissionEmun.MarketQuotationReport, "Market Quotation Report"),
                new Permission(PermissionEmun.ClientQuotationReport, "Client Quotation Report"),
                new Permission(PermissionEmun.PurchaseReport, "Purchase Report"),
                new Permission(PermissionEmun.StockEntryReport, "Stock Entry Report"),
                new Permission(PermissionEmun.CurrentStockReport, "Current Stock Report"),
                new Permission(PermissionEmun.SalesReport, "Sales Report"),
                new Permission(PermissionEmun.MarketPriceReport, "Market Price Report"),
                new Permission(PermissionEmun.StockPriceReport, "Stock Price Report")
            };
            return permissions;
        }
    }
}
