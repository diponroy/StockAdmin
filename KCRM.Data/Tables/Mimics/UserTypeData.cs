﻿using System.Collections.Generic;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;

namespace KCRM.Data.Tables.Mimics
{
    public class UserTypeData
    {
        public ICollection<UserType> GetAll()
        {
            var userTypes = new List<UserType>()
            {
                new UserType(UserTypeEnum.Administrator),
                new UserType(UserTypeEnum.ApplicationUser, "Application User")
            };
            return userTypes;
        }
    }
}
