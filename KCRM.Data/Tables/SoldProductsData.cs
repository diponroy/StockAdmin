﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class SoldProductsData : SetupWithPrimaryKey<SoldProducts>
    {
        public SoldProductsData()
            : base()
        {
            
        }
        public SoldProductsData(StockAdminContext context)
            : base(context)
        {
            
        }
    }
}
