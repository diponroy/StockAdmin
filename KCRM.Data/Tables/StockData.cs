﻿using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class StockData : SetupWithAddTrackAndCreatedDate<Stock>
    {
        public StockData()
            : base()
        {
            
        }
        public StockData(StockAdminContext context)
            : base(context)
        {
            
        }

        public override void Add(Stock entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }
    }
}
