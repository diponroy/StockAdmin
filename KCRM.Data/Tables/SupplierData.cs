﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class SupplierData : SetupWithReplaceAndCreatedDate<Supplier>
    {
        public SupplierData()
            : base()
        {
            
        }
        public SupplierData(StockAdminContext context)
            : base(context)
        {
            
        }

        public ICollection<Supplier> GetAll()
        {
            return base.Get().OrderBy(x=>x.Name).ToList();
        }

        public bool AnyName(string name)
        {
            return DbSet.Any(x => x.Name.ToLower().Equals(name.ToLower()));
        }

        public bool AnyEmail(string email)
        {
            return DbSet.Any(x => x.Email.ToLower().Equals(email.ToLower()));
        }

        public bool AnyNameExceptUser(string name, long id)
        {
            return DbSet.Any(x => x.Name.ToLower().Equals(name.ToLower()) && x.Id != id);
        }

        public bool AnyEmailExceptUser(string email, long id)
        {
            return DbSet.Any(x => x.Email.ToLower().Equals(email.ToLower()) && x.Id != id);
        }

        public override void Add(Supplier supplier)
        {
            base.ValidateToAdd(supplier);
            base.Add(supplier);
        }

        public override void Replace(Supplier supplier)
        {
            base.ValidateToReplace(supplier);
            Supplier oldSupplier = Get(supplier.Id);
            oldSupplier.Name = supplier.Name;
            oldSupplier.Email = supplier.Email;
            oldSupplier.DateOfCreation = supplier.DateOfCreation;
            oldSupplier.ContactNumber = supplier.ContactNumber;
            oldSupplier.Address = supplier.Address;
            oldSupplier.ReplacedByUserId = supplier.ReplacedByUserId;
            oldSupplier.ReplacedDateTime = supplier.ReplacedDateTime;
            base.Replace(oldSupplier);
        }
    }
}
