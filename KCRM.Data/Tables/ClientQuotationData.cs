﻿using KCRM.Data.Shared;
using KCRM.Db.Contexts;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Data.Tables
{
    public class ClientQuotationData : SetupWithAddTrackAndCreatedDate<ClientQuotation>
    {
        public ClientQuotationData()
            : base()
        {
            
        }
        public ClientQuotationData(StockAdminContext context)
            : base(context)
        {
            
        }

        public override void Add(ClientQuotation entity)
        {
            base.ValidateToAdd(entity);
            base.Add(entity);
        }
    }
}
