// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   This class represents the standard header and footer for a PDF print.
//   application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Drawing;
using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Rectangle = iTextSharp.text.Rectangle;

namespace KCRM.Web.Utility.PdfReport
{

    /// <summary>
    /// This class represents the standard header and footer for a PDF print.
    /// application.
    /// </summary>
    public class PrintHeaderFooter : PdfPageEventHelper
    {
        private PdfContentByte _pdfContent;
        private PdfTemplate _pageNumberTemplate;
        private BaseFont _baseFont;
        private DateTime _printTime;

        public string Title { get; set; }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            _printTime = DateTime.Now;
            _baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            _pdfContent = writer.DirectContent;
            _pageNumberTemplate = _pdfContent.CreateTemplate(50, 50);
        }

        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);

            Rectangle pageSize = document.PageSize;

            if (Title != string.Empty)
            {
                _pdfContent.BeginText();
                _pdfContent.SetFontAndSize(_baseFont, 11);
                _pdfContent.SetRGBColorFill(0, 0, 0);
                _pdfContent.SetTextMatrix(pageSize.GetLeft(40), pageSize.GetTop(40));
                _pdfContent.ShowText(Title);
                _pdfContent.EndText();
            }
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            int pageN = writer.PageNumber;
            string text = pageN + " of ";
            float len = _baseFont.GetWidthPoint(text, 8);

            Rectangle pageSize = document.PageSize;
            _pdfContent = writer.DirectContent;
            _pdfContent.SetRGBColorFill(100, 100, 100);

            /*current and total page*/
            _pdfContent.BeginText();
            _pdfContent.SetFontAndSize(_baseFont, 8);
            _pdfContent.SetTextMatrix(pageSize.Width / 2, pageSize.GetBottom(10));
            _pdfContent.ShowText(text);
            _pdfContent.EndText();
            _pdfContent.AddTemplate(_pageNumberTemplate, (pageSize.Width / 2) + len, pageSize.GetBottom(10));

            /*server date at top*/
            _pdfContent.BeginText();
            _pdfContent.SetFontAndSize(_baseFont, 6);
            _pdfContent.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, _printTime.ToString("dd-MM-yyyy hh:mm:ss tt"), pageSize.GetRight(10), pageSize.GetTop(10), 0);
            _pdfContent.EndText();

            /*horizontal line*/
            PdfContentByte cb = writer.DirectContent;
            cb.MoveTo(pageSize.GetLeft(0), pageSize.GetBottom(45));
            cb.LineTo(pageSize.GetRight(0), pageSize.GetBottom(45));
            cb.SetLineWidth(0.5f);
            cb.Stroke();

            _pdfContent.BeginText();
            _pdfContent.SetFontAndSize(_baseFont, 6);
            _pdfContent.SetTextMatrix(10, pageSize.GetBottom(35));
            _pdfContent.ShowText("Dhaka: 223, Nawabpur Road (Tower House), Dhaka-1100, Bangladesh, Ph.:880-2-7117530, 9550853, 9556625, Email: StockAdmin@gmail.com.");
            _pdfContent.EndText();

            _pdfContent.BeginText();
            _pdfContent.SetFontAndSize(_baseFont, 6);
            _pdfContent.SetTextMatrix(10, pageSize.GetBottom(25));
            _pdfContent.ShowText("Chittagong: 183. Jubilee Road (Liberty Tower), Chittagong, Bangladesh, Tel:880-31-639227, 638973");
            _pdfContent.EndText();
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            _pageNumberTemplate.BeginText();
            _pageNumberTemplate.SetFontAndSize(_baseFont, 8);
            _pageNumberTemplate.SetTextMatrix(0, 0);
            _pageNumberTemplate.ShowText(string.Empty + (writer.PageNumber - 1));
            _pageNumberTemplate.EndText();
        }
    }
}