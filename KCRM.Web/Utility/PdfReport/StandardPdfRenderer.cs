// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   This class is responsible for rendering a html text string to a PDF document
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;

namespace KCRM.Web.Utility.PdfReport
{
    /// <summary>
    /// This class is responsible for rendering a html text string to a PDF document using the html renderer of iTextSharp
    /// </summary>
    public class StandardPdfRenderer
    {
        private const int HorizontalMargin = 40;
        private const int VerticalMargin = 40;

        public byte[] Render(string htmlText, string pageTitle)
        {
            byte[] renderedBuffer;

            using (var outputMemoryStream = new MemoryStream())
            {
                using (var pdfDocument = new Document(PageSize.A4, HorizontalMargin, HorizontalMargin, VerticalMargin, VerticalMargin))
                {
                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, outputMemoryStream);
                    pdfWriter.CloseStream = false;
                    pdfWriter.PageEvent = new PrintHeaderFooter { Title = pageTitle };
                    pdfDocument.Open();
                    using (var htmlViewReader = new StringReader(htmlText))
                    {
                        //using (var htmlWorker = new HTMLWorker(pdfDocument))
                        //{
                        //    htmlWorker.Parse(htmlViewReader);
                        //}

                        //create the default CSS Resolver
                        XMLWorkerHelper helperInstance = XMLWorkerHelper.GetInstance();
                        CssFilesImpl cssFiles = new CssFilesImpl();
                        cssFiles.Add(helperInstance.GetDefaultCSS());
                        StyleAttrCSSResolver cssRevolver = new StyleAttrCSSResolver(cssFiles);

                        //create the default Font provider
                        //XMLWorkerFontProvider xmlWorkerFontProvider = new XMLWorkerFontProvider();
                        //HtmlPipelineContext htmlContext = new HtmlPipelineContext(new CssAppliersImpl(xmlWorkerFontProvider));
                        HtmlPipelineContext htmlContext = new HtmlPipelineContext(new CssAppliersImpl());


                        //create the default tag provider
                        ITagProcessorFactory tpf = Tags.GetHtmlTagProcessorFactory();
                        htmlContext.SetTagFactory(tpf);

                        //set the image provider, in my case a blank image provider as my docs have no images
                        htmlContext.SetImageProvider(new BlankImageProvider(pdfDocument));

                        //create the Pipeline
                        IPipeline pipeline = new CssResolverPipeline(cssRevolver, new
                        HtmlPipeline(htmlContext, new PdfWriterPipeline(pdfDocument, pdfWriter)));
                        //parse the XHTML
                        XMLWorker worker = new XMLWorker(pipeline, true);
                        XMLParser p = new XMLParser(worker);
                        p.Parse(htmlViewReader);
                    }
                    pdfDocument.Close();
                }

                renderedBuffer = new byte[outputMemoryStream.Position];
                outputMemoryStream.Position = 0;
                outputMemoryStream.Read(renderedBuffer, 0, renderedBuffer.Length);
            }

            return renderedBuffer;
        }
    }
}