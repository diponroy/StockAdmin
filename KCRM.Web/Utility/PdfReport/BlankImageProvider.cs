using iTextSharp.text;
using iTextSharp.tool.xml.pipeline.html;

namespace KCRM.Web.Utility.PdfReport
{
    public class BlankImageProvider : IImageProvider
    {
        //Store a reference to the main document so that we can access the page size and margins
        private Document _mainDoc;
        //Constructor
        public BlankImageProvider(Document doc)
        {
            this._mainDoc = doc;
        }

        public string GetImageRootPath()
        {
            return "";
        }

        public void Reset()
        {
            return;
        }

        public iTextSharp.text.Image Retrieve(string src)
        {
            return null;

        }

        public void Store(string src, iTextSharp.text.Image img)
        {
            return;
        }
    }
}