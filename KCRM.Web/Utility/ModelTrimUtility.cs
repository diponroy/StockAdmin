﻿using System.Web.Mvc;

namespace KCRM.Web.Utility
{
    public class TrimModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
        ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
                return null;
            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueResult);
            return valueResult.AttemptedValue.Trim();
        }
    }

}