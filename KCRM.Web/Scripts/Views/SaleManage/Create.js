﻿/*Validations*/
ko.validation.rules['validStockConsumption'] = {
    validator: function (value, option) {
        var products = option().products;
        var totalConsumtion = parseInt(value);
        for (var i in products) {
            if (products[i].productId() === option().productId) {
                totalConsumtion += parseInt(products[i].stockQuantity());
            }
        }
        var hasStock = null;
        var json = JSON.stringify({ productId: option().productId, quantity: totalConsumtion });
        $.when(
            $.ajax({
                url: '/SaleManage/HasEnoughStock',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            hasStock = (textStatus === 'success') ? data : null;
        });
        return hasStock === option().hasEnoughStock;
    },
    message: 'There is no enough stock to sale'
};
ko.validation.registerExtenders();

ko.validation.rules['lessOrEqualTotalQuantity'] = {
    validator: function (value, option) {
        var optionValue = option();
        if (typeof (optionValue) === 'undefined' || optionValue === '' || parseInt(optionValue) < 1) {
            return false;
        }
        return parseInt(value) <= parseInt(optionValue);
    },
    message: 'Stock consumption should be less or equal to the total sale quantity'
};
ko.validation.registerExtenders();


/*Models*/
function Product(id, name, unit) {
    this.id = id;
    this.name = name;
    this.unit = unit;
}
function Customer(id, name) {
    this.id = id;
    this.name = name;
}
function SoldProductModel(options) {
    this.productId = ko.observable(options.productId);
    this.productName = ko.observable(options.productName);
    this.productUnit = ko.observable(options.productUnit);
    this.quantity = ko.observable(options.quantity);
    this.price = ko.observable(options.price);
    this.hasStockConsumption = ko.observable(options.hasStockConsumption);
    this.stockQuantity = ko.observable(options.stockQuantity);
}
/*Viewmodels*/
/*Sold Products ViewModel*/
function SoldProduct() {
    var self = this;
    self.editIndex = ko.observable(null);
    self.products = ko.observableArray([]);
    self.getProduct = function (id) {
        var products = self.products();
        var product = null;
        for (var i = 0; i < products.length; i++) {
            if (products[i].id === id) {
                product = products[i];
                break;
            }
        }
        return product;
    };

    self.soldProducts = ko.observableArray([]);

    self.productId = ko.observable().extend({required:true});
    self.productName = ko.computed(function() {
        var product = self.getProduct(self.productId());
        return (product != null) ? product.name : '-';
    }, this);
    self.productUnit = ko.computed(function() {
        var product = self.getProduct(self.productId());
        return (product != null) ? product.unit : '-';
    }, this);
    self.quantity = ko.observable().extend({ required: true, min: 1, digit: true});
    self.price = ko.observable().extend({ required: true, min: 0, number: true });
    self.hasStockConsumption = ko.observable(true).extend({required: true});
    self.stockQuantity = ko.observable().extend({
        required: true,
        min: 1,
        digit: true,
        lessOrEqualTotalQuantity: ko.computed(function () {
            return self.quantity();
        }, this),
        validStockConsumption: ko.computed(function () {
            return {
                productId: self.productId(),
                products: self.soldProducts(),
                hasEnoughStock: true
            };
        }, this)
    });
    self.totalPrice = ko.computed(function () {
        var totalPrice = 0;
        var products = self.soldProducts();
        for (var i in products) {
            totalPrice += parseFloat(products[i].price());
        }
        return parseFloat(totalPrice).toFixed(2);
    }, this);
        
    /*error*/
    self.errorWithOutStock = ko.validation.group([self.productId, self.hasStockConsumption, self.quantity, self.price]);
    self.errorWithStock = ko.validation.group([self.productId, self.hasStockConsumption, self.quantity, self.price, self.stockQuantity]);
    self.errors = function() {
        var error = (self.hasStockConsumption()) ? self.errorWithStock : self.errorWithOutStock;
            return error;
    };   
    self.removeErrors = function () {
        if (self.hasStockConsumption()) {
            self.errorWithStock.showAllMessages(false);
        } else {
            self.errorWithOutStock.showAllMessages(false);
        }
    };
    self.showErrors = function () {
        if (self.hasStockConsumption()) {
            self.errorWithStock.showAllMessages();
        } else {
            self.errorWithOutStock.showAllMessages();
        }
    };
    self.hasErrors = function () {
        var errorLength = (self.hasStockConsumption())
                            ? self.errorWithStock().length
                            : self.errorWithOutStock().length;
        return errorLength;
    };

    self.objToPost = function() {
        var products = [];
        var data = self.soldProducts();
        $.each(data, function (i) {
            var aProduct = {
                ProductId: data[i].productId(),
                HasStockConsumption: data[i].hasStockConsumption(),
                Quantity: data[i].quantity(),
                Price: data[i].price(),
                StockConsumptions: []
            };
            if (data[i].hasStockConsumption()) {
                aProduct.StockConsumptions.push({ Quantity: data[i].stockQuantity() });
            }
            products.push(aProduct);
        });
        return products;
    };

    /*Gets*/
    self.loadProducts = function () {
        self.products([]);
        $.ajax({
            url: '/SaleManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].UnitName ));
                });
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*local Actions*/
    self.addSold = function(item) {
        var arr = self.soldProducts();
        var options = {
            productId: item.productId(),
            productName: item.productName(),
            productUnit: item.productUnit(),
            quantity: item.quantity(),
            price: item.price(),
            hasStockConsumption: item.hasStockConsumption(),
            stockQuantity: (item.hasStockConsumption()) ? item.stockQuantity() : null,
        };
        arr.push(new SoldProductModel(options));
        self.soldProducts([]);
        self.soldProducts(arr);
    };
    self.create = function (item) {
        self.removeErrors();
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        var add = function(product) {
            self.addSold(product);
            self.reset();
            ToastSuccess('Product added to Sold Product List.');
        };
        
        var addedProducts = $.grep(self.soldProducts(), function (data) {
            return data.productId() === item.productId();
        });
        var alreadyAdded = addedProducts.length != 0;
        if (alreadyAdded) {
            bootbox.confirm("This Product has already been added to Sold Product List, do you still want to add a new one ?", function (result) {
                if (result === true) {
                    add(item);
                }
            });
        } else {
            add(item);
        }
        $('#stack1').modal('hide');
    };
    
    self.delete = function (item) {
        bootbox.confirm("Do you want to delete the Product from Sale ?", function (result) {
            if (result === true) {
                self.soldProducts.remove(item);
                ToastSuccess('Product deleted.');
            }
        });
    };
    
    self.showToUpdate = function (item, event) {
        var context = ko.contextFor(event.target);
        var index = context.$index();       
        self.editIndex(index);
        
        var product = self.soldProducts()[index];
        self.productId(product.productId());
        self.quantity(product.quantity());
        self.price(product.price());
        self.hasStockConsumption(product.hasStockConsumption());
        self.stockQuantity(product.stockQuantity());
        
        $("#divActionCreate").hide();
        $('#btnCloseModal').hide();
        $("#divActionUpdate").show();
        $('#stack1').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    };
    self.update = function (item) {
        self.removeErrors();
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        
        var editIndex = self.editIndex();
        var product = self.soldProducts()[editIndex];
        product.productId(self.productId());
        product.quantity(self.quantity());
        product.price(self.price());
        product.hasStockConsumption(self.hasStockConsumption());
        var stockQuantity = (self.hasStockConsumption()) ? self.stockQuantity() : null;
        product.stockQuantity(stockQuantity);

        $('#stack1').modal('hide');
    };
    self.cancelUpdate = function (item) {
        $('#stack1').modal('hide');
    };

    /*Resets*/
    self.init = function () {
        self.loadProducts();
        self.reset();
        self.resetSoldProducts();
        activeParentMenu($('li.submenu a[href="/SaleManage/Index"]:first'));
    };
    self.reset = function () {
        self.productId('');
        self.quantity('');
        self.price('');
        self.stockQuantity('');
        self.removeErrors();
    };
    self.resetSoldProducts = function () {
        self.soldProducts([]);
    };
}
/*Main ViewModel*/
function ViewModel() {
    var self = this;
    /*main view models call backs*/
    CustomerCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var custName = own.name();
            own.clear();
            self.loadCustomers();
            setTimeout(function () {
                var recentVal = $("#customerList option:contains('" + custName + "')").attr('value');
                $("#customerList").val(recentVal);
                $("#customerList").trigger("change");
            }, 500);
            $('#divCustomerSetup').modal('hide');
            ToastSuccess('Customer has been created successfully');
        }
    };
    ProductCreateVm.prototype.createdCallBack = function (data) {
        var mainView = this;
        if (data == true) {
            var lastProduct = mainView.name();
            mainView.reset();
            $('#stack2').modal('hide');
            $('#stack1').modal('show');
            self.products.loadProducts();
            
            setTimeout(function () {
                var recentVal = $("#sltproduct option:contains('" + lastProduct + "')").attr('value');
                $("#sltproduct").val(recentVal);
                $("#sltproduct").trigger("change");
            }, 500);

            ToastSuccess('Product has been created successfully');
        }
    };

    ProductUnitCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var lastUnit = own.name();
            own.clear();
            $('#stack4').modal('hide');
            $('#stack2').removeData("modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#sltUnit').html("");
            self.productCreate.loadUnits();

            setTimeout(function () {
                var recentVal = $("#sltUnit option:contains('" + lastUnit + "')").attr('value');
                $("#sltUnit").val(recentVal);
                $("#sltUnit").trigger("change");
            }, 500);

            ToastSuccess('Product Unit has been created successfully');
        }
    };

    self.customer = new CustomerCreateVm();
    self.productCreate = new ProductCreateVm();
    self.productUnitCreate = new ProductUnitCreateVm();

    self.customerId = ko.observable('').extend({ required: true });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.products = new SoldProduct();  
    self.customers = ko.observableArray([]);
    
    self.errors = ko.validation.group([self.customerId, self.dateOfCreation]);
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.hasError = function () {
        return self.errors().length;
    };

    self.objToPost = function() {
        var sale = {
            CustomerId: self.customerId(),
            DateOfCreation: self.dateOfCreation()
        };  
        var soldProducts = self.products.objToPost();
        
        return {            
            sale: sale,
            products: soldProducts
        };
    };

    /*Gets*/
    self.loadCustomers = function () {
        self.customers([]);
        $.ajax({
            url: '/SaleManage/GetCustomers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    /*Action*/
    self.create = function() {
        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }
        
        if (self.localCreate() == true) {
            var json = JSON.stringify(self.objToPost());
            $.ajax({
                url: '/SaleManage/TryToCreate',
                dataType: "json",
                type: "POST",
                data: json,
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: false,
                cache: false,
                success: function (data) {               
                    ToastSuccess('Sales created.');
                    self.reset();
                },
                error: function (xhr) {
                    ToastError(xhr);
                }
            });
        }   
    };

    self.localCreate = function() {
        if (self.products.soldProducts().length == 0) {
            bootbox.alert("You have to add \"Product\" to create Sale");
            return false;
        }
        return true;
    };

    /*resets*/
    self.init = function () {
        self.products.init();
        self.loadCustomers();
        self.reset();
    };
    self.reset = function () {
        self.customerId('');
        self.dateOfCreation(currentDate());
        self.products.resetSoldProducts();
        self.removeErrors();
    };
    
    /*Select change events*/
    self.customerId.subscribe(function (newValue) {
        if (typeof (newValue) === 'undefined' || newValue === '') {
            self.removeErrors();
            return;
        }
        self.products.resetSoldProducts([]);
    });

    
}
$(document).ready(function() {

    $('.datepicker').datepicker();
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);

    $("#btnAddCustomer").click(function () {
        viewModel.customer.clear();
        $('#divCustomerSetup').removeData("modal").modal();
    });
    $("#btnAddProduct").click(function () {
        viewModel.productCreate.reset();
        viewModel.productCreate.init();
        $('#stack1').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnCloseModal").click(function () {
        viewModel.products.reset();
        $('#stack1').modal('show');        
    });
    $('#btnAddSoldProduct').click(function () {
        viewModel.products.reset();
        $("#divActionCreate").show();
        $('#btnCloseModal').show();
        $("#divActionUpdate").hide();
        $('#stack1').removeData("modal").modal();
    });
    $("#btnClose3").click(function () {
        viewModel.productUnitCreate.clear();
        $('#stack4').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
});