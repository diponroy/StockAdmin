﻿
function ContactPersonCreateVm() {
    var self = this;

    /*callbacks*/
    self.callback = this.createdCallBack;

    /*fields*/
    self.contactPersonName = ko.observable('').extend({ required: true });
    self.contactPersonNumber = ko.observable('').extend({ maxLength: 50 });
    self.customerId = ko.observable('').extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasError = function () {
        return self.errors().length > 0;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    self.reset = function () {
        self.contactPersonName('');
        self.contactPersonNumber('');
        self.errors.showAllMessages(false);
        return true;
    };

    /*objects to post*/
    self.contactPerson = function () {
        var contactPersonObj = {
            Name: self.contactPersonName(),
            ContactNumber: self.contactPersonNumber(),
            CustomerId: self.customerId()
        };

        return{
            contactPerson: contactPersonObj
        };
    };

    self.create = function () {
        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return true;
        }
        var json = JSON.stringify(self.contactPerson());
        $.ajax({
            url: '/CustomerManage/CreateContactPerson',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (typeof self.callback != 'undefined') {
                    self.callback(data);
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.init = function() {

    };
}