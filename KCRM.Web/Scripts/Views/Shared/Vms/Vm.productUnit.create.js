﻿/*Validations*/
ko.validation.rules['unitNameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/ProductUnitManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Product Unit name is already in use'
};
ko.validation.registerExtenders();

/*View models*/
function ProductUnitCreateVm() {
    var self = this;
    /*callbacks*/
    self.callback = this.createdCallBack;
    /*Fields*/
    self.name = ko.observable('').extend({ required: true, maxLength: 50, unitNameUsed: false });
    self.description = ko.observable('').extend({ maxLength: 50 });
    /*errors*/
    self.errors = ko.validation.group([self.name, self.description]);

/*objects to post*/
    self.productUnit = function () {
        var productUnitObj = {
            Name: self.name(),
            Description: self.description(),
        };
        return productUnitObj;
    };

    /*create or save field values*/
     self.save = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }
        var json = JSON.stringify({ productUnit: self.productUnit() });
        $.ajax({
            url: '/ProductUnitManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    if (typeof self.callback != 'undefined') {
                        self.callback(data);
                    }
                }
                //else {
                //    ToastError("Name is already in use!!!")
                //}
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

/*reset all fields*/
    self.clear = function () {
        self.name(''),
        self.description(''),
        self.errors.showAllMessages(false);
        return true;
    };

/*main function*/
    self.init = function () {
        };
}