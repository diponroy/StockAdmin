﻿
/*Validations*/
ko.validation.rules['customerNameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Customer Name is already in use'
};
ko.validation.registerExtenders();

ko.validation.rules['customerEmailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsEmailUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Customer Email is already in use'
};
ko.validation.registerExtenders();


function CustomerCreateVm() {
    var self = this;
    self.callback = this.createdCallBack;
    self.name = ko.observable('').extend({ required: true, maxLength: 50, customerNameUsed: false });
    self.email = ko.observable('').extend({ required: true, maxLength: 50, email: true, customerEmailUsed: false });
    self.contactNumber = ko.observable('').extend({ maxLength: 50 });
    self.address = ko.observable('').extend({ maxLength: 300 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasErrors = function () {
        return self.errors().length > 0;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    self.getCustomerDetail = function () {

        var customerObj = {
            Name: self.name(),
            Email: self.email(),
            ContactNumber: self.contactNumber(),
            Address: self.address(),
            DateOfCreation: self.dateOfCreation()
        };

        return {
            customer: customerObj,
            contactPersons: null
        };
    };


    /*Actions*/
    self.saveToServer = function () {
        var json = JSON.stringify(self.getCustomerDetail());
        $.ajax({
            url: '/CustomerManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (typeof self.callback != 'undefined') {
                    self.callback(data);
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };


    self.save = function () {
        self.errors.showAllMessages(false);
        if (self.hasErrors()) {
            self.errors.showAllMessages();
            return;
        }

        self.saveToServer();
    };

    self.clear = function () {
        self.name(''),
        self.email(''),
        self.contactNumber(''),
        self.address(''),
        self.dateOfCreation(''),
        self.dateOfCreation(currentDate()),
        self.removeErrors();
        self.errors.showAllMessages(false);
        return true;
    };

    self.init = function () {
    };
}