
﻿/*Validations*/
ko.validation.rules['supplierNameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/SupplierManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Supplier Name has already been used'
};
ko.validation.registerExtenders();


ko.validation.rules['supplierEmailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.when(
            $.ajax({
                url: '/SupplierManage/IsEmailUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Supplier Email has already been used'
};
ko.validation.registerExtenders();

/*view model*/
function SupplierCreateVm() {
    var self = this;
    /*callbacks*/
    self.callback = this.createdCallBack;

    /*fields*/
    self.name = ko.observable('').extend({ required: true, maxLength: 50, supplierNameUsed: false });
    self.email = ko.observable('').extend({ email: true, maxLength: 50, supplierEmailUsed: false });
    self.dateOfCreation = ko.observable(currentDate()).extend({required: true,});
    self.contactNumber = ko.observable().extend({maxLength: 50,});
    self.address = ko.observable('').extend({maxLength: 300,});

    /*errors*/
    self.errors = ko.validation.group(self);
    self.hasError = function() {
        return self.errors().length > 0;
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };

    /*objects to post*/
    self.supplier = function () {
        var obj = {
            Name: self.name(),
            Email: self.email(),
            ContactNumber: self.contactNumber(),
            Address: self.address(),
            DateOfCreation: self.dateOfCreation(),
        };
        return obj;
    };

    self.create = function () {
        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return true;
        }
        var json = JSON.stringify({ supplier: self.supplier() });
        $.ajax({
            url: '/SupplierManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (typeof self.callback != 'undefined') {
                    self.callback(data);
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.reset = function () {
        self.name('');
        self.email('');
        self.contactNumber('');
        self.address('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.removeErrors();
        return true;
    };

    self.init = function () {
    };
}