﻿ko.validation.rules['productNameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/ProductManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Product Name is already in use'
};
ko.validation.registerExtenders();

/*Template viewmodel*/
function ProductCreateVm() {
    var self = this;
    /*callback*/
    self.callback = this.createdCallBack;
    /*Fields*/
    self.name = ko.observable('').extend({ required: true, maxLength: 50, productNameUsed: false });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required : true, });
    self.productCategory = ko.observable().extend({ required : true });
    self.unit = ko.observable().extend({ required : true });
    self.description = ko.observable('').extend({ maxLength : 150 });
    /*errors*/
    self.errors = ko.validation.group(self);
    /*objects to post*/
    self.product = function( ) {
        var productObj = {
            Name : self.name(),
            Category : self.productCategory(),
            ProductUnitId : self.unit(),
            Description : self.description(),
            DateOfCreation : self.dateOfCreation()
        };
        return productObj;
    };
    self.loadCategories = function( ) {
        $.ajax({
            url : '/ProductManage/GetProductCategories',
            dataType : "json",
            type : "POST",
            contentType : 'application/json; charset=utf-8',
            data : "{}",
            async : true,
            processData : false,
            cache : false,
            success : function(data) {
                $('#sltProductCategory').append(new Option('--- Select ---', ''));
                $.each(data, function(i) {
                    $('#sltProductCategory').append(new Option(data[i].Name, data[i].Value));
                });
            },
            error : function(xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.loadUnits = function( ) {
        $.ajax({
            url : '/ProductManage/GetProductUnits',
            dataType : "json",
            type : "POST",
            contentType : 'application/json; charset=utf-8',
            data : "{}",
            async : true,
            processData : false,
            cache : false,
            success : function(data) {
                $('#sltUnit').append(new Option('--- Select ---', ''));
                $.each(data, function(i) {
                    $('#sltUnit').append(new Option(data[i].Name, data[i].Id));
                });
            },
            error : function(xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.create = function( ) {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }
        var json = JSON.stringify({ product : self.product() });
        $.ajax({
            url : '/ProductManage/Create',
            dataType : "json",
            type : "POST",
            contentType : 'application/json; charset=utf-8',
            data : json,
            async : true,
            processData : false,
            cache : false,
            success : function(data) {
                if (data === true) {
                    if (typeof self.callback != 'undefined') {
                        self.callback(data);
                    }
                }
            },
            error : function(xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.reset = function( ) {
        self.name('');
        self.description('');
        self.productCategory('');
        self.unit('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.errors.showAllMessages(false);
        return true;
    };

    self.init = function () {
        $('#sltProductCategory').html("");
        $('#sltUnit').html("");
        self.loadCategories();
        self.loadUnits();
        $("#btnAddProductUnit").click(function () {
            $('#stack2').modal('hide');
            $('#stack4').removeData("modal").modal({
                backdrop: 'static',
                keyboard: false
            });
        });
    };
}


