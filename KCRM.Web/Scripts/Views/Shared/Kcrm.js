﻿/*html templates initialize*/
$(function () {
    infuser.defaults.templateUrl = "/Templates";
    infuser.defaults.templatePrefix = "_";
    infuser.defaults.templateSuffix = ".tmpl.html";
});


/*Date picker for knockout*/
ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var options = allBindingsAccessor().datepickerOptions || {};      
        $(element).datepicker(options).on("changeDate", function (ev) {
            var observable = valueAccessor();
            observable(ev.date);
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).datepicker("setValue", value);
    }
};


/*Datetime issues with moment*/
function clientDate(date) {
    return moment(date);
};

function currentDate() {
    return moment();
};

function dateFromDateString(dateString) {
    return moment(dateString, 'DD-MM-YYYY');
};

function clientDateStringFromDate(date) {
    return moment(date).format('DD-MM-YYYY');
};


/*Toaster notifications
http://codeseven.github.io/toastr/demo.html
*/
function ToastError(error) {
    toastr.options = {
        "closeButton": true,
        "debug": true,
        "positionClass": "toast-top-right",
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.error('There has been an error, please try again', 'Error');
}
function ToastSuccess(message) {
    toastr.options = {
        "closeButton": true,
        "debug": true,
        "positionClass": "toast-top-right",
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.success(message, 'Success');
}



// === Fixes the position of buttons group in content header and top user navigation === //
function fix_position() {
    var uwidth = $('#user-nav > ul').width();
    $('#user-nav > ul').css({ width: uwidth, 'margin-left': '-' + uwidth / 2 + 'px' });

    var cwidth = $('#content-header .btn-group').width();
    $('#content-header .btn-group').css({ width: cwidth, 'margin-left': '-' + uwidth / 2 + 'px' });
}

/*Active main menu*/
function activeParentMenu(object) {
    object.parents('li:eq(1)').addClass('active');
    object.parents('li:eq(1)').addClass('open');
}

$(document).ready(function () {
    
    // === Sidebar navigation === //
    $('.submenu > a').click(function (e) {
        e.preventDefault();

        var submenu = $(this).siblings('ul');
        var li = $(this).parents('li');
        var submenus = $('#sidebar li.submenu ul');
        var submenus_parents = $('#sidebar li.submenu');
        if (li.hasClass('open')) {
            if (($(window).width() > 768) || ($(window).width() < 479)) {
                submenu.slideUp();
            } else {
                submenu.fadeOut(250);
            }
            li.removeClass('open');
            li.removeClass('active');
        } else {
            if (($(window).width() > 768) || ($(window).width() < 479)) {
                submenus.slideUp();
                submenu.slideDown();
            } else {
                submenus.fadeOut(250);
                submenu.fadeIn(250);
            }
            submenus_parents.removeClass('open');
            submenus_parents.removeClass('active');
            li.addClass('open');
        }
    });

    var ul = $('#sidebar > ul');
    $('#sidebar > a').click(function (e) {
        e.preventDefault();
        var sidebar = $('#sidebar');
        if (sidebar.hasClass('open')) {
            sidebar.removeClass('open');
            ul.slideUp(250);
        } else {
            sidebar.addClass('open');
            ul.slideDown(250);
        }
    });

    // === Resize window related === //
    $(window).resize(function () {
        if ($(window).width() > 479) {
            ul.css({ 'display': 'block' });
            $('#content-header .btn-group').css({ width: 'auto' });
        }
        if ($(window).width() < 479) {
            ul.css({ 'display': 'none' });
            fix_position();
        }
        if ($(window).width() > 768) {
            $('#user-nav > ul').css({ width: 'auto', margin: '0' });
            $('#content-header .btn-group').css({ width: 'auto' });
        }
    });

    if ($(window).width() < 468) {
        ul.css({ 'display': 'none' });
        fix_position();
    }

    if ($(window).width() > 479) {
        $('#content-header .btn-group').css({ width: 'auto' });
        ul.css({ 'display': 'block' });
    }

    // === Tooltips === //
    $('.tip').tooltip();
    $('.tip-left').tooltip({ placement: 'left' });
    $('.tip-right').tooltip({ placement: 'right' });
    $('.tip-top').tooltip({ placement: 'top' });
    $('.tip-bottom').tooltip({ placement: 'bottom' });

    // === Search input typeahead === //
    //$('#search input[type=text]').typeahead({
    //    source: ['Dashboard', 'Form elements', 'Common Elements', 'Validation', 'Wizard', 'Buttons', 'Icons', 'Interface elements', 'Support', 'Calendar', 'Gallery', 'Reports', 'Charts', 'Graphs', 'Widgets'],
    //    items: 4
    //});

    // === Style switcher === //
    $('#style-switcher i').click(function () {
        if ($(this).hasClass('open')) {
            $(this).parent().animate({ marginRight: '-=190' });
            $(this).removeClass('open');
        } else {
            $(this).parent().animate({ marginRight: '+=190' });
            $(this).addClass('open');
        }
        $(this).toggleClass('icon-arrow-left');
        $(this).toggleClass('icon-arrow-right');
    });

    $('#style-switcher a').click(function () {
        var style = $(this).attr('href').replace('#', '');
        $('.skin-color').attr('href', 'css/maruti.' + style + '.css');
        $(this).siblings('a').css({ 'border-color': 'transparent' });
        $(this).css({ 'border-color': '#aaaaaa' });
    });

    $(document)
    .ajaxStart(function () {
        $('#divSpinner').fadeIn();
        //$('#divPageContainer').block({
        //    message: '<h1>Processing</h1>'
        //});
    })
    .ajaxStop(function () {
        $('#divSpinner').fadeOut();
        //$('#divPageContainer').unblock();
    });
});





