﻿function Product(id, name, unit) {
    this.id = id;
    this.name = name;
    this.unit = unit;
}

function ProductStock(name, unit, quantity) {
    this.name = name;
    this.unit = unit;
    this.quantity = quantity;
}

/*main View models*/
function ViewModel() {
    var self = this;
    /*main view models call backs*/
    ProductCreateVm.prototype.createdCallBack = function (data) {
        var productMainView = this;
        if (data == true) {
            var lastProduct = productMainView.name();
            productMainView.reset();
            $('#stack2').modal('hide');
            //$('#stack1').modal('show');
            self.loadProducts();

            setTimeout(function () {
                var recentVal = $("#sltproduct option:contains('" + lastProduct + "')").attr('value');
                $("#sltproduct").val(recentVal);
                $("#sltproduct").trigger("change");
            }, 500);

            ToastSuccess('Product has been created successfully');
        }
    };
    ProductUnitCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var lastUnit = own.name();
            own.clear();
            $('#stack4').modal('hide');
            $('#stack2').removeData("modal").modal({
                backdrop: 'remove',
                keyboard: false
            });
            $('#sltUnit').html("");
            self.productCreate.loadUnits();

            setTimeout(function () {
                var recentVal = $("#sltUnit option:contains('" + lastUnit + "')").attr('value');
                $("#sltUnit").val(recentVal);
                $("#sltUnit").trigger("change");
            }, 500);

            ToastSuccess('Product Unit has been created successfully');
        }
    };
    
    self.productCreate = new ProductCreateVm();
    self.productUnitCreate = new ProductUnitCreateVm();
    self.productId = ko.observable().extend({ required: true });
    self.protuctUnit = ko.observable('');
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.quantity = ko.observable().extend({ required: true, min: 1 , digit:true});
    
    self.products = ko.observableArray([]);
    self.productStocks = ko.observableArray([]);

    /*Errors*/
    self.errors = ko.validation.group([self.productId, self.quantity]);
    self.removeErrors = function() {
        self.errors.showAllMessages(false);
    };
    self.showErrors = function() {
        self.errors.showAllMessages();
    };
    self.hasErrors = function() {
        return self.errors().length;
    };

    self.objToPost = function() {
        return {            
            ProductId: self.productId(),
            Quantity: self.quantity(),
            DateOfCreation: self.dateOfCreation()
        };
    };

    /*Resets*/
    self.init = function() {
        self.loadProducts();
        self.loadProductStocks();
        activeParentMenu($('li.submenu a[href="/StockManage/Index"]:first'));
    };
    self.reset = function () {
        self.productId('');
        self.protuctUnit('');
        self.quantity('');
        self.dateOfCreation(currentDate());
        self.removeErrors();
    };

    /*Gets*/
    self.loadProducts = function() {
        self.products([]);
        $.ajax({
            url: '/StockManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].UnitName));
                });                
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };  
    self.loadProductStocks = function () {
        self.productStocks([]);
        $.ajax({
            url: '/StockManage/GetCurrentStocks',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new ProductStock(data[i].Name, data[i].UnitName, data[i].AvailableQutity));
                });
                self.productStocks(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*Actions*/
    self.create = function () {
        self.removeErrors();
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/StockManage/TryToCreate',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.reset();
                    self.loadProductStocks();                    
                    ToastSuccess('Stock added successfully');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*Local Actions*/
    self.createDone = function () {
        $('#stack1').slideUp('slow');
        $('#btnAddStock').hide().slideDown('slow');
    };

    /*Select changes*/
    //self.productId.subscribe(function (newValue) {
    //    self.reset();
    //    if (typeof newValue === "undefined" || $.trim(newValue) === '') {
    //        return;
    //    }
    //});
}


$(document).ready(function () {
    /*template call*/
    $('.datepicker').datepicker();

    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false
    });

    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    
    $("#btnAddProduct").click(function () {
        viewModel.productCreate.reset();
        viewModel.productCreate.init();
        $('#stack2').removeData("modal").modal();
    });
    $("#btnClose3").click(function () {
        viewModel.productUnitCreate.clear();
        $('#stack4').modal('hide');
        $('#stack2').removeData("modal").modal();
    });
    $('#btnAddStock').click(function () {
        viewModel.reset();
        $('#stack1').hide().slideDown('slow');
        $(this).slideUp('slow');
    });
});