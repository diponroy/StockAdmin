﻿function ViewModel() {

    var self = this;
    self.loginName = ko.observable('admin').extend({ required: true, maxLength: 50, });
    self.password = ko.observable('admin').extend({ required: true, maxLength: 50, });
    self.loginErrors = ko.validation.group([self.loginName, self.password]);

    self.recoverLoginName = ko.observable('admin').extend({ required: true, maxLength: 50, });
    self.email = ko.observable('').extend({ required: true, maxLength: 50, email: true });
    self.recoverErrors = ko.validation.group([self.recoverLoginName, self.email]);

    self.loginDetail = function() {
        var obj = {
            loginName: self.loginName(),
            password: self.password()
        };
        return obj;
    };
    
    self.recoverDetail = function () {
        var obj = {
            loginName: self.recoverLoginName(),
            email: self.email()
        };
        return obj;
    };

   self.login = function () {
        self.loginErrors.showAllMessages(false);
        if (self.loginErrors().length > 0) {
            self.loginErrors.showAllMessages();
            return true;
        }

       //Spinner
        $('#divSpinner').fadeIn();

        var json = JSON.stringify(self.loginDetail());
        
        $.ajax({
            url: '/LoginManage/TryToCreateSession',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                //Spinner
                $('#divSpinner').fadeOut();

                if (data === true) {
                    self.resetLoginDetail();
                    window.location = '/Dashboard/All';
                } else {
                    bootbox.alert("Login Name or Password do not match");
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };
    
    self.recover = function () {
        self.recoverErrors.showAllMessages(false);
        if (self.recoverErrors().length > 0) {
            self.recoverErrors.showAllMessages();
            return true;
        }

        //Spinner
        $('#divSpinner').fadeIn();

        var json = JSON.stringify(self.recoverDetail());
        $.ajax({
            url: '/LoginManage/TryToRecover',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                //Spinner
                $('#divSpinner').fadeOut();

                if (data === true) {
                    self.resetRecoverDetail();
                    bootbox.alert("New password has been sent to your email.");
                } else {
                    bootbox.alert("Login Name or Email do not match");

                } 
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.init = function() {
        self.resetLoginDetail();
        self.resetRecoverDetail();
    };
    self.resetLoginDetail = function() {
        self.loginName('admin');
        self.password('admin');
        self.loginErrors.showAllMessages(false);
    };  
    self.resetRecoverDetail = function () {
        self.recoverLoginName('admin');
        self.email('diponsust.kcrm');
        self.recoverErrors.showAllMessages(false);
    };
}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    
    $('#btnRecover').click(function () {
        viewModel.resetRecoverDetail();
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
    $('#btnBackToLogin').click(function () {
        viewModel.resetLoginDetail();
        $("#recoverform").hide();
        $("#loginform").fadeIn();
    });

    $(document).keypress(function (e) {
        if (e.keyCode == 13) {
            if ($('#divLogin').is(":visible")) {
                $("#btnLogin").trigger("click");
            } else {
                $("#btnRecoverPassword").trigger("click");
            }
        }
    });
});

