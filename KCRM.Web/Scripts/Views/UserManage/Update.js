﻿
/*Validations*/
ko.validation.rules['loginNameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id : $('#txtUserId').val(), loginName: val });
        $.when(
            $.ajax({
                url: '/UserManage/IsLoginNameUsedExceptUser',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Login Name is already in use'
};
ko.validation.registerExtenders();


ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#txtUserId').val(), email: val });
        $.when(
            $.ajax({
                url: '/UserManage/IsEmailExceptUser',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This User Email is already in use'
};
ko.validation.registerExtenders();


function UserViewModel() {

    /*Fields*/
    var self = this;

    self.id = ko.observable('').extend({
        required: true,
    });
    self.loginName = ko.observable('').extend({
        required: true,
        maxLength: 50,
        loginNameUsed: false
    });
    self.password = ko.observable('').extend({
        maxLength: 50,
    });
    self.dateOfCreation = ko.observable(currentDate()).extend({
        required: true,
    });
    self.userType = ko.observable('').extend({
        required: true,
    });
    self.firsName = ko.observable('').extend({
        maxLength: 50,
    });
    self.lastName = ko.observable('').extend({
        maxLength: 50,
    });
    self.contactNumber = ko.observable('').extend({
        maxLength: 50,
    });
    self.address = ko.observable('').extend({
        maxLength: 300,
    });
    self.email = ko.observable('').extend({
        required: true,
        email: true,
        maxLength: 50,
        emailUsed: false
    });


    self.errors = ko.validation.group(self);

    self.user = function () {
        var userObj = {
            Id: self.id(),
            FirstName: self.firsName(),
            LastName: self.lastName(),
            LoginName: self.loginName(),
            Password: self.password(),
            ContactNumber: self.contactNumber(),
            UserType: self.userType(),
            Address: self.address(),
            Email: self.email(),
            DateOfCreation: self.dateOfCreation(),
        };
        return userObj;
    };


    self.update = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        var json = JSON.stringify({ user: self.user() });
        $.ajax({
            url: '/UserManage/Update',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.reset();
                    ToastSuccess('User updated successfully');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.loadUserTypes = function () {
        $.ajax({
            url: '/UserManage/GetUserTypes',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: "{}",
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                $('#sltUserType').append(new Option('--- Chosse ---', ''));
                $.each(data, function (i) {
                    $('#sltUserType').append(new Option(data[i].Name, data[i].Value));
                });

                self.load();
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.load = function () {
        var json = JSON.stringify({ id: self.id() });
        $.ajax({
            url: '/UserManage/Get',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.firsName(data.FirstName);
                self.lastName(data.LastName),
                self.loginName(data.LoginName);
                self.email(data.Email);
                self.password('');
                self.contactNumber(data.ContactNumber);
                self.userType(data.UserType);
                self.address(data.Address);
                self.dateOfCreation('');
                self.dateOfCreation(clientDate(data.DateOfCreation));
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.reset = function () {
        self.load();
    };
    self.init = function() {
        self.id($('#txtUserId').val());
        self.loadUserTypes();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}



$(document).ready(function () {
    $('.datepicker').datepicker();

    var viewModel = new UserViewModel();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    viewModel.init();
});
