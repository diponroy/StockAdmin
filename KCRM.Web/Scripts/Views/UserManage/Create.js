﻿/*Validations*/
ko.validation.rules['loginNameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ loginName: val });
        $.when(
            $.ajax({
                url: '/UserManage/IsLoginNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Login Name is already in use'
};
ko.validation.registerExtenders();


ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.when(
            $.ajax({
                url: '/UserManage/IsEmailUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This User Email is already in use'
};
ko.validation.registerExtenders();


function UserViewModel() {

    /*Fields*/
    var self = this;

    self.loginName = ko.observable('').extend({
        required: true,
        maxLength: 50,
        loginNameUsed: false
    });
    self.password = ko.observable('').extend({
        required: true,
        maxLength: 50,
    });
    self.dateOfCreation = ko.observable(currentDate()).extend({
        required: true,
    });
    self.userType = ko.observable().extend({
        required: true,
    });
    self.firsName = ko.observable('').extend({
        maxLength: 50,
    });
    self.lastName = ko.observable('').extend({
        maxLength: 50,
    });
    self.contactNumber = ko.observable('').extend({
        maxLength: 50,
    });
    self.address = ko.observable('').extend({
        maxLength: 300,
    });
    self.email = ko.observable('').extend({
        required: true,
        email: true,
        maxLength: 50,
        emailUsed: false
    });


    self.errors = ko.validation.group(self);

    self.user = function() {
        var userObj = {
            FirstName: self.firsName(),
            LastName: self.lastName(),
            LoginName: self.loginName(),
            Password: self.password(),
            ContactNumber: self.contactNumber(),
            UserType: self.userType(),
            Address: self.address(),
            Email: self.email(),
            DateOfCreation: self.dateOfCreation(),
        };
        return userObj;
    };


    self.loadUserTypes = function () {
        $.ajax({
            url: '/UserManage/GetUserTypes',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: "{}",
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                $('#sltUserType').append(new Option('--- Chosse ---', ''));
                $.each(data, function (i) {
                    $('#sltUserType').append(new Option(data[i].Name, data[i].Value));
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.create = function() {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        var json = JSON.stringify({ user: self.user() });
        $.ajax({
            url: '/UserManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {                
                if (data === true) {
                    self.reset();
                    ToastSuccess("User created succesfully");
                }
            },
            error: function(xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.reset = function () {
        self.firsName('');
        self.lastName('');
        self.loginName('');
        self.password('');
        self.contactNumber('');
        self.userType('');
        self.address('');
        self.email('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.errors.showAllMessages(false);
        return true;
    };
    
    self.init = function () {
        self.loadUserTypes();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}



$(document).ready(function () {
    $('.datepicker').datepicker();   
    var viewModel = new UserViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
    ko.validatedObservable(viewModel);
});
