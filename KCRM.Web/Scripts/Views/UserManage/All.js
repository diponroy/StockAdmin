﻿

function User(id, loginName, userType, firstName, lastName, contactNumber, dateOfCreation, email) {
    this.id = ko.observable(id);
    this.loginName = ko.observable(loginName);
    this.userType = ko.observable(userType);
    this.firstName = ko.observable(firstName);
    this.lastName = ko.observable(lastName);
    this.contactNumber = ko.observable(contactNumber);
    this.dateOfCreation = ko.observable(dateOfCreation);
    this.email = ko.observable(email);
    this.updateUrl = ko.computed(function () {
        return '/UserManage/Update/' + id;
    }, this);
    this.deleteUrl = ko.computed(function () {
        return '/UserManage/Delete';
    }, this);
}



function ViewModel() {

    var self = this;
    self.users = ko.observableArray([]);
    
    self.load = function() {
        $.ajax({
            url: '/UserManage/GetAll',
            dataType: "json",
            contentType: 'application/json',
            type: "GET",
            success: function(data) {
                var arr = [];
                $.each(data, function(i) {
                    arr.push(new User(data[i].Id, data[i].LoginName, data[i].UserTypeString, data[i].FirstName, data[i].LastName, data[i].ContactNumber, clientDateStringFromDate(data[i].DateOfCreation), data[i].Email));
                });
                self.users(arr);
            },
            error: function(xhr) {
                ToastError(xhr);
            }
        });
    };
    
    self.deleteUser = function(item) {
        bootbox.confirm("Are you sure, you want to delete the User ?", function(result) {
            if (result === true) {
                var json = JSON.stringify({
                    id: item.id(),
                });
                $.ajax({
                    url: item.deleteUrl(),
                    dataType: "json",
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    data: json,
                    async: true,
                    processData: false,
                    cache: false,
                    success: function(data) {
                        if (data === true) {
                            self.load();
                            ToastSuccess("User deleted successfully");
                        }
                    },
                    error: function(xhr) {
                        ToastError(xhr);
                    }
                });
            }
        });
        return true;
    };

    self.init = function() {
        self.load();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
};


$(document).ready(function () {    
    //$('.data-table').dataTable.order.sSortDescending({
    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
    });
    
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});