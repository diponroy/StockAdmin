﻿/*Model*/
function Product(id, name) {
    this.id = id;
    this.name = name;
}
function ProductPrice(id, name, price) {
    this.id = id;
    this.name = name;
    this.price = price;
}

/*View models*/
function ViewModel() {
    var self = this;
    self.productId = ko.observable().extend({ required: true });
    self.price = ko.observable().extend({ required: true, min: 0 });
    self.products = ko.observableArray([]);
    self.productPrices = ko.observableArray([]);
    
    /*errors*/
    self.erros = ko.validation.group([self.productId, self.price]);
    self.removeErrors = function () {
        self.erros.showAllMessages(false);
    };
    self.showErrors = function () {
        self.erros.showAllMessages();
    };
    self.hasErrors = function () {
        return self.erros().length;
    };

    self.objToPost = function () {
        return {
            ProductId: self.productId(),
            Price: self.price()
        };
    };

    /*Get*/
    self.loadProducts = function () {
        self.products([]);
        $.ajax({
            url: '/StockPriceManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name,data[i].UnitName));
                });
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.loadProductPrices = function () {
        self.productPrices([]);
        $.ajax({
            url: '/StockPriceManage/GetStockPrices',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    var price = (parseInt(data[i].Price) === 0) ? 'Price not yet set' : data[i].Price;
                    arr.push(new ProductPrice(data[i].Id, data[i].ProductName, price));
                });
                self.productPrices(arr);
                },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.showToUpdate = function (item) {
        self.reset();
        if ($('#divAddStockPrice').is(":hidden")) {
            $('#divAddStockPrice').hide().slideDown('slow');         
        }
      
        $.ajax({
            url: '/StockPriceManage/GetPrice/' + item.id,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.productId(item.id);
                self.price(data.Price);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };


    /*local Action*/
    self.updateDone = function () {
        $('#divAddStockPrice').slideUp('slow');
    };

    /*Action*/
        
    self.update = function () {
        self.removeErrors();
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/StockPriceManage/TryToUpdate',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.loadProductPrices();
                    self.reset();
                    ToastSuccess('Stock Product Price updated succesfully');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*reset*/
    self.init = function () {
        self.loadProducts();
        self.loadProductPrices();
        activeParentMenu($('li.submenu a[href="/StockPriceManage/Index"]:first'));
    };
    self.reset = function () {
        self.productId('');
        self.price('');
        self.removeErrors();
    };

    /*select change events*/
    self.productId.subscribe(function (newValue) {
        if (typeof (newValue) === 'undefined' || newValue === '') {
            self.price('');
            self.removeErrors();
            return;
        }
    });
}

$(document).ready(function () {
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});