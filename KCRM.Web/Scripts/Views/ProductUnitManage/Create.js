﻿
/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/ProductUnitManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Product Unit name is already in use'
};
ko.validation.registerExtenders();


function ProductUnitViewModel() {
    var self = this;
    self.name = ko.observable('').extend({ required: true, maxLength: 50, nameUsed: false });
    self.description = ko.observable('').extend({ maxLength: 50 });

    self.errors = ko.validation.group(self);

    self.productUnit = function () {
        var productUnitObj = {
            Name: self.name(),
            Description: self.description(),
        };
        return productUnitObj;
    };

    self.save = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        var json = JSON.stringify({ productUnit: self.productUnit() });
        $.ajax({
            url: '/ProductUnitManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.clear();
                    ToastSuccess("Product Unit created successfully");
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.clear = function () {
        self.name(''),
        self.description(''),
        self.errors.showAllMessages(false);
        return true;
    };
    self.init = function() {
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}

$(document).ready(function () {

    $('.datepicker').datepicker();

    var viewModel = new ProductUnitViewModel();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    viewModel.init();
});
