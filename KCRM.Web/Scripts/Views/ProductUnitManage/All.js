﻿
function ProductUnit(id, name, description) {

    this.id = ko.observable(id);
    this.name = ko.observable(name);
    this.description = ko.observable(description);
    this.updateUrl = ko.computed(function () {
        return '/ProductUnitManage/Update/' + id;
    }, this);
    this.deleteUrl = ko.computed(function () {
        return '/ProductUnitManage/Delete';
    }, this);
}


function ViewModel() {

    var self = this;

    self.productUnits = ko.observableArray([]),

    self.load = function () {
        $.ajax({
            url: '/ProductUnitManage/GetAll',
            dataType: "json",
            contentType: 'application/json',
            type: "GET",
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new ProductUnit(data[i].Id, data[i].Name, data[i].Description));
                });
                self.productUnits(arr);

                $('.data-table').dataTable({
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "sDom": '<""l>t<"F"fp>',
                    "bFilter": false,
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };


    self.deleteProductUnit = function (item) {

        bootbox.confirm("Are you sure, you want to delete the Product Unit ?", function (result) {

            if (result === true) {
                var json = JSON.stringify({
                    id: item.id(),
                });
                $.ajax({
                    url: item.deleteUrl(),
                    dataType: "json",
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    data: json,
                    async: true,
                    processData: false,
                    cache: false,
                    success: function (data) {

                        if (data === true) {
                            bootbox.alert("Product Unit deleted successfully");
                        }
                    },
                    error: function (xhr) {
                        ToastError(xhr);
                    }
                });
            }
        });
        return true;
    };

    self.init = function() {
        self.load();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}

$(document).ready(function () {

    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});