﻿
function Product(id, name, category, unitType, description, dateOfCreation) {
    this.id = ko.observable(id);
    this.name = ko.observable(name);
    this.category = ko.observable(category);
    this.unitType = ko.observable(unitType);
    this.dateOfCreation = ko.observable(dateOfCreation);
    this.description = ko.observable(description);
    this.updateUrl = ko.computed(function () {
        return '/ProductManage/Update/' + id;
    }, this);
}

function ViewModel() {
    var self = this;
    self.products = ko.observableArray([]);

    self.load = function () {
        $.ajax({
            url: '/ProductManage/GetAll',
            dataType: "json",
            contentType: 'application/json',
            type: "GET",
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].CategoryName, data[i].UnitName, data[i].Description, clientDateStringFromDate(data[i].DateOfCreation)));
                });
                self.products(arr);

                $('.data-table').dataTable({
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "sDom": '<""l>t<"F"fp>',
                    "bFilter": false,
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    self.init = function () {
        self.load();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
};


$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});