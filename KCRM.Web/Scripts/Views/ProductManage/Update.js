﻿
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#txtProductId').val(), name: val });
        $.when(
            $.ajax({
                url: '/ProductManage/IsNameUsedExceptProduct',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Product Name is already in use'
};
ko.validation.registerExtenders();


function ProductViewModel() {

    var self = this;

    self.id = ko.observable('').extend({
        required: true,
    });
    self.name = ko.observable('').extend({
        required: true,
        maxLength: 50,
        nameUsed: false
    });

    self.dateOfCreation = ko.observable('').extend({
        required: true,
    });
    
    self.category = ko.observable('').extend({
        required: true,
    });

    self.unitType = ko.observable('').extend({
        requird: true,
    });

    self.description = ko.observable('').extend({
        maxLength: 150
    });
   

    self.errors = ko.validation.group(self);

    self.product = function () {
        
        var productObj = {
            Id: self.id(),
            Name: self.name(),
            Category: self.category(),
            UnitName: self.unitType(),
            Description: self.description(),
            DateOfCreation: self.dateOfCreation()
        };
        return productObj;
    };


    self.update = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }
        var json = JSON.stringify({ product: self.product() });
        $.ajax({
            url: '/ProductManage/Update',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.reset();
                    ToastSuccess('Product updated succesfully');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.loadCategories = function () {
        $.ajax({
            url: '/ProductManage/GetProductCategories',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: "{}",
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                $('#sltProductCategory').append(new Option('--- Select ---', ''));
                $.each(data, function (i) {
                    $('#sltProductCategory').append(new Option(data[i].Name, data[i].Value));
                });

                self.load();
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.load = function () {
        var json = JSON.stringify({ id: self.id() });
        $.ajax({
            url: '/ProductManage/Get',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.name(data.Name),
                self.category(data.Category),
                self.unitType(data.UnitName),
                self.description(data.Description),
                self.dateOfCreation(''),
                self.dateOfCreation(clientDate(data.DateOfCreation));
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.reset = function () {
        self.load();
    };
    self.init = function () {
        self.id($('#txtProductId').val());
        self.loadCategories();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}


$(document).ready(function () {

    $('.datepicker').datepicker();

    var viewModel = new ProductViewModel();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    viewModel.init();
});