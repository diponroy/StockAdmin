﻿function Product(id, name, unit) {
    this.id = id;
    this.name = name;
    this.unit = unit;
}
function Supplier(id, name) {
    this.id = id;
    this.name = name;
}
function Customer(id, name) {
    this.id = id;
    this.name = name;
}
function User(id, loginName) {
    this.id = id;
    this.loginName = loginName;
}
function PurchasedProduct(productId, productName, productUnit, quantity, price) {
    this.productId = ko.observable(productId);
    this.productName = ko.observable(productName);
    this.productUnit = ko.observable(productUnit);
    this.quantity = ko.observable(quantity);
    this.price = ko.observable(price);
}

function PurchasedProductViewModel() {
    var self = this;
    self.productId = ko.observable().extend({ required: true });
    self.productName = ko.observable().extend({ });
    self.productUnit = ko.observable().extend({ });
    self.quantity = ko.observable().extend({ required: true, digit: true });
    self.price = ko.observable().extend({ required: true, number: true });
    self.errors = ko.validation.group(self);
    /*old logs*/
    self.oldProductId = ko.observable('');
    self.oldProductName = ko.observable('');
    self.oldProductUnit = ko.observable('');
    self.oldQuantity = ko.observable('');
    self.oldPrice = ko.observable('');
    self.reset = function() {
        self.productId('');
        self.quantity('');
        self.price('');
        self.oldProductId('');
        self.oldQuantity('');
        self.oldPrice('');
        self.errors.showAllMessages(false);
        return true;
    };
    self.show = function(item) {
        self.reset();
        self.productId(item.productId());
        self.quantity(item.quantity());
        self.price(item.price());
        
        self.oldProductId(item.productId());
        self.oldQuantity(item.quantity());
        self.oldPrice(item.price());
    };
    self.resetToOld = function () {
        self.productId(self.oldProductId());
        self.quantity(self.oldQuantity());
        self.price(self.oldPrice());
    };
}
/*main view model*/
function ViewModel() {
    var self = this;
    /*main view models call backs*/
    SupplierCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            self.loadSuppliers();
            var lastSupplier = own.name();
            own.reset();
            setTimeout(function () {
                var recentVal = $("#supplierList option:contains('" + lastSupplier + "')").attr('value');
                $("#supplierList").val(recentVal);
                $("#supplierList").trigger("change");
            }, 500);
            ToastSuccess('Supplier has been created successfully');
            $('#divSupplierSetup').modal('hide');
        }
    };
    CustomerCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            self.loadCustomers();
            var custName = own.name();
            own.clear();
            setTimeout(function () {
                var recentVal = $("#customerList option:contains('" + custName + "')").attr('value');
                $("#customerList").val(recentVal);
                $("#customerList").trigger("change");
            }, 500);

            $('#divCustomerSetup').modal('hide');
            ToastSuccess('Customer has been created successfully');          
        }
    };
    PurchaserCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            self.loadUsers();
            var lastPurchaser = own.loginName();
            own.reset();

            setTimeout(function () {
                var recentVal = $("#inspectorList option:contains('" + lastPurchaser + "')").attr('value');
                $("#inspectorList").val(recentVal);
                $("#inspectorList").trigger("change");
            }, 500);

            $('#divPurchaserSetup').modal('hide');
            ToastSuccess('Purchaser has been created successfully');         
        }
    };
    ProductCreateVm.prototype.createdCallBack = function (data) {
        var productMainView = this;
        if (data == true) {
            self.loadProducts();
            var lastProduct = productMainView.name();
            productMainView.reset();
            ToastSuccess('Product has been created successfully');
            $('#stack2').modal('hide');
            $('#stack1').modal('show');
            setTimeout(function( ) {
                var recentVal = $("#sltproduct option:contains('" + lastProduct + "')").attr('value');
                $("#sltproduct").val(recentVal);
                $("#sltproduct").trigger("change");
            }, 500);
        }
    };
    ProductUnitCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var lastUnit = own.name();
            own.clear();
            $('#stack4').modal('hide');
            $('#stack2').removeData("modal").modal();
            $('#sltUnit').html("");
            self.productCreate.loadUnits();
            setTimeout(function () {
                var recentVal = $("#sltUnit option:contains('" + lastUnit + "')").attr('value');
                $("#sltUnit").val(recentVal);
                $("#sltUnit").trigger("change");
            }, 500);

            ToastSuccess('Product Unit has been created successfully');
        }
    };

    self.purchasedProduct = new PurchasedProductViewModel();
    self.supplierCreate = new SupplierCreateVm();
    self.customer = new CustomerCreateVm();
    self.purchaser = new PurchaserCreateVm();
    self.productCreate = new ProductCreateVm();
    self.productUnitCreate = new ProductUnitCreateVm();

    self.supplierId = ko.observable().extend({ required: true });
    self.customerId = ko.observable().extend({ required: true });
    self.userId = ko.observable().extend({ required: true });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.purchasedProducts = ko.observableArray([]);
    self.totalPrice = ko.computed(function() {
        var totalPrice = 0;
        var products = self.purchasedProducts();
        for (var i in products) {
            totalPrice += parseFloat(products[i].price());
        }
        return parseFloat(totalPrice).toFixed(2);
    }, this);
    self.customers = ko.observableArray([]);
    self.users = ko.observableArray([]);
    self.products = ko.observableArray([]);
    self.suppliers = ko.observableArray([]);
    /*errors*/
    self.errors = ko.validation.group([self.supplierId, self.customerId, self.userId, self.dateOfCreation]);
    self.removeErrors = function() {
        self.errors.showAllMessages(false);
    };
    
    /*Gets*/
    self.loadCustomers = function () {
        self.customers([]);
        $.ajax({
            url: '/PurchaseManage/GetCustomers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.loadProducts = function () {
        self.products([]);
        $.ajax({
            url: '/PurchaseManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].UnitName));
                });
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.loadUsers = function () {
        self.users([]);
        $.ajax({
            url: '/PurchaseManage/GetUsers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new User(data[i].Id, data[i].LoginName));
                });
                self.users(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.loadSuppliers = function () {
        self.suppliers([]);
        $.ajax({
            url: '/PurchaseManage/GetSuppliers/',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Supplier(data[i].Id, data[i].Name));
                });
                self.suppliers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;

    };
    self.getPurchaseDetail = function () {
        var purchase = {
            SupplierId: self.supplierId(),
            CustomerId: self.customerId(),
            PurchaseByUserId: self.userId(),
            DateOfCreation: self.dateOfCreation()
        };

        var purchasedProductObj = [];
        var purchasedProducts = self.purchasedProducts();
        for (i in purchasedProducts) {
            purchasedProductObj.push({
                ProductId: purchasedProducts[i].productId(),
                Quantity: purchasedProducts[i].quantity(),
                Price: purchasedProducts[i].price()
            });
        }

        return {
            purchase: purchase,
            purchasedProducts: purchasedProductObj
        };
    };
    self.getProduct = function (id) {
        var products = self.products();
        var product = null;
        for (var i = 0; i < products.length; i++) {
            if (products[i].id === id) {
                product = products[i];
                break;
            }
        }
        return product;
    };

    /*local Actions*/
    self.createPurchasedProduct = function (item) {
        self.purchasedProduct.errors.showAllMessages(false);
        if (self.purchasedProduct.errors().length > 0) {
            self.purchasedProduct.errors.showAllMessages();
            return;
        }
        self.addPurchasedProduct(item);
        self.purchasedProduct.reset();
        ToastSuccess('Purchased product added to Purchase');
        $('#stack1').modal('hide');
    };
    self.addPurchasedProduct = function (item) {
        var arr = self.purchasedProducts();
        var product = self.getProduct(item.productId());
        arr.push(new PurchasedProduct(item.productId(), product.name, product.unit, item.quantity(), item.price()));
        self.purchasedProducts([]);
        self.purchasedProducts(arr);
    };
    self.updatePurchasedProduct = function (item) {
        self.purchasedProduct.errors.showAllMessages(false);
        if (self.purchasedProduct.errors().length > 0) {
            self.purchasedProduct.errors.showAllMessages();
            return;
        }
        self.addPurchasedProduct(item);
        self.purchasedProduct.reset();

        ToastSuccess('Purchased Product updated and added to Purchase');
        $('#stack1').modal('hide');
    };
    self.cancelPurchasedProductUpdate = function (item) {
        self.purchasedProduct.resetToOld();
        self.purchasedProduct.errors.showAllMessages(false);
        if (self.purchasedProduct.errors().length > 0) {
            self.purchasedProduct.errors.showAllMessages();
            return;
        }
        self.addPurchasedProduct(self.purchasedProduct);
        self.purchasedProduct.reset();
        $('#stack1').modal('hide');
    };
    self.showToUpdate = function (item) {
        self.purchasedProduct.show(item);
        self.purchasedProducts.remove(item);

        $("#divActionCreate").hide();
        $('#btnCloseModal').hide();
        $("#divActionUpdate").show();
        $('#stack1').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
        return true;
    };
    self.removePurchasedProduct = function (item) {
        bootbox.confirm("Do you want to delete the Purchased Product ?", function (result) {
            if (result === true) {
                self.purchasedProducts.remove(item);
                ToastSuccess('Purchased product deleted.');
            }
        });
    };
    
    /*Actions*/
    self.create = function () {
        self.removeErrors();
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }
        
        if (self.localCreate() == true) {
            var json = JSON.stringify(self.getPurchaseDetail());
            $.ajax({
                url: '/PurchaseManage/TryToCreate',
                dataType: "json",
                type: "POST",
                data: json,
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    if (data === true) {
                        ToastSuccess('Purchase created successfully');
                        self.reset();
                    }
                },
                error: function (xhr) {
                    ToastError(xhr);
                }
            });
        }
        return true;
    };
    self.localCreate = function() {
        if (self.purchasedProducts().length == 0) {
            bootbox.alert("You have to add \"Purchased Product\" to create Purchase");
            return false;
        }
        return true;
    };
    self.init = function () {
        self.loadCustomers();
        self.loadSuppliers();
        self.loadUsers();
        self.loadProducts();
        activeParentMenu($('li.submenu a[href="/PurchaseManage/Index"]:first'));
    };
    self.reset = function () {
        self.supplierId('');
        self.customerId('');
        self.userId('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.purchasedProducts([]);
        self.loadCustomers();
        self.loadSuppliers();
        self.loadUsers();
        self.removeErrors();
        return true;
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();
    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false
    });

    var viewModel = new ViewModel();
    viewModel.init();

    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);

    $("#btnAddContactPerson").click(function () {
        $("#divActionCreate").show();
        $('#btnCloseModal').show();
        $("#divActionUpdate").hide();
        $('#divContactPerson').removeData("modal").modal();
    });
    $("#btnAddSupplier").click(function () {
        viewModel.supplierCreate.reset();
        viewModel.supplierCreate.init();
        $('#divSupplierSetup').removeData("modal").modal();
    });
    $("#btnAddCustomer").click(function () {
        viewModel.customer.clear();
        viewModel.customer.init();
        $('#divCustomerSetup').removeData("modal").modal();
    });
    $("#btnAddPurchaser").click(function () {
        viewModel.purchaser.reset();
        viewModel.purchaser.init();
        $('#divPurchaserSetup').removeData("modal").modal();
    });
    $("#btnAddProduct").click(function () {
        viewModel.productCreate.reset();
        viewModel.productCreate.init();
        $('#stack1').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnClose1").click(function () {
        $('#stack1').removeData("modal").modal();

    });
    $("#btnClose3").click(function () {
        viewModel.productUnitCreate.clear();
        $('#stack4').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnAddPurchasedProduct").click(function () {
        viewModel.productCreate.reset();
        viewModel.purchasedProduct.reset();
        $("#divActionCreate").show();
        $('#btnCloseModal').show();
        $("#divActionUpdate").hide();
        $('#stack1').removeData("modal").modal();
    });
});