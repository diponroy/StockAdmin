﻿
/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Customer Name is already in use'
};
ko.validation.registerExtenders();

ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsEmailUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Customer Email is already in use'
};
ko.validation.registerExtenders();


function ContactPerson(contactPersonName, contactPersonNumber) {
    this.contactPersonName = ko.observable(contactPersonName);
    this.contactPersonNumber = ko.observable(contactPersonNumber);
}

function ContactPersonViewModel() {
    var self = this;
    self.contactPersonName = ko.observable('').extend({ required: true});
    self.contactPersonNumber = ko.observable('').extend({ maxLength: 50});
    self.errors = ko.validation.group(self);
    
    /* old logs*/
    self.oldContactPersonName = ko.observable('');
    self.oldContactPersonNumber = ko.observable('');

    self.reset = function() {
        self.contactPersonName('');
        self.contactPersonNumber('');
        self.oldContactPersonName('');
        self.oldContactPersonNumber('');
        self.errors.showAllMessages(false);
        return true;
    };

    self.show = function(item) {
        self.reset();
        self.contactPersonName(item.contactPersonName());
        self.contactPersonNumber(item.contactPersonNumber());

        self.oldContactPersonName(item.contactPersonName());
        self.oldContactPersonNumber(item.contactPersonNumber());
    };

    self.resetToOld = function() {
        self.contactPersonName(self.contactPersonName());
        self.contactPersonNumber(self.contactPersonNumber());
    };
}


function CustomerViewModel() {
    var self = this;
    self.contactPerson = new ContactPersonViewModel();
    self.name = ko.observable('').extend({ required: true, maxLength: 50, nameUsed: false });
    self.email = ko.observable('').extend({ required: true, maxLength: 50, email: true, emailUsed: false });
    self.contactNumber = ko.observable('').extend({ maxLength: 50});
    self.address = ko.observable('').extend({ maxLength: 300 });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });

    self.contactPersons = ko.observableArray([]);

    self.errors = ko.validation.group(self);
    self.removeErrors = function() {self.errors.showAllMessages(false);};

    self.getCustomerDetail = function() {

        var customerObj = {
            Name: self.name(),
            Email: self.email(),
            ContactNumber: self.contactNumber(),
            Address: self.address(),
            DateOfCreation: self.dateOfCreation()
        };

        var contactPersonObj = [];
        var contactPersons = self.contactPersons();
        for (i in contactPersons) {
            contactPersonObj.push({
                Name: contactPersons[i].contactPersonName(),
                ContactNumber: contactPersons[i].contactPersonNumber()
            });
        }

        return {
            customer: customerObj,
            contactPersons: contactPersonObj
        };
    };


    /*local actions*/

    self.createContactPerson = function(item) {
        self.contactPerson.errors.showAllMessages(false);
        if (self.contactPerson.errors().length > 0) {
            self.contactPerson.errors.showAllMessages();
            return;
        }
        self.addContactPerson(item);
        self.contactPerson.reset();
        ToastSuccess('Contact Person added to Customer');
        $('#divContactPerson').modal('hide');
    };

    self.addContactPerson = function(item) {
        var arr = self.contactPersons();
        arr.push(new ContactPerson(item.contactPersonName(), item.contactPersonNumber()));
        self.contactPersons([]);
        self.contactPersons(arr);
    };

    self.updateContactPerson = function(item) {
        self.contactPerson.errors.showAllMessages(false);
        if (self.contactPerson.errors().length > 0) {
            self.contactPerson.errors.showAllMessages();
            return;
        }
        self.addContactPerson(item);
        self.contactPerson.reset();

        ToastSuccess('Contact Person updated and added to Customer');
        $('#divContactPerson').modal('hide');
    };

    self.cancelContactPersonUpdate = function(item) {
        self.contactPerson.resetToOld();
        self.contactPerson.errors.showAllMessages(false);
        if (self.contactPerson.errors().length > 0) {
            self.contactPerson.errors.showAllMessages();
            return;
        }
        self.addContactPerson(self.contactPerson);
        self.contactPerson.reset();

        $('#divContactPerson').modal('hide');
    };

    self.showToUpdate = function(item) {
        self.contactPerson.show(item);
        self.contactPersons.remove(item);

        $("#divActionCreate").hide();
        $('#btnCloseModal').hide();
        $("#divActionUpdate").show();
        $('#divContactPerson').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
        return true;
    };

    self.removeContactPerson = function(item) {
        bootbox.confirm("Do you want to delete the Contact Person?", function(result) {
            if (result === true) {
                self.contactPersons.remove(item);
                ToastSuccess('Contact Person deleted.');
            }
        });
    };


    /*Actions*/
    self.saveToServer = function() {
        var json = JSON.stringify(self.getCustomerDetail());
        $.ajax({
            url: '/CustomerManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    ToastSuccess('Customer created successfully');
                    self.clear();
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    

    self.save = function() {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return;
        }

        if (self.contactPersons().length == 0) {
            bootbox.confirm("You want to add customer without a \"Contact Person\" ?", function(result) {
                if (result == true) {
                    self.saveToServer();
                }              
            });
        } else {
            self.saveToServer();
        }
    };


    
    ///*Redirect to all page*/
    //self.back = function () {

    //    var url = '@Url.Action("Login", "Login")';
    //    window.location.href = url;
    //}

    

    self.clear = function() {
            self.name(''),
            self.email(''),
            self.contactNumber(''),
            self.address(''),
            self.dateOfCreation(''),
            self.dateOfCreation(currentDate()),
            self.contactPersons([]),
            self.removeErrors();
            self.errors.showAllMessages(false);
            return true;
        };

        self.init = function() {
            activeParentMenu($('li.submenu a[href="/CustomerManage/Index"]:first'));
        };
    }

$(document).ready(function () {
    
    $('.datepicker').datepicker();

    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false
    });

    $("#btnAddContactPerson").click(function () {
        $("#divActionCreate").show();
        $('#btnCloseModal').show();
        $("#divActionUpdate").hide();
        $('#divContactPerson').removeData("modal").modal();
    });
    
    var viewModel = new CustomerViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});
