﻿
/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#txtCustomerId').val(), name: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsNameUsedExceptCustomer',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This customer name is already in use'
};
ko.validation.registerExtenders();

ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ id: $('#txtCustomerId').val(), email: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsEmailUsedExceptCustomer',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This customer email is already in use'
};
ko.validation.registerExtenders();


function ContactPerson(id, contactPersonName, contactPersonNumber) {
    this.contactPersonId = ko.observable(id);
    this.contactPersonName = ko.observable(contactPersonName);
    this.contactPersonNumber = ko.observable(contactPersonNumber);
}

function ContactPersonViewModel() {
    var self = this;
    self.contactPersonId = ko.observable('').extend({});
    self.contactPersonName = ko.observable('').extend({ required: true });
    self.contactPersonNumber = ko.observable('').extend({ maxLength: 50});
    self.errors = ko.validation.group(self);

    /* old logs*/
    self.oldContactPersonId = ko.observable('');
    self.oldContactPersonName = ko.observable('');
    self.oldContactPersonNumber = ko.observable('');

    self.reset = function () {
        self.contactPersonId('');
        self.contactPersonName('');
        self.contactPersonNumber('');
        self.oldContactPersonId('');
        self.oldContactPersonName('');
        self.oldContactPersonNumber('');
        self.errors.showAllMessages(false);
        return true;
    };

    self.show = function (item) {
        self.reset();
        self.contactPersonId(item.contactPersonId());
        self.contactPersonName(item.contactPersonName());
        self.contactPersonNumber(item.contactPersonNumber());

        self.oldContactPersonId(item.contactPersonId());
        self.oldContactPersonName(item.contactPersonName());
        self.oldContactPersonNumber(item.contactPersonNumber());
    };

    self.resetToOld = function () {
        self.contactPersonId(self.oldContactPersonId());
        self.contactPersonName(self.oldContactPersonName());
        self.contactPersonNumber(self.oldContactPersonNumber());
    };
}

function CustomerViewModel() {

    var self = this;
    self.contactPerson = new ContactPersonViewModel();
    self.id = ko.observable('').extend({ required: true });
    self.name = ko.observable('').extend({ required: true, maxLength: 50, nameUsed: false });
    self.email = ko.observable('').extend({ required: true, email: true, maxLength: 50, emailUsed: false });
    self.contactNumber = ko.observable('').extend({ required: true, maxLength: 50});
    self.address = ko.observable('').extend({ required: true, maxLength: 300 });
    self.dateOfCreation = ko.observable('').extend({ required: true });

    self.contactPersons = ko.observableArray([]);
    self.removedContactPersons = ko.observableArray([]);
    self.updatedContactPersons = ko.observableArray([]);

    self.errors = ko.validation.group(self);

    self.getUpdateDetail = function () {

        var customerObj = {
            Id: self.id(),
            Name: self.name(),
            Email: self.email(),
            ContactNumber: self.contactNumber(),
            Address: self.address(),
            DateOfCreation: self.dateOfCreation()
        };

        var createdContactPersonsList = [];
        var updatedContactPersonsList = [];
        var removedContactPersonsList;
        var contactPersons = self.contactPersons();
        var updatedContactPersons = self.updatedContactPersons();
        var removedContactPersons = self.removedContactPersons();
        var updatedContactPersonsCopy = [];

        /*removing the items from updatedPersons which is already in removedPersons*/
        var flag, position = 0;
        for (var i = 0; i < updatedContactPersons.length; i++) {
            for (var j = 0; j <= removedContactPersons.length; j++) {
                if (updatedContactPersons[i] != removedContactPersons[j]) {
                    for (var k = 0; k <= updatedContactPersonsCopy.length; k++) {
                        if (updatedContactPersons[i] == updatedContactPersonsCopy[k]) {
                            flag = 0;
                            break;
                        }
                        flag = 1;
                    }
                    if (flag == 1) {
                        updatedContactPersonsCopy[position++] = updatedContactPersons[i];
                    }
                }
            }
        }
        
        for (i in contactPersons) {
            if (contactPersons[i].contactPersonId() == 0) {
                createdContactPersonsList.push({
                    Name: contactPersons[i].contactPersonName(),
                    ContactNumber: contactPersons[i].contactPersonNumber()
                });
            }
        }

        for (i in updatedContactPersonsCopy) {
            var person = self.getContactPerson(updatedContactPersonsCopy[i]);

            updatedContactPersonsList.push({
                Id: person.contactPersonId(),
                Name: person.contactPersonName(),
                ContactNumber: person.contactPersonNumber()
            });
        }

        removedContactPersonsList = removedContactPersons;

        return {
            customer: customerObj,
            createdPersons: createdContactPersonsList,
            updatedPersons: updatedContactPersonsList,
            removedPersons: removedContactPersonsList
        };
    };


    /*Actions*/
    self.update = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        var json = JSON.stringify(self.getUpdateDetail());
        $.ajax({
            url: '/CustomerManage/Update',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function(data) {
                if (data === true) {
                    self.reset();
                    ToastSuccess('Customer updated successfully');
                    }
            },
           error: function (xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };


    /*get all*/
    self.load = function () {
        var json = JSON.stringify({ id: self.id() });
        $.ajax({
            url: '/CustomerManage/Get',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.name(data.Name);
                self.email(data.Email),
                self.contactNumber(data.ContactNumber);
                self.address(data.Address);
                self.dateOfCreation('');
                self.dateOfCreation(clientDate(data.DateOfCreation));
            },
            error: function (xhr) {
                alert(xhr);
                ToastError(xhr);
            }
        });
    };


    self.loadContactPersons = function () {
        self.contactPersons([]);
        $.ajax({
            url: '/CustomerManage/GetContactPersons/' + self.id(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new ContactPerson(data[i].Id, data[i].Name, data[i].ContactNumber));
                });
                self.contactPersons(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.getContactPerson = function (id) {
        var contactPersons = self.contactPersons();
        var person = null;
        for (var i = 0; i < contactPersons.length; i++) {
            if (contactPersons[i].contactPersonId() === id) {
                person = contactPersons[i];
                break;
            }
        }
        return person;
    };
    
    self.reset = function () {
        self.load();
        return true;
    };


    /*local actions*/
    self.createContactPerson = function (item) {
        self.contactPerson.errors.showAllMessages(false);
        if (self.contactPerson.errors().length > 0) {
            self.contactPerson.errors.showAllMessages();
            return;
        }
        self.insertContactPerson(item);

        self.contactPerson.reset();
        ToastSuccess('Contact Person added to Customer');
        $('#divContactPerson').modal('hide');
    };

    self.insertContactPerson = function (item) {
        var arr = self.contactPersons();
        arr.push(new ContactPerson(0, item.contactPersonName(), item.contactPersonNumber()));
        self.contactPersons([]);
        self.contactPersons(arr);
    };

    self.addContactPerson = function (item) {
        var arr = self.contactPersons();
        arr.push(new ContactPerson(item.contactPersonId(), item.contactPersonName(), item.contactPersonNumber()));
        self.contactPersons([]);
        self.contactPersons(arr);
    };

    self.updateContactPerson = function (item) {
        self.contactPerson.errors.showAllMessages(false);
        if (self.contactPerson.errors().length > 0) {
            self.contactPerson.errors.showAllMessages();
            return;
        }

        self.addContactPerson(item);

        if (item.contactPersonId() != 0) {
            var prr = self.updatedContactPersons();
            prr.push(item.contactPersonId());
            self.updatedContactPersons([]);
            self.updatedContactPersons(prr);
        }      

        self.contactPerson.reset();

        ToastSuccess('Contact Person updated and added to queue');
        $('#divContactPerson').modal('hide');
    };

    self.cancelContactPersonUpdate = function (item) {
        self.contactPerson.resetToOld();
        self.contactPerson.errors.showAllMessages(false);
        if (self.contactPerson.errors().length > 0) {
            self.contactPerson.errors.showAllMessages();
            return;
        }       
        
        self.addContactPerson(self.contactPerson);
        self.contactPerson.reset();
        
        $('#divContactPerson').modal('hide');
    };

    self.removeContactPerson = function (item) {
        bootbox.confirm("Do you want to delete the Contact Person ?", function (result) {
            if (result === true) {
                if (item.contactPersonId() != 0) {
                    var arr = self.removedContactPersons();
                    arr.push(item.contactPersonId());
                    self.removedContactPersons(arr);
                }
                self.contactPersons.remove(item);              
                ToastSuccess('Contact Person deleted from the queue');
            }
        });
    };

    self.showToUpdate = function (item) {
        self.contactPerson.show(item);
        self.contactPersons.remove(item);

        $("#divActionCreate").hide();
        $('#btnCloseModal').hide();
        $("#divActionUpdate").show();
        $('#divContactPerson').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
        return true;
    };
    self.init = function () {
        self.id($('#txtCustomerId').val());
        self.load();
        self.loadContactPersons();

        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}

$(document).ready(function () {

    $('.datepicker').datepicker();

    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false
    });

    $("#btnAddContactPerson").click(function () {
        $("#divActionCreate").show();
        $('#btnCloseModal').show();
        $("#divActionUpdate").hide();
        $('#divContactPerson').removeData("modal").modal();
    });

    var viewModel = new CustomerViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});



