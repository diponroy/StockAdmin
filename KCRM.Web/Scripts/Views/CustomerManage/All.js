﻿
function Customer(id, name, email, contactNumber, address, dateOfCreation) {
    
    this.id = ko.observable(id);
    this.name = ko.observable(name);
    this.email = ko.observable(email);
    this.contactNumber = ko.observable(contactNumber);
    this.address = ko.observable(address);
    this.dateOfCreation = ko.observable(dateOfCreation);
    this.updateUrl = ko.computed(function () {
        return '/CustomerManage/Update/' + id;
    }, this);
    this.deleteUrl = ko.computed(function () {
        return '/CustomerManage/Delete';
    }, this);
}


function ViewModel() {

    var self = this;
    
    self.customers = ko.observableArray([]),

    self.load= function() {
        $.ajax({
            url: '/CustomerManage/GetAll',
            dataType: "json",
            contentType: 'application/json',
            type: "GET",
            success: function(data) {
                var arr = [];
                $.each(data, function(i) {
                    arr.push(new Customer(data[i].Id, data[i].Name, data[i].Email, data[i].ContactNumber, data[i].Address, clientDateStringFromDate(data[i].DateOfCreation)));
                });
                self.customers(arr);

                $('.data-table').dataTable({
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "sDom": '<""l>t<"F"fp>',
                    "bFilter": false,
                });
            },
            error: function(xhr) {
                ToastError(xhr);
            }
        });
    };


    self.deleteCustomer = function (item) {

        bootbox.confirm("Are you sure, you want to delete the Customer ?", function(result) {

            if (result === true) {
                var json = JSON.stringify({
                    id: item.id(),
                });
                $.ajax({
                    url: item.deleteUrl(),
                    dataType: "json",
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    data: json,
                    async: true,
                    processData: false,
                    cache: false,
                    success: function(data) {

                        if (data === true) {
                            bootbox.alert("Customer deleted successfully");
                        }
                    },
                    error: function(xhr) {
                        ToastError(xhr);
                    }
                });
            }
        });
        return true;
    };
    self.init = function () {
        self.load();
        activeParentMenu($('li.submenu a[href="/CustomerManage/Index"]:first'));
    };
}

$(document).ready(function () {

    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});