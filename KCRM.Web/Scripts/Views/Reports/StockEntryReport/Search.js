﻿function Category(id, name) {
    this.id = id;
    this.name = name;
}

function Product(id, name, category) {
    this.id = id;
    this.name = name;
    this.category = category;
}

function Stock(id, name, description,dateofcreation, quantity) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.dateOfCreation = dateofcreation,
    this.quantity = quantity;
}

/*View Model*/
function ViewModel() {
    var self = this;
    self.fromDate = ko.observable(currentDate());
    self.toDate = ko.observable(currentDate());
    
    self.categoryId = ko.observable('');
    self.productId = ko.observable('');

    self.productCategory = ko.observableArray([]);
    self.allProduct = ko.observableArray([]);
    self.products = ko.observableArray([]);

    self.currentStock = ko.observableArray([]);

    //self.CompareDate = function () {
    //    alert("exiting");
    //    if (self.fromDate() >= self.toDate()) {
    //        alert("Error");
    //    }
    //};


    self.objToPost = function() {
        return {
            fromDate: (typeof (self.fromDate()) === 'undefined') ? 0 : self.fromDate(),
            toDate: (typeof (self.toDate()) === 'undefined') ? 0 : self.toDate(),
            reportDate: currentDate(),
            productCategoryId: (typeof (self.categoryId()) === 'undefined') ? 0 : self.categoryId(),
            productId: (typeof (self.productId()) === 'undefined') ? 0 : self.productId()
        };
    };


    self.categoryId.subscribe(function(newValue) {
        self.products([]);

        if (typeof (newValue) === 'undefined') {
            self.products(self.allProduct());
            return true;
        }

        var arr = $.grep(self.allProduct(), function(item, index) {
            return item.category === newValue;
        });
        self.products(arr);
        return true;
    });

    self.init = function () {
        activeParentMenu($('li.submenu a[href="/StockEntryReport/Index"]:first'));
        self.loadCategory();
        self.loadProduct();
    };


/*GET all txt box value */
    self.loadCategory = function() {
        self.productCategory([]);
        $.ajax({
            url: '/ProductManage/GetProductCategories',
            dataType: "Json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            data: "{}",
            async: true,
            processData: false,
            cache: false,
            success: function(data) {
                var arr = [];
                $.each(data, function(i) {
                    arr.push(new Category(data[i].Value, data[i].Name));
                });
                self.productCategory(arr);
            },
            errors: function(xhr) {
                ToastError(xhr);
            }
        });
    };
    self.loadProduct = function() {
        self.allProduct([]);
        self.products([]);
        $.ajax({
            url: '/StockEntryReport/GetAllProducts',
            dataType: "Json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function(data) {
                var arr = [];
                $.each(data, function(i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].Category));
                });
                self.allProduct(arr);
                self.products(arr);
            },
            errors: function(xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.search = function () {
        //self.CompareDate();
        self.currentStock([]);

        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/StockEntryReport/SearchtStockEntry',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function(data) {
                var arr = [];
                $.each(data, function(i) {
                    arr.push(new Stock(data[i].Product.Id, data[i].Product.Name, data[i].Product.Description, clientDateStringFromDate(data[i].DateOfCreation), data[i].Quantity));
                });
                self.currentStock(arr);
            },
            error: function(xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    
    self.setTempModel = function () {
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/StockEntryReport/SetTempModel',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/StockEntryReport/StockEnrtyPdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };


};

$(document).ready(function() {

    /*date pickers*/
    $('.datepicker').datepicker();

    /*View model*/
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});


