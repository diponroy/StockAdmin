﻿function QuotationCode(id, code) {
    this.id = id;
    this.code = code;
}

function Customer(id, name) {
    this.id = id;
    this.name = name;
}

function User(id, loginName) {
    this.id = id;
    this.loginName = loginName;
}

function Search(data, inspections) {
    this.id = data.Id;
    this.dateOfCreation = clientDateStringFromDate(data.DateOfCreation);
    this.quotationCode = data.QuotationCode;
    this.customerName = data.Customer;
    this.inspectorName = data.PriceInspector;
    this.inspections = inspections;

    this.totalPrice = function () {
        var total = parseFloat(0);
        var allInspections = this.inspections;
        $.each(allInspections, function (i) {
            total += parseFloat(allInspections[i].price);
        });
        return total.toFixed(2);
    };
}

function Inspection(inspection) {
    this.id = inspection.Id;
    this.supplierName = inspection.SupplierName;
    this.name = inspection.ProductName;
    this.unit = inspection.ProductUnit;
    this.quantity = inspection.ProductQuantity;
    this.price = inspection.ProductPrice;
}

function ViewModel() {
    var self = this;
    self.quotationCode = ko.observable('');
    self.customerId = ko.observable('');
    self.inspectorId = ko.observable('');
    self.fromDate = ko.observable(currentDate());
    self.toDate = ko.observable(currentDate());
    
    self.quotationCodes = ko.observableArray([]);
    self.customers = ko.observableArray([]);
    self.inspectors = ko.observableArray([]);
    self.searches = ko.observableArray([]);
    

    /*Gets*/
    self.loadQuotationCodes = function () {
        self.quotationCodes([]);
        var obj = {
            customerId: (typeof (self.customerId()) === 'undefined') ? 0 : self.customerId(),
            priceInspectorId: (typeof (self.inspectorId()) === 'undefined') ? 0 : self.inspectorId(),
        };
        var json = JSON.stringify(obj);
        $.ajax({
            url: '/MarketQuotationReport/GetQuotationCodes',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new QuotationCode(data[i].Id, data[i].QuotationCode));
                });
                self.quotationCodes(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    
    self.loadCustomers = function (userId) {
        self.customers([]);
        $.ajax({
            url: '/MarketQuotationReport/GetCustomers/' + userId,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadUsers = function () {
        self.inspectors([]);
        $.ajax({
            url: '/MarketQuotationReport/GetUsers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new User(data[i].Id, data[i].LoginName));
                });
                self.inspectors(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    
    /*Actions*/
    self.search = function () {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        self.searches([]);

        var json = JSON.stringify(self.getSearchDetail());
        $.ajax({
            url: '/MarketQuotationReport/SearchResult',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    var inspections = [];
                    $.each(data[i].Inspections, function (j) {
                        inspections.push(new Inspection(data[i].Inspections[j]));
                    });
                    arr.push(new Search(data[i], inspections));
                });
                self.searches(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.setTempModel = function () {
        var json = JSON.stringify(self.getSearchDetail());
        $.ajax({
            url: '/MarketQuotationReport/SetTempModel',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/MarketQuotationReport/MarketQuotationReportPdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    
    /*selected change events*/
    self.inspectorId.subscribe(function(newValue) {
        self.customers([]);
        if (typeof (newValue) == 'undefined') {
            return true;
        }
        self.loadCustomers(newValue);
        return true;
    });

    self.customerId.subscribe(function(newValue) {
        self.quotationCodes([]);
        if (typeof (newValue) == 'undefined') {
            return true;
        }
        self.loadQuotationCodes(newValue);
        return true;
    });



    self.getSearchDetail = function () {
        return {
            quotationId: (typeof (self.quotationCode()) === 'undefined') ? 0 : self.quotationCode(),
            customerId: (typeof (self.customerId()) === 'undefined') ? 0 : self.customerId(),
            inspectorId: (typeof (self.inspectorId()) === 'undefined') ? 0 : self.inspectorId(),
            fromDate: self.fromDate(),
            toDate: self.toDate(),
            reportDate: currentDate()
        };
    };

    self.init = function () {
        self.loadUsers();        
        activeParentMenu($('li.submenu a[href="/MarketQuotationReport/Index"]:first'));
    };
}

$(document).ready(function () {
    $('.datepicker').datepicker();

    //$('.data-table').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": '<""l>t<"F"fp>',
    //    "bFilter": false,
    //    "bPaginate": false,
    //    "bInfo": false
    //});

    var viewModel = new ViewModel();
    viewModel.init();

    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});

