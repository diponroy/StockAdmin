﻿/*Models*/
function Customer(id, name) {
    this.id = id;
    this.name = name;
}

function Inspector(id, loginName, firstName, lastName) {
    this.id = id;
    this.logingName = loginName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.nameDetail = this.firstName + " " + this.lastName + "(" + this.logingName + ")";
}

function MarketQuotation(id, code) {
    this.id = id;
    this.code = code;
}

function ContactPerson(id, name) {
    this.id = id;
    this.name = name;
}

function QuotationOrder(data) {
    this.productName = data.Product.Name;
    this.productUnitName = data.Product.UnitName;
    this.quantity = data.Quantity;
    this.vat = parseFloat(data.Vat).toFixed(2);
    this.price = parseFloat(data.Price).toFixed(2);
    this.total = function () {
        var total = parseFloat(data.Total);
        return total.toFixed(2);
    };
}

function ClientQuotation(data) {
    this.dateOfCreation = clientDateStringFromDate(data.DateOfCreation);
    this.customer = data.Customer.Name;
    this.contactPerson = data.CustomerContactPerson.Name;
    this.priceInspector = data.MarketQuotation.PriceInspectorUser.FirstName + " " + data.MarketQuotation.PriceInspectorUser.LastName;
    this.orders = function() {
        var clientOrders = [];
        $.each(data.ClientQuotationOrders, function(i) {
            clientOrders.push(new QuotationOrder(data.ClientQuotationOrders[i]));
        });
        return clientOrders;
    };
    this.grandTotal = function() {
        var total = parseFloat(0);
        var allOrder = this.orders();
        $.each(allOrder, function (i) {
            total += parseFloat(allOrder[i].total());
        });
        return total.toFixed(2);
    };
    this.rowspan = parseInt(this.orders().length) + 1;
}

/*View Models*/
function ViewModel() {
    var self = this;
    self.fromDate = ko.observable(currentDate());
    self.toDate = ko.observable(currentDate());
    self.customerId = ko.observable('');
    self.contactPersonId = ko.observable('');
    self.priceInspectorId = ko.observable('');
    self.marketQutationId = ko.observable('');

    self.customers = ko.observableArray([]);
    self.contactPersons = ko.observableArray([]);
    self.priceInspectors = ko.observableArray([]);
    self.marketQutaions = ko.observableArray([]);
    self.quotations = ko.observableArray([]);

    /*Post objects*/
    self.objToPost = function () {       
        var obj = {
            FromDate: self.fromDate(),
            ToDate: self.toDate(),
            CustomerId: (typeof (self.customerId()) === 'undefined') ? 0 : self.customerId(),
            ContactPersonId: (typeof (self.contactPersonId()) === 'undefined') ? 0 : self.contactPersonId(),
            PriceInspectorId: (typeof (self.priceInspectorId()) === 'undefined') ? 0 : self.priceInspectorId(),
            MarketQuotationId: (typeof (self.marketQutationId()) === 'undefined') ? 0 : self.marketQutationId(),
            ReportDate: currentDate()
        };
        return obj;
    };

    /*Server Gets*/
    self.getCustomers = function () {
        self.customers([]);
        $.ajax({
            url: '/ClientQuotationReport/GetCustomers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    self.getContactPersons = function (customerId) {
        self.contactPersons([]);
        $.ajax({
            url: '/ClientQuotationReport/GetContactPersons/' + customerId,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new ContactPerson(data[i].Id, data[i].Name));
                });
                self.contactPersons(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    self.getPriceInspectors = function (customerId) {
        self.priceInspectors([]);
        $.ajax({
            url: '/ClientQuotationReport/GetPriceInspectorByCustomer/' + customerId,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Inspector(data[i].Id, data[i].LoginName, data[i].FirstName, data[i].LastName));
                });
                self.priceInspectors(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.getMarketQutaions = function () {
        self.marketQutaions([]);
        var postObj = {
            customerId: (typeof (self.customerId()) === 'undefined') ? 0 : self.customerId(),
            contactPersonId: (typeof (self.contactPersonId()) === 'undefined') ? 0 : self.contactPersonId(),
            priceInspectorId: (typeof (self.priceInspectorId()) === 'undefined') ? 0 : self.priceInspectorId()
        };
        var json = JSON.stringify(postObj);
        $.ajax({
            url: '/ClientQuotationReport/GetMarketQuotations',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new MarketQuotation(data[i].Id, data[i].QuotationCode));
                });
                self.marketQutaions(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
   
    self.getQuotations = function () {
        self.quotations([]);
        var json = JSON.stringify({ model: self.objToPost() });
        $.ajax({
            url: '/ClientQuotationReport/GetClientQuotations',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                var quoations = data.ClientQuotations;
                $.each(quoations, function (i) {
                    arr.push(new ClientQuotation(quoations[i]));
                });
                self.quotations(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.setTempModel = function () {
        var json = JSON.stringify({ model: self.objToPost() });
        $.ajax({
            url: '/ClientQuotationReport/SetTempModel',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/ClientQuotationReport/QuotationPdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    self.showSingleClientQuotation = function () {

        if (self.marketQutationId() == 0 || typeof (self.priceInspectorId()) === 'undefined') {
            bootbox.alert('You must have to select a Quotation Code');
            return;
        }

        var json = JSON.stringify({ model: self.objToPost() });
        $.ajax({
            url: '/ClientQuotationReport/SetTempModel',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/ClientQuotationReport/SingleQuotationPdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*select change events*/
    self.customerId.subscribe(function(newValue) {
        self.contactPersons([]);
        self.priceInspectors([]);
        if (typeof (newValue) === 'undefined') {
            return true;
        }
        self.getContactPersons(newValue);
        self.getPriceInspectors(newValue);
        return true;
    });
    
    self.priceInspectorId.subscribe(function (newValue) {
        self.marketQutaions([]);
        if (typeof (newValue) === 'undefined') {
            return true;
        }
        self.getMarketQutaions();
        return true;
    });

    /*resets*/
    self.init = function () {
        activeParentMenu($('li.submenu a[href="/ClientQuotationReport/Index"]:first'));
        self.getCustomers();
    };
}


$(document).ready(function () {

    /*date pickers*/
    $('.datepicker').datepicker();

    /*view models*/
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
});