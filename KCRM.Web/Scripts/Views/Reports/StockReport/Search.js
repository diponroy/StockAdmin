﻿function Category(id, name) {
    this.id = id;
    this.name = name;
}

function Product(id, name, category) {
    this.id = id;
    this.name = name;
    this.category = category;
}

function stockList(id, name, unit, quantity) {
    this.id = id;
    this.name = name;
    this.unit = unit;
    this.quantity = quantity;
}

/*View Model*/
function ViewModel() {
    var self = this;
    self.categoryId = ko.observable('');
    self.productId = ko.observable('');

    self.productCategory = ko.observableArray([]);
    self.allProduct = ko.observableArray([]);
    self.products = ko.observableArray([]);

    self.currentStock = ko.observableArray([]);

    self.objToPost = function () {
        return {
            productCategory: (typeof (self.categoryId()) === 'undefined') ? 0 : self.categoryId(),
            productId: (typeof (self.productId()) === 'undefined') ? 0 : self.productId(),
            reportDate: currentDate()
        };
    };


    self.categoryId.subscribe(function (newValue) {
        self.products([]);

        if (typeof (newValue) === 'undefined') {
            self.products(self.allProduct());
            return true;
        }

        var arr = $.grep(self.allProduct(), function (item, index) {
            return item.category === newValue;
        });
        self.products(arr);
        return true;
    });

    self.init = function () {
        activeParentMenu($('li.submenu a[href="/StockReport/Index"]:first'));
        self.loadCategory();
        self.loadProduct();
    };



    /*All txt box value get*/
    self.loadCategory = function () {
        self.productCategory([]);
        $.ajax({
            url: '/ProductManage/GetProductCategories',
            dataType: "Json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            data: "{}",
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Category(data[i].Value, data[i].Name));
                });
                self.productCategory(arr);
            },
            errors: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    self.loadProduct = function () {
        self.allProduct([]);
        self.products([]);
        $.ajax({
            url: '/StockReport/GetAllProducts',
            dataType: "Json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].Category));
                });
                self.allProduct(arr);
                self.products(arr);
            },
            errors: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.search = function () {
        self.currentStock([]);

        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/StockReport/SearchCurrentStock',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new stockList(data[i].Id, data[i].Name, data[i].UnitName, data[i].AvailableQutity));
                });
                self.currentStock(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.setTempModel = function () {
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/StockReport/SetTempModel',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/StockReport/CurrentStockPdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
 };

$(document).ready(function() {
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});


