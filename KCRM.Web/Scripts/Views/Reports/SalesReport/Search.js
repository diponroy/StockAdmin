﻿/*Models*/
function Customer(id, name) {
    this.id = id;
    this.name = name;
}

function SoldProduct(data) {
    this.name = data.Product.Name;
    this.unitName = data.Product.UnitName;
    this.quantity = data.Quantity;
    this.price = data.Price;
    this.hasStockConsumption = data.HasStockConsumption;
    this.stockQuantity = (this.hasStockConsumption === true) ? data.StockConsumptions[0].Quantity : 0;
}

function Sale(data) {
    this.dateOfCreation = clientDateStringFromDate(data.DateOfCreation);
    this.customerName = data.Customer.Name;
    this.products = function() {
        var arr = [];
        $.each(data.SoldProducts, function(i) {
            arr.push(new SoldProduct(data.SoldProducts[i]));
        });
        return arr;
    };
    this.totalPrice = function() {
        var total = parseFloat(0);
        var allProduct = this.products();
        $.each(allProduct, function(i) {
            total += parseFloat(allProduct[i].price);
        });
        return total.toFixed(2);
    };
}

/*View Models*/
function ViewModel() {
    var self = this;
    self.fromDate = ko.observable(currentDate());
    self.toDate = ko.observable(currentDate());
    self.customerId = ko.observable('');

    self.customers = ko.observableArray([]);  
    self.sales = ko.observableArray([]);

    /*Post objects*/
    self.objToPost = function() {
        return {            
            fromDate: self.fromDate(),
            toDate: self.toDate(),
            reportDate: currentDate(),
            customerId: (typeof (self.customerId()) === 'undefined') ? 0 : self.customerId()
        };
    };

    /*Server Gets*/
    self.getCustomers = function() {        
        self.customers([]);
        $.ajax({
            url: '/SalesReport/GetCustomers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    self.getSales = function () {
        self.sales([]);
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/SalesReport/GetSales',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Sale(data[i]));
                });
                self.sales(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    self.setTempModel = function () {
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/SalesReport/SetTempModel',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/SalesReport/SalesPdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*resets*/
    self.init = function() {
        activeParentMenu($('li.submenu a[href="/StockReport/Index"]:first'));
        self.getCustomers();
    };
}


$(document).ready(function () {
    
    /*date pickers*/
    $('.datepicker').datepicker();

    /*view models*/
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
});