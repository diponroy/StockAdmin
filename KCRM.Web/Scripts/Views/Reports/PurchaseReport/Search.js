﻿function Supplier(id, name) {
    this.id = id;
    this.name = name;
}

function Customer(id, name) {
    this.id = id;
    this.name = name;
}

function User(id, loginName) {
    this.id = id;
    this.loginName = loginName;
}

function Search(data, products) {
    this.id = data.Id;
    this.dateOfCreation = clientDateStringFromDate(data.DateOfCreation);
    this.supplierName = data.Supplier;
    this.customerName = data.Customer;
    this.purchaserName = data.Purchaser;
    this.products = products;

    this.totalPrice = function() {
        var total = parseFloat(0);
        var allProducts = this.products;
        $.each(allProducts, function(i) {
            total += parseFloat(allProducts[i].price);
        });
        return total.toFixed(2);
    };
}

function PurchasedProduct(product) {
    this.id = product.Id;
    this.name = product.Name;
    this.unit = product.UnitName;
    this.quantity = product.Quantity;
    this.price = product.Price;
}

function ViewModel() {
    var self = this;
    self.supplierId = ko.observable('').extend({});
    self.customerId = ko.observable('').extend({});
    self.userId = ko.observable('').extend({});
    self.fromDate = ko.observable(currentDate()).extend({});
    self.toDate = ko.observable(currentDate()).extend({});

    self.customers = ko.observableArray([]);
    self.users = ko.observableArray([]);
    self.suppliers = ko.observableArray([]);
    self.searches = ko.observableArray([]);
    
    /*errors*/
    self.errors = ko.validation.group([self.supplierId, self.customerId, self.userId, self.fromDate, self.toDate]);
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };
    
    /*Gets*/
    self.loadCustomers = function (supplierId) {
        self.customers([]);
        $.ajax({
            url: '/PurchaseReport/GetCustomers/' + supplierId,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadUsers = function () {
        self.users([]);
        var obj = {
            customerId: (typeof (self.customerId()) == 'undefined') ? 0 : self.customerId(),
            supplierId: (typeof (self.supplierId()) == 'undefined') ? 0 : self.supplierId()
        };
        var json = JSON.stringify(obj);
        $.ajax({
            url: '/PurchaseReport/GetUsers',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new User(data[i].Id, data[i].LoginName));
                });
                self.users(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadSuppliers = function () {
        self.suppliers([]);
        $.ajax({
            url: '/PurchaseReport/GetSuppliers/',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Supplier(data[i].Id, data[i].Name));
                });
                self.suppliers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    
    self.getSearchDetail = function () {
        return {          
            supplierId: (typeof (self.supplierId()) === 'undefined') ? 0 : self.supplierId(),
            customerId: (typeof (self.customerId()) === 'undefined') ? 0 : self.customerId(),
            userId: (typeof (self.userId()) === 'undefined') ? 0 : self.userId(),
            fromDate: self.fromDate(),
            toDate: self.toDate(),
            reportDate: currentDate()
        };
    };
    
    self.init = function () {
        self.loadSuppliers();
        activeParentMenu($('li.submenu a[href="/PurchaseReport/Index"]:first'));
    };
    

    /*Actions*/
    self.search = function() {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        self.searches([]);
        
        var json = JSON.stringify(self.getSearchDetail());
        $.ajax({
            url: '/PurchaseReport/SearchResult',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {           
                var arr = [];               
                $.each(data, function (i) {
                    var products = [];
                    $.each(data[i].Products, function(j) {
                        products.push(new PurchasedProduct(data[i].Products[j]));
                    });
                    arr.push(new Search(data[i], products));
                });
                self.searches(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
       return true;
    };
    
    self.setTempModel = function () {
        var json = JSON.stringify(self.getSearchDetail());
        $.ajax({
            url: '/PurchaseReport/SetTempModel',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    window.open('/PurchaseReport/PurchasePdf');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };


    /*selected change events*/
    self.supplierId.subscribe(function(newValue) {
        self.customers([]);
        if (typeof (newValue) == 'undefined') {
            return true;
        }
        self.loadCustomers(newValue);
        return true;
    });

    self.customerId.subscribe(function(newValue) {
        self.users([]);
        if (typeof (newValue) == 'undefined') {
            return true;
        }
        self.loadUsers(newValue);
        return true;
    });

}


$(document).ready(function () {
    $('.datepicker').datepicker();

    //$('.data-table').dataTable({
    //    "bJQueryUI": true,
    //    "sDom": '<""l>t<"F"fp>',
    //    "bFilter": false,
    //    "bPaginate": false,
    //    "bInfo": false
    //});

    var viewModel = new ViewModel();
    viewModel.init();

    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});