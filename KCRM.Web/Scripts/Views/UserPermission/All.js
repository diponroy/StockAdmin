﻿
function User(id, loginName, firstName, lastName) {
    this.id = id;
    this.loginName = loginName;
    this.firstName = firstName;
    this.lastName = lastName;
}

function Permission(value, name) {

    this.attrId = 'permission_' + value;
    this.attrName = 'permission';
    this.id = value;
    this.name = name;
}


function ViewModel() {
    var self = this;

    self.userId = ko.observable().extend({ required: true });

    self.errors = ko.validation.group([self.userId]);

    self.users = ko.observableArray([]);

    self.permissions = ko.observableArray([]);

    self.loadUsers = function () {    
        $.ajax({
            url: '/UserPermissionManage/GetUsers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {

                var arr = [];
                $.each(data, function (i) {
                    arr.push(new User(data[i].Id, data[i].LoginName, data[i].FirstName, data[i].LastNmae));
                });
                self.users(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });   
        return true;
    };

    self.loadPermissions = function () {     
        $.ajax({
            url: '/UserPermissionManage/GetPermissions',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Permission(data[i].Value, data[i].Name));
                });
                self.permissions(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });


    };
  
    self.updatePermissions = function () {       
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }    
        var permissions = [];
        $('input[type="checkbox"][name="permission"]:checked').each(function() {
            permissions.push($(this).val());
        });
        var json = JSON.stringify({ model: { userId: self.userId(), permissions: permissions } });
        $.ajax({
            url: '/UserPermissionManage/TryToUpdatePermission',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    ToastSuccess('Permissions updated successfully');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    
    /*resets*/
    self.resetPermissionCheckBoxes = function () {
        $('input[type="checkbox"][name="permission"]:checked').each(function () {
            $(this).prop('checked', false);
        });
    };


    self.init = function() {
        self.loadUsers();
        self.loadPermissions();
        activeParentMenu($('li.submenu a[href="/UserPermissionManage/Index"]:first'));

    };
    self.reset = function () {
        self.userId('');
        self.resetPermissionCheckBoxes();
        self.errors.showAllMessages(false);
        return true;
    };
    
    self.userId.subscribe(function (newValue) {
        self.resetPermissionCheckBoxes();
        if (typeof (newValue) === 'undefined' || newValue === '') {
            self.errors.showAllMessages(false);
            return true;
        }     
        $.ajax({
            url: '/UserPermissionManage/GetUserPermissions/' + self.userId(),
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {           
                $.each(data, function (i) {
                    var permissionId = '#permission_' + data[i].PermissionEmun;
                    $(permissionId).prop('checked', true);
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    });
};



$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    viewModel.init();
});