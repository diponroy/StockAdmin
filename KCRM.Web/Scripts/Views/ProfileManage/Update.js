﻿/*Validations*/
ko.validation.rules['passwordConfirm'] = {
    validator: function (val, otherVal) {
        var password = otherVal();
        return val === password;
    },
    message: 'Password and confirmed password is not same.'
};
ko.validation.registerExtenders();


/*View Model*/
function ViewModel() {
    var self = this;
    self.password = ko.observable().extend({ required: true });
    self.confirmPassword = ko.observable().extend({
        required: true, 
        passwordConfirm: ko.computed(function () {
            return self.password();
        }, this)
    });
    
    /*errors*/
    self.errors = ko.validation.group([self.password, self.confirmPassword]);
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };
    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.hasErrors = function () {
        return self.errors().length;
    };
    
    /*object to post*/
    self.objToPost = function() {
        return {
            password: self.password()
        };
    };

    /*Server Actions*/
    self.update = function () {
        self.removeErrors();
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }

        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/ProfileManage/TryToUpdate',
            dataType: "json",
            type: "POST",
            data: json,
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.reset();
                    ToastSuccess('Your password is updated');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    
    /*resets*/
    self.init = function() {

    };
    self.reset = function() {
        self.password('');
        self.confirmPassword('');
        self.removeErrors();
    };

}


$(document).ready(function () {
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});