﻿/*validation */
ko.validation.rules['codeUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ code: val });
        $.when(
            $.ajax({
                url: '/MarketQuotationManage/IsCodeUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Market Quotation Code is already in use'
};
ko.validation.registerExtenders();


/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Customer Name is already in use'
};
ko.validation.registerExtenders();

ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.when(
            $.ajax({
                url: '/CustomerManage/IsEmailUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: '  This Customer Email is already in use'
};
ko.validation.registerExtenders();


function Customer(id, name) {
    this.id = id;
    this.name = name;
}

function Product(id, name, unit) {
    this.id = id;
    this.name = name;
    this.unit = unit;
}

function Supplier(id, name) {
    this.id = id;
    this.name = name;
}

function User(id, loginName) {
    this.id = id;
    this.loginName = loginName;
}


function Inspection(productId, productName, productUnit, quantity, price, supplierId, supplierName) {
    this.productId = ko.observable(productId);
    this.productName = ko.observable(productName);
    this.productUnit = ko.observable(productUnit);
    this.quantity = ko.observable(quantity);
    this.price = ko.observable(price);
    this.supplierId = ko.observable(supplierId);
    this.supplierName = ko.observable(supplierName);
}

/*View Models*/
function InspectionViewModel() {
    var self = this;
    self.productId = ko.observable().extend({ required: true });
    self.quantity = ko.observable().extend({ required: true, min: 1, digit:true });
    self.price = ko.observable().extend({ required: true, min: 0, number:true});
    self.supplierId = ko.observable().extend({ required: true });
    self.supplierName = ko.observable().extend({});
    self.errors = ko.validation.group(self);

    /*Old logs*/
    self.oldProductId = ko.observable('');
    self.oldProductName = ko.observable('');
    self.oldProductUnit = ko.observable('');
    self.oldQuantity = ko.observable('');
    self.oldPrice = ko.observable('');
    self.oldSupplierId = ko.observable('');
    self.oldSupplierName = ko.observable('');

    self.reset = function () {
        self.productId('');
        self.quantity('');
        self.price('');
        self.supplierId('');
        self.supplierName('');

        self.oldProductId('');
        self.oldProductName('');
        self.oldProductUnit('');
        self.oldQuantity('');
        self.oldPrice('');
        self.oldSupplierId('');
        self.oldSupplierName('');
        self.errors.showAllMessages(false);
        return true;
    };

    self.show = function (item) {
        self.reset();
        self.productId(item.productId());
        self.quantity(item.quantity());
        self.price(item.price());
        self.supplierId(item.supplierId());
        self.supplierName(item.supplierName());

        self.oldProductId(item.productId());
        self.oldProductName(item.productName());
        self.oldProductUnit(item.productUnit());
        self.oldQuantity(item.quantity());
        self.oldPrice(item.price());
        self.oldSupplierId(item.supplierId());
        self.oldSupplierName(item.supplierName());
    };

    self.resetToOld = function () {
        self.productId(self.oldProductId());
        self.quantity(self.oldQuantity());
        self.price(self.oldPrice());
        self.supplierId(self.oldSupplierId());
        self.supplierName(self.oldSupplierName());
    };
}

function ViewModel() {
    var self = this;
    /*main view models call backs*/
    CustomerCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var custName = own.name();
            own.clear();
            self.loadCustomers();

            setTimeout(function () {               
                var recentVal = $("#customerList option:contains('" + custName + "')").attr('value');
                $("#customerList").val(recentVal);
                $("#customerList").trigger("change");
            }, 500);
            $('#divCustomerSetup').modal('hide');
            ToastSuccess('Customer has been created successfully');
        }
    };
    
    ProductCreateVm.prototype.createdCallBack = function (data) {
        var productMainView = this;
        if (data == true) {
            var lastProduct = productMainView.name();
            productMainView.reset();
            $('#stack2').modal('hide');
            $('#stack1').removeData("modal").modal();
            self.loadProducts();

            setTimeout(function () {
                var recentVal = $("#sltproduct option:contains('" + lastProduct + "')").attr('value');
                $("#sltproduct").val(recentVal);
                $("#sltproduct").trigger("change");
            }, 500);

            ToastSuccess('Product has been created successfully');
        }
    };

    ProductUnitCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var lastUnit = own.name();
            own.clear();
            $('#stack4').modal('hide');
            $('#stack2').removeData("modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#sltUnit').html("");
            self.productCreate.loadUnits();

            setTimeout(function () {
                var recentVal = $("#sltUnit option:contains('" + lastUnit + "')").attr('value');
                $("#sltUnit").val(recentVal);
                $("#sltUnit").trigger("change");
            }, 500);

            ToastSuccess('Product Unit has been created successfully');
        }
    };

    /*price inspector*/
    PurchaserCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            self.loadUsers();
            var lastPurchaser = own.loginName();
            own.reset();

            setTimeout(function () {
                var recentVal = $("#inspectorList option:contains('" + lastPurchaser + "')").attr('value');
                $("#inspectorList").val(recentVal);
                $("#inspectorList").trigger("change");
            }, 500);
            $('#divPurchaserSetup').modal('hide');
            ToastSuccess('Purchaser has been created successfully');
        }
    };

    SupplierCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            self.loadSuppliers();
            var lastSupplier = own.name();
            own.reset();
            $('#stack3').modal('hide');
            $('#stack1').removeData("modal").modal();

            setTimeout(function () {
                var recentVal = $("#supplierList option:contains('" + lastSupplier + "')").attr('value');
                $("#supplierList").val(recentVal);
                $("#supplierList").trigger("change");
            }, 500);

            ToastSuccess('Supplier has been created successfully');
        }
    };

    self.customer = new CustomerCreateVm();
    self.purchaser = new PurchaserCreateVm();
    self.productCreate = new ProductCreateVm();
    self.productUnitCreate = new ProductUnitCreateVm();
    self.supplierCreate = new SupplierCreateVm();

    self.inspection = new InspectionViewModel();
    self.quotationCode = ko.observable().extend({ required: true, codeUsed: false });
    self.customerName = ko.observable().extend({ required: true });
    self.userName = ko.observable().extend({ required: true });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.inspections = ko.observableArray([]);
    self.totalPrice = ko.computed(function () {
        var totalPrice = 0;
        var products = self.inspections();
        for (var i in products) {
            totalPrice += parseFloat(products[i].price());
        }
        return parseFloat(totalPrice).toFixed(2);
    }, this);
    
    self.customers = ko.observableArray([]);
    self.users = ko.observableArray([]);
    self.products = ko.observableArray([]);
    self.suppliers = ko.observableArray([]);


    /*errors*/
    self.errors = ko.validation.group([self.quotationCode, self.customerName, self.userName, self.dateOfCreation]);
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };
 
    self.init = function () {
        self.loadCustomers();
        self.loadUsers();
        self.loadProducts();
        self.loadSuppliers();
        activeParentMenu($('li.submenu a[href="/MarketQuotationManage/Create"]:first'));
    };
    

    /*Resets*/
    self.reset = function () {
        self.quotationCode('');
        self.customerName('');
        self.userName('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.inspections([]);
        self.loadCustomers();
        self.loadUsers();
        self.removeErrors();
        return true;
    };   

    /*local Gets*/
    self.getQuotationDetail = function () {
        var quotation = {
            QuotationCode: self.quotationCode(),
            CustomerId: self.customerName(),
            PriceInspectorUserId: self.userName(),
            DateOfCreation: self.dateOfCreation()
        };

        var inspectionObj = [];
        var inspections = self.inspections();
        for (i in inspections) {
            inspectionObj.push({
                ProductId: inspections[i].productId(),
                Quantity: inspections[i].quantity(),
                Price: inspections[i].price(),
                SupplierId: inspections[i].supplierId()
            });
        }

        return {
            quotation: quotation,
            inspections: inspectionObj
        };
    };

    self.getProduct = function (id) {
        var products = self.products();
        var product = null;
        for (var i = 0; i < products.length; i++) {
            if (products[i].id === id) {
                product = products[i];
                break;
            }
        }
        return product;
    };
    
    self.getSupplier = function (id) {
        var suppliers = self.suppliers();
        var supplier = null;
        for (var i = 0; i < suppliers.length; i++) {
            if (suppliers[i].id === id) {
                supplier = suppliers[i];
                break;
            }
        }
        return supplier;
    };


    /*Gets*/
    self.loadCustomers = function () {
        self.customers([]);
        $.ajax({
            url: '/MarketQuotationManage/GetCustomers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadProducts = function () {
        self.products([]);
        $.ajax({
            url: '/MarketQuotationManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].UnitName));
                });
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadUsers = function () {
        self.users([]);
        $.ajax({
            url: '/MarketQuotationManage/GetUsers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new User(data[i].Id, data[i].LoginName));
                });
                self.users(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadSuppliers = function () {
        self.suppliers([]);
        $.ajax({
            url: '/MarketQuotationManage/GetSuppliers/',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Supplier(data[i].Id, data[i].Name));
                });
                self.suppliers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.generateCode = function () {
        $.ajax({
            url: '/MarketQuotationManage/GetNextCode',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.quotationCode(data);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };


    /*local Actions*/
   
    self.createInspection = function (item) {
        self.inspection.errors.showAllMessages(false);
        if (self.inspection.errors().length > 0) {
            self.inspection.errors.showAllMessages();
            return;
        }
        self.addInspection(item);
        self.inspection.reset();
        ToastSuccess('Inspection added to Quotation Inspections');
        $('#stack1').modal('hide');
    };
    
    self.addInspection = function (item) {
        var arr = self.inspections();
        var product = self.getProduct(item.productId());
        var supplier = self.getSupplier(item.supplierId());
        arr.push(new Inspection(item.productId(), product.name, product.unit, item.quantity(), item.price(), item.supplierId(), supplier.name));
        self.inspections([]);
        self.inspections(arr);
    };
   
    self.updateInspection = function (item) {
        self.inspection.errors.showAllMessages(false);
        if (self.inspection.errors().length > 0) {
            self.inspection.errors.showAllMessages();
            return;
        }
        self.addInspection(item);
        self.inspection.reset();

        ToastSuccess('Inspection updated and added to Quotation Inspections');
        $('#stack1').modal('hide');
    };

    self.cancelInspectionUpdate = function (item) {
        self.inspection.resetToOld();
        self.inspection.errors.showAllMessages(false);
        if (self.inspection.errors().length > 0) {
            self.inspection.errors.showAllMessages();
            return;
        }
        self.addInspection(self.inspection);
        self.inspection.reset();
        $('#stack1').modal('hide');
    };
    
    self.showToUpdate = function (item) {
        self.inspection.show(item);
        self.inspections.remove(item);

        $("#divActionCreate").hide();
        $('#btnCloseModal').hide();
        $("#divActionUpdate").show();
        $('#stack1').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
        return true;
    };
    
    self.removeInspection = function (item) {
        bootbox.confirm("Do you want to delete this Inspection ?", function (result) {
            if (result === true) {
                self.inspections.remove(item);
                ToastSuccess('Inspection deleted.');
            }
        });
    };


    /*Actions*/
    self.create = function () {
        self.removeErrors();
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
        }
        
        if (self.localCreate() == true) {
            var json = JSON.stringify(self.getQuotationDetail());
            $.ajax({
                url: '/MarketQuotationManage/TryToCreate',
                dataType: "json",
                type: "POST",
                data: json,
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    if (data === true) {
                        ToastSuccess('Quotation added successfully');
                        self.reset();
                    }
                },
                error: function (xhr) {
                    ToastError(xhr);
                }
            });
        }
        return;
    };

    self.localCreate = function() {
        if (self.inspections().length == 0) {
            bootbox.alert("You have to add \"Quotation Inspection\" to create Market Quotation");
            return false;
        }
        return true;
    };
}


$(document).ready(function () {

    $('.datepicker').datepicker();
    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false        
    });

    var viewModel = new ViewModel();
    viewModel.init();    
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);

    /*add popup  buttons*/
    $("#btnAddCustomer").click(function () {
        viewModel.customer.clear();
        $('#divCustomerSetup').removeData("modal").modal();
    });
    $("#btnAddPurchaser").click(function () {
        viewModel.purchaser.reset();
        viewModel.purchaser.init();
        $('#divPurchaserSetup').removeData("modal").modal();
    });
    $("#btnAddProduct").click(function () {
        viewModel.productCreate.reset();
        viewModel.productCreate.init();
        $('#stack1').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnAddSupplier").click(function () {
        viewModel.supplierCreate.reset();
        viewModel.supplierCreate.init();
        $('#stack1').modal('hide');
        $('#stack3').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnClose1").click(function () {
        $('#stack1').removeData("modal").modal();
    });
    $("#btnClose2").click(function () {
        $('#stack1').removeData("modal").modal();
    });
    $("#btnClose3").click(function () {
        viewModel.productUnitCreate.clear();
        $('#stack4').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnAddInspection").click(function () {
        viewModel.inspection.reset();
        $("#divActionCreate").show();
        $('#btnCloseModal').show();
        $("#divActionUpdate").hide();
        $('#stack1').removeData("modal").modal();
    });
});