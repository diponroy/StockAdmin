﻿/*Model*/
function Product(id, name) {
    this.id = id;
    this.name = name;
}

function ProductPrice(id, name, price) {
    this.id = id;
    this.name = name;
    this.price = price;
}


/*View models*/
function ViewModel() {
    var self = this;
    self.productId = ko.observable().extend({ required: true });
    self.price = ko.observable().extend({ required: true, min: 0 });
    self.products = ko.observableArray([]);
    self.productPrices = ko.observableArray([]);

    /*Errors*/
    self.erros = ko.validation.group([self.productId, self.price]);
    self.removeErrors = function () {
        self.erros.showAllMessages(false);
    };
    self.showErrors = function () {
        self.erros.showAllMessages();
    };
    self.hasErrors = function () {
        return self.erros().length;
    };

    self.objToPost = function () {
        return {
            ProductId: self.productId(),
            Price: self.price()
        };
    };

    /*Get*/
    self.loadProducts = function () {
        self.products([]);
        $.ajax({
            url: '/MarketPriceManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].UnitName));
                });
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadProductWithPrice = function () {
        self.productPrices([]);
        $.ajax({
            url: '/MarketPriceManage/GetMarketPrice',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    var price = (parseInt(data[i].Price) === 0) ? 'Price is not yet set' : data[i].Price;
                    arr.push(new ProductPrice(data[i].Id, data[i].ProductName, price));
                });
                self.productPrices(arr);
               },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    self.showToUpdate = function (item) {
        self.reset();
        if ($('#divAddMarketPrice').is(":hidden")) {
            $('#divAddMarketPrice').hide().slideDown('slow');
        }
        $.ajax({
            url: '/MarketPriceManage/GetPriceForUpdate/' + item.id,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.productId(item.id);
                self.price(data.Price);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*Local Actions*/
    self.updateDone = function () {
        $('#divAddMarketPrice').slideUp('slow');
    };

    ///*Add spinner*/
    //$.blockUI({ message: $('#throbber') });
    //$.ajax({
    //    complete: function (data) {
    //        $.unblockUI();
    //    }
    //});

    /*Action*/
    self.update = function () {
        self.removeErrors();
        if (self.hasErrors()) {
            self.showErrors();
            return;
        }
        var json = JSON.stringify(self.objToPost());
        $.ajax({
            url: '/MarketPriceManage/TryToUpdate',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data === true) {
                    self.loadProductWithPrice();
                    self.reset();
                    ToastSuccess('Product price updated succesfully');
                }
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };

    /*reset*/
    self.init = function () {
        self.loadProducts();
        self.loadProductWithPrice();
        activeParentMenu($('li.submenu a[href="/MarketPriceManage/Index"]:first'));
    };
    self.reset = function () {
        self.productId('');
        self.price('');
        self.removeErrors();
    };
    self.removeErrors = function () {
        self.erros.showAllMessages(false);
    };
    self.showErrors = function () {
        self.erros.showAllMessages();
    };
    self.hasErrors = function () {
        return self.erros().length;
    };

    /*select change events*/
    self.productId.subscribe(function (newValue) {
        if (typeof (newValue) === 'undefined' || newValue === '') {
            self.price('');
            self.removeErrors();
            return;
        }
    });
}

$(document).ready(function () {
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});
