﻿/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val, id: $('#txtSupplierId').val() });
        $.when(
            $.ajax({
                url: '/SupplierManage/IsNameUsedExceptUser',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Supplier Name has already been used'
};
ko.validation.registerExtenders();


ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val, id: $('#txtSupplierId').val()});
        $.when(
            $.ajax({
                url: '/SupplierManage/IsEmailUsedExceptUser',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Supplier Email has already been used'
};
ko.validation.registerExtenders();




function ViewModel() {

    /*Fields*/
    var self = this;

    self.id = ko.observable('').extend({
        required: true,
    });

    self.name = ko.observable('').extend({
        required: true,
        maxLength: 50,
        nameUsed: false
    });
    self.email = ko.observable('').extend({
        email: true,
        maxLength: 50,
        emailUsed: false
    });
    self.dateOfCreation = ko.observable().extend({
        required: true,
    });
    self.contactNumber = ko.observable('').extend({
        maxLength: 50,
        number: true
    });

    self.address = ko.observable('').extend({
        maxLength: 300,
    });


    self.errors = ko.validation.group(self);

    self.supplier = function() {
        var obj = {
            Id: self.id(),
            Name: self.name(),
            Email: self.email(),
            ContactNumber: self.contactNumber(),
            Address: self.address(),
            DateOfCreation: self.dateOfCreation(),
        };
        return obj;
    };


    self.update = function() {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        var json = JSON.stringify({ supplier: self.supplier() });
        $.ajax({
            url: '/SupplierManage/Update',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {                
                if (data === true) {
                    self.reset();
                    ToastSuccess("Supplier updated succesfully");
                }
            },
            error: function(xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };
    
    self.load = function () {
        var json = JSON.stringify({ id: self.id() });
        $.ajax({
            url: '/SupplierManage/Get',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                self.name(data.Name);
                self.email(data.Email);
                self.contactNumber(data.ContactNumber);
                self.address(data.Address);
                self.dateOfCreation('');
                self.dateOfCreation(clientDate(data.DateOfCreation));
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.reset = function () {
        self.load();
        return true;
    };
    self.init = function () {
        self.id($('#txtSupplierId').val());
        self.load();

        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}




$(document).ready(function() {
    $('.datepicker').datepicker();
    
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
    viewModel.init();
});