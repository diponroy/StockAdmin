﻿/*Validations*/
ko.validation.rules['nameUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ name: val });
        $.when(
            $.ajax({
                url: '/SupplierManage/IsNameUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Supplier Name has already been used'
};
ko.validation.registerExtenders();


ko.validation.rules['emailUsed'] = {
    validator: function (val, otherVal) {
        var isUsed;
        var json = JSON.stringify({ email: val });
        $.when(
            $.ajax({
                url: '/SupplierManage/IsEmailUsed',
                dataType: "json",
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: json,
                async: false,
            })
        ).then(function (data, textStatus, jqXhr) {
            isUsed = (textStatus === 'success') ? data : null;
        });
        return isUsed === otherVal;
    },
    message: 'This Supplier Email has already been used'
};
ko.validation.registerExtenders();

function ViewModel() {

    /*Fields*/
    var self = this;

    self.name = ko.observable('').extend({
        required: true,
        maxLength: 50,
        nameUsed: false
    });
    self.email = ko.observable('').extend({
        email: true,
        maxLength: 50,
        emailUsed: false
    });
    self.dateOfCreation = ko.observable(currentDate()).extend({
        required: true,
    });
    self.contactNumber = ko.observable().extend({
        maxLength: 50,
    });

    self.address = ko.observable('').extend({
        maxLength: 300,
    });

    self.errors = ko.validation.group(self);

    self.supplier = function() {
        var obj = {
            Name: self.name(),
            Email: self.email(),
            ContactNumber: self.contactNumber(),
            Address: self.address(),
            DateOfCreation: self.dateOfCreation(),
        };
        return obj;
    };


    self.create = function() {
        self.errors.showAllMessages(false);
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }

        var json = JSON.stringify({ supplier: self.supplier() });
        $.ajax({
            url: '/SupplierManage/Create',
            dataType: "json",
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: json,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {                
                if (data === true) {
                    self.reset();
                    ToastSuccess("Supplier created succesfully");
                }
            },
            error: function(xhr) {
                ToastError(xhr);
            }
        });

        return true;
    };

    self.reset = function () {
        self.name('');
        self.email('');
        self.contactNumber('');
        self.address('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.errors.showAllMessages(false);
        return true;
    };
    self.init = function () {
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
}

$(document).ready(function() {
    $('.datepicker').datepicker();
    
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);
});