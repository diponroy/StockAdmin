﻿

function Supplier(id, name, email, contactNumber, dateOfCreation) {
    this.id = ko.observable(id);
    this.name = ko.observable(name);
    this.email = ko.observable(email);
    this.contactNumber = ko.observable(contactNumber);
    this.dateOfCreation = ko.observable(dateOfCreation);
    this.updateUrl = ko.computed(function () {
        return '/SupplierManage/Update/' + id;
    }, this);
}



function ViewModel() {

    var self = this;

    self.suppliers = ko.observableArray([]),
    self.load = function () {
        $.ajax({
            url: '/SupplierManage/GetAll',
            dataType: "json",
            contentType: 'application/json',
            type: "GET",
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Supplier(data[i].Id, data[i].Name, data[i].Email, data[i].ContactNumber, clientDateStringFromDate(data[i].DateOfCreation)));
                });
                self.suppliers(arr);

                $('.data-table').dataTable({
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "sDom": '<""l>t<"F"fp>',
                    "bFilter": false,
                });
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
    };
    self.init = function () {
        self.load();
        activeParentMenu($('li.submenu a[href="/UserManage/Index"]:first'));
    };
};




$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);
    viewModel.init();
});