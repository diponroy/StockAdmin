﻿function Customer(id, name) {
    this.id = id;
    this.name = name;
}
function Product(id, name, unit) {
    this.id = id;
    this.name = name;
    this.unit = unit;
}
function ContactPerson(id, name) {
    this.id = id;
    this.name = name;
}
function Qutation(id, code) {
    this.id = id;
    this.code = code;
}
function Order(productId, productName, productUnit, quantity, price, vat) {
    this.productId = ko.observable(productId);
    this.productName = ko.observable(productName);
    this.productUnit = ko.observable(productUnit);
    this.quantity = ko.observable(quantity);
    this.price = ko.observable(price);
    this.vat = ko.observable(vat);
    this.total = parseFloat(price) + parseFloat(vat);
}

/*View Models*/
function OrderViewModel() {
    var self = this;
    self.productId = ko.observable().extend({ required: true });
    self.quantity = ko.observable().extend({ required: true, min: 1, digit:true});
    self.price = ko.observable().extend({ required: true, min: 0, number:true});
    self.vat = ko.observable('0').extend({ required: true, min: 0, number:true });
    self.errors = ko.validation.group(self);
    
    /*Old logs*/
    self.oldProductId = ko.observable('');
    self.oldProductName = ko.observable('');
    self.oldProductUnit = ko.observable('');
    self.oldQuantity = ko.observable('');
    self.oldPrice = ko.observable('');
    self.oldVat = ko.observable(0);
   
    self.reset = function () {
        self.productId('');
        self.quantity('');
        self.price('');
        self.vat(0);

        self.oldProductId('');
        self.oldProductName('');
        self.oldProductUnit('');
        self.oldQuantity('');
        self.oldPrice('');
        self.oldVat(0);
        self.errors.showAllMessages(false);
        return true;
    };
    
    self.show = function (item) {
        self.reset();
        self.productId(item.productId());
        self.quantity(item.quantity());
        self.price(item.price());
        self.vat(item.vat());
        
        self.oldProductId(item.productId());
        self.oldProductName(item.productName());
        self.oldProductUnit(item.productUnit());
        self.oldQuantity(item.quantity());
        self.oldPrice(item.price());
        self.oldVat(item.vat());
    };

    self.resetToOld = function() {
        self.productId(self.oldProductId());
        self.quantity(self.oldQuantity());
        self.price(self.oldPrice());
        self.vat(self.oldVat());
    };
}

function ViewModel() {
    var self = this;
    /*main view models call backs*/
    CustomerCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var custName = own.name();
            own.clear();
            self.loadCustomers();

            setTimeout(function () {
                var recentVal = $("#customerList option:contains('" + custName + "')").attr('value');
                $("#customerList").val(recentVal);
                $("#customerList").trigger("change");
            }, 500);
            $('#divCustomerSetup').modal('hide');
            ToastSuccess('Customer has been created successfully');
        }
    };
    ContactPersonCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {           
            var contactName = own.contactPersonName();
            self.loadContactPersons(own.customerId());
            own.reset();

            setTimeout(function () {
                var recentVal = $("#contactList option:contains('" + contactName + "')").attr('value');
                $("#contactList").val(recentVal);
                $("#contactList").trigger("change");
            }, 500);

            $('#divContactPersonSetup').modal('hide');
            ToastSuccess('Contact Person has been created successfully');
        }
    };
    ProductCreateVm.prototype.createdCallBack = function (data) {
        var productMainView = this;
        if (data == true) {          
            var lastProduct = productMainView.name();
            //productMainView.reset();
            $('#stack2').modal('hide');
            $('#stack1').removeData("modal").modal();
            self.loadProducts();

            setTimeout(function () {
                var recentVal = $("#sltproduct option:contains('" + lastProduct + "')").attr('value');
                $("#sltproduct").val(recentVal);
                $("#sltproduct").trigger("change");
            }, 500);

            ToastSuccess('Product has been created successfully');
        }
    };
    ProductUnitCreateVm.prototype.createdCallBack = function (data) {
        var own = this;
        if (data == true) {
            var lastUnit = own.name();
            own.clear();
            $('#stack4').modal('hide');
            $('#stack2').removeData("modal").modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#sltUnit').html("");
            self.productCreate.loadUnits();

            setTimeout(function () {
                var recentVal = $("#sltUnit option:contains('" + lastUnit + "')").attr('value');
                $("#sltUnit").val(recentVal);
                $("#sltUnit").trigger("change");
            }, 500);

            ToastSuccess('Product Unit has been created successfully');
        }
    };

    self.productUnitCreate = new ProductUnitCreateVm();
    self.customer = new CustomerCreateVm();
    self.contactperson = new ContactPersonCreateVm();
    self.productCreate = new ProductCreateVm();

    self.order = new OrderViewModel();
    self.customerName = ko.observable().extend({ required: true });
    self.contactPerson = ko.observable().extend({ required: true });
    self.dateOfCreation = ko.observable(currentDate()).extend({ required: true });
    self.marketQutationCode = ko.observable().extend({ required: true });
    self.orders = ko.observableArray([]);
    self.totalPrice = ko.computed(function() {
        var totalPrice = 0;
        var orders = self.orders();
        for (var i in orders) {
            totalPrice += parseFloat(orders[i].total);
        }
        return parseFloat(totalPrice).toFixed(2);
    }, this);
    self.errors = ko.validation.group([self.customerName, self.contactPerson, self.dateOfCreation, self.marketQutationCode]);
    
    self.customers = ko.observableArray([]);
    self.contactPersons = ko.observableArray([]);
    self.marketQutations = ko.observableArray([]);
    self.products = ko.observableArray([]);

    /*Resets*/
    self.init = function() {
        self.loadCustomers();
        self.loadProducts();
        activeParentMenu($('li.submenu a[href="/ClientQuotationManage/Index"]:first'));
    };
    self.reset = function () {
        self.customerName('');
        self.contactPerson('');
        self.marketQutationCode('');
        self.dateOfCreation('');
        self.dateOfCreation(currentDate());
        self.orders([]);
        self.loadCustomers();
        self.contactPersons([]);
        self.marketQutations([]);
        self.removeErrors();
        return true;
    };
    self.removeErrors = function() {
        self.errors.showAllMessages(false);
    };
    
    /*local Gets*/
    self.getQutationDetail = function() {
        var qutation = {
            CustomerId: self.customerName(),
            CustomerContactPersonId: self.contactPerson(),
            MarketQuotationId: self.marketQutationCode(),
            DateOfCreation: self.dateOfCreation()
        };

        var orderObj = [];
        var orders = self.orders();
        for (i in orders) {
            orderObj.push({
                ProductId: orders[i].productId(),
                Quantity: orders[i].quantity(),
                Price: orders[i].price(),
                Vat: orders[i].vat()
            });
        }

        return {
            quotation: qutation,
            orders: orderObj
        };
    };

    self.getProduct = function (id) {
        var products = self.products();
        var product = null;
        for (var i = 0; i < products.length; i++) {
            if (products[i].id === id) {
                product = products[i];
                break;
            }
        }
        return product;
    };
    
    /*local Actions*/
    self.showToUpdate = function (item) {
        self.order.show(item);
        self.orders.remove(item);
        $("#divActionCreateOrder").hide();
        $('#btnCloseModal').hide();
        $("#divActionUpdateOrder").show();
        $('#stack1').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
        return true;
    };
    
    self.addOrder = function (item) {
        var arr = self.orders();
        var product = self.getProduct(item.productId());
        arr.push(new Order(item.productId(), product.name, product.unit, item.quantity(), item.price(), item.vat()));
        self.orders([]);
        self.orders(arr);
    };

    self.createOrder = function (item) {
        self.order.errors.showAllMessages(false);
        if (self.order.errors().length > 0) {
            self.order.errors.showAllMessages();
            return;
        }
        self.addOrder(item);
        self.order.reset();
        ToastSuccess('Order added to quotation Orders.');
        $('#stack1').modal('hide');
    };

    self.updateOrder = function (item) {
        self.order.errors.showAllMessages(false);
        if (self.order.errors().length > 0) {
            self.order.errors.showAllMessages();
            return;
        }
        self.addOrder(item);
        self.order.reset();

        ToastSuccess('Order updated and added to Quotation Orders');
        $('#stack1').modal('hide');
    };
    
    self.cancelOrderUpdate = function (item) {
        self.order.resetToOld();
        self.order.errors.showAllMessages(false);
        if (self.order.errors().length > 0) {
            self.order.errors.showAllMessages();
            return;
        }
        self.addOrder(self.order);
        self.order.reset();
        
        $('#stack1').modal('hide');
    };
    
    self.removeOrder = function (item) {
        bootbox.confirm("Do you want to delete the Order?", function (result) {
            if (result === true) {
                self.orders.remove(item);
                ToastSuccess('Order deleted.');
            }
        });
    };
    
    /*Actions*/
    self.create = function () {
        self.removeErrors();
        if (self.errors().length > 0) {
            self.errors.showAllMessages();
            return true;
        }
        if (self.localCreate() == true) {
            var json = JSON.stringify(self.getQutationDetail());
            $.ajax({
                url: '/ClientQuotationManage/TryToCreate',
                dataType: "json",
                type: "POST",
                data: json,
                contentType: 'application/json; charset=utf-8',
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    if (data === true) {
                        self.reset();
                        ToastSuccess('Quotation added successfully');
                    }
                },
                error: function (xhr) {
                    ToastError(xhr);
                }
            });
        }
        return true;
    };
    
    self.localCreate = function () {
        if (self.orders().length == 0) {
            bootbox.alert("You have to add \"Quotation Order\" to create Client Quotation");
            return false;
        }
        return true;
    };

    /*Gets*/
    self.loadCustomers = function () {
        self.customers([]);        
        $.ajax({
            url: '/ClientQuotationManage/GetCustomers',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Customer(data[i].Id, data[i].Name));
                });
                self.customers(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadProducts = function () {
        self.products([]);
        $.ajax({
            url: '/ClientQuotationManage/GetProducts',
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Product(data[i].Id, data[i].Name, data[i].UnitName));
                });
                self.products(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };

    self.loadContactPersons = function (customerId) {
        self.contactPersons([]);
        $.ajax({
            url: '/ClientQuotationManage/GetContactPersons/' + customerId,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new ContactPerson(data[i].Id, data[i].Name));
                });
                self.contactPersons(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;
    };
    
    self.loadQutations = function (customerId) {
        self.marketQutations([]);
        $.ajax({
            url: '/ClientQuotationManage/GetMarketQuotations/' + customerId,
            dataType: "json",
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                var arr = [];
                $.each(data, function (i) {
                    arr.push(new Qutation(data[i].Id, data[i].QuotationCode));
                });
                self.marketQutations(arr);
            },
            error: function (xhr) {
                ToastError(xhr);
            }
        });
        return true;

    };
    
    /*Select change events*/
    self.customerName.subscribe(function (newValue) {
        self.orders([]);
        self.contactPersons([]);
        self.marketQutations([]);
        
        if (typeof (newValue) === 'undefined' || newValue === '') {
            self.removeErrors();
            return true;
        }
        self.loadContactPersons(newValue);
        self.loadQutations(newValue);
        return true;
    });
    
    self.marketQutationCode.subscribe(function (newValue) {
        if (typeof (newValue) === 'undefined' || newValue === '') {
            self.removeErrors();
            return true;
        }
        self.orders([]);
        return true;
    });
}


$(document).ready(function () {

    $('.datepicker').datepicker();
    
    $('.data-table').dataTable({
        "bJQueryUI": true,
        "sDom": '<""l>t<"F"fp>',
        "bFilter": false,
        "bPaginate": false,
        "bInfo": false
    });
    var viewModel = new ViewModel();
    viewModel.init();
    ko.applyBindings(viewModel);
    ko.validatedObservable(viewModel);

    $("#btnAddCustomer").click(function() {
        viewModel.customer.clear();
        $('#divCustomerSetup').removeData("modal").modal();
    });
    $("#btnAddContactPerson").click(function () {
        if (typeof (viewModel.customerName()) == 'undefined' || viewModel.customerName() == '') {
            bootbox.alert("You have to select a \"Customer\" first");
            return false;
        } else {
            viewModel.contactperson.reset();
            viewModel.contactperson.customerId(viewModel.customerName());
            $('#divContactPersonSetup').removeData("modal").modal();
        }
    });
    $("#btnClose3").click(function () {
        viewModel.productUnitCreate.clear();
        $('#stack4').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $("#btnAddOrder").click(function () {
        viewModel.order.reset();
        $("#divActionCreateOrder").show();
        $('#btnCloseModal').show();
        $("#divActionUpdateOrder").hide();
        $('#stack1').removeData("modal").modal();
    });
    $("#btnAddProduct").click(function () {
        viewModel.productCreate.reset();
        viewModel.productCreate.init();
        $('#stack1').modal('hide');
        $('#stack2').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });
	
    $("#btnClose1").click(function () {
        $('#stack1').modal('show');
    });   
});