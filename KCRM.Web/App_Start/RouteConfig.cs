﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KCRM.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "",        // URL with parameters
                new { controller = "LoginManage", action = "Login" }  // Parameter defaults
            );

            routes.MapRoute(
                name: "DefaultControllerAndAction",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Dashboard", action = "All", id = UrlParameter.Optional }
            );
        }
    }
}