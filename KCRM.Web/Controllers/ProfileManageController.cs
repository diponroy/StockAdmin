﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class ProfileManageController: KcrmController
    {
        private readonly ProfileManageLogic _logic;
        public ProfileManageController() : this(new ProfileManageLogic()){}
        public ProfileManageController(ProfileManageLogic logic) : base()
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult TryToUpdate(string password)
        {
            UserSessionModel model = GetUserSessionModel();
            User user = new User()
            {
                Id = model.Id,
                ReplacedByUserId = model.Id,
                Password = password,
                ReplacedDateTime = DateTime.Now
            };
            return Json(_logic.TryToUpdate(user), JsonRequestBehavior.AllowGet);
        }
	}
}