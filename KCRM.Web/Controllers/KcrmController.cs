﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class KcrmController : Controller
    {
        private const string UserDetailSession = "UserDetail";
        private const string MenuPermissionSession = "PermissionDetail";

        private readonly bool _permissionEnabled;
        private readonly PermissionEmun _currentPermission;

        protected KcrmController()
        {
            _permissionEnabled = false;
        }
        protected KcrmController(PermissionEmun permission)
        {
            _permissionEnabled = true;
            _currentPermission = permission;
        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (_permissionEnabled)
            {
                if (!HasSessions())
                {
                    Rederect(requestContext, Url.Action("Login", "LoginManage"));
                    return;
                }

                if (!HasPermission(_currentPermission))
                {
                    Rederect(requestContext, Url.Action("PermissionError", "Error"));
                }                
            }
            else
            {
                var controllerName = requestContext.RouteData.Values["controller"].ToString();
                if (!controllerName.ToLower().Equals("LoginManage".ToLower()) && !HasSessions())
                {
                    Rederect(requestContext, Url.Action("Login", "LoginManage"));
                }
            }
        }

        private void Rederect(RequestContext requestContext, string action)
        {
            requestContext.HttpContext.Response.Clear();
            requestContext.HttpContext.Response.Redirect(action);
            requestContext.HttpContext.Response.End();
        }

        protected bool HasSessions()
        {
            return this.Session[UserDetailSession] != null && this.Session[MenuPermissionSession] != null;
        }

        protected bool HasPermission(PermissionEmun permission)
        {
            return GetMenuSessionModel().Has(permission);
        }


        protected void AbandonSession()
        {
            if (HasSessions())
            {
                 Session.Abandon();          
            }
        }

        #region UserSessionModel
        protected void SetUserSessionModel(UserSessionModel userSessionModel)
        {
            this.Session[UserDetailSession] = userSessionModel;
            var value = GetUserSessionModel();
        }
        protected UserSessionModel GetUserSessionModel()
        {
            return (UserSessionModel)this.Session[UserDetailSession];
        }
        #endregion

        protected void SetMenuSessionModel(List<PermissionEmun> permission)
        {
            this.Session[MenuPermissionSession] = new MenuSessionModel(permission);
        }
        protected MenuSessionModel GetMenuSessionModel()
        {
           return (MenuSessionModel)this.Session[MenuPermissionSession];
        }
    }
}