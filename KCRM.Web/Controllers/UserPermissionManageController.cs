﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class UserPermissionManageController : KcrmController
    {
        private readonly UserPermissionManageLogic _logic;
        public UserPermissionManageController() : this(new UserPermissionManageLogic())
        {
            
        }
        public UserPermissionManageController(UserPermissionManageLogic logic) : base(PermissionEmun.UserPermissionSetup)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("All");
        }

        public ActionResult All()
        {
            return View(); 
        }


        public JsonResult GetUsers()
        {
            return Json(_logic.GetUsers(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPermissions()
        {
            return Json(_logic.GetPermissions(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserPermissions(long id)
        {
            return Json(_logic.GetNotRemovedUserPermissions(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToUpdatePermission(UserPermissionModel model)
        {
            var userSession = GetUserSessionModel();
            model.UpdatedDateTime = DateTime.Now;
            model.UpddatedByUserId = userSession.Id;
            return Json(_logic.UpdatePermission(model), JsonRequestBehavior.AllowGet);
        }
	}
}