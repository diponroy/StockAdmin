﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class SupplierManageController : KcrmController
    {
        private readonly SupplyerManageLogic _logic;

        public SupplierManageController()
            : this(new SupplyerManageLogic())
        {
            
        }
        public SupplierManageController(SupplyerManageLogic supplyerManageLogic):base(PermissionEmun.SupplierSetup)
        {
            this._logic = supplyerManageLogic;
        }

        #region Views
        public ActionResult Index()
        {
            return View("All");
        }
        public ActionResult All()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Update(long id)
        {
            return View(id);
        }
        #endregion

        public JsonResult GetAll()
        {
            return Json(_logic.GetAll(), JsonRequestBehavior.AllowGet);
        }

        #region Create
        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_logic.NameUsed(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsed(string email)
        {
            return Json(_logic.EmailUsed(email), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Supplier supplier)
        {
            supplier.AddedByUserId = GetUserSessionModel().Id;
            supplier.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAdd(supplier), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update
        [HttpPost]
        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsNameUsedExceptUser(string name, long id)
        {
            return Json(_logic.NameUsedExceptUser(name, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsedExceptUser(string email, long id)
        {
            return Json(_logic.EmailUsedExceptUser(email, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(Supplier supplier)
        {
            supplier.ReplacedByUserId = GetUserSessionModel().Id;
            supplier.ReplacedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(supplier), JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}