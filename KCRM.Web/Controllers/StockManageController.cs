﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class StockManageController : KcrmController
    {
        private readonly StoctManageLogic _logic;
        public StockManageController()
            : this(new StoctManageLogic())
        {
            
        }
        public StockManageController(StoctManageLogic customerManageLogic)
            : base(PermissionEmun.Stock)
        {
            _logic = customerManageLogic;
        }


        #region Views
        public ActionResult Index()
        {
            return View("Create");
        }

        public ActionResult Create()
        {
            return View();
        }
        #endregion

        #region Gets
        [HttpGet]
        public JsonResult GetCurrentStocks()
        {
            return Json(_logic.GetCurrentStocks(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet); 
        }
        #endregion

        #region Create
        [HttpPost]
        public JsonResult TryToCreate(Stock stock)
        {
            UserSessionModel model = GetUserSessionModel();
            stock.AddedByUserId = model.Id;
            stock.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAdd(stock), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}