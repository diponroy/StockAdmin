﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class ProductUnitManageController : KcrmController
    {
        private readonly ProductUnitManageLogic _logic;

        public ProductUnitManageController()
            : this(new ProductUnitManageLogic())
        {
            
        }
        public ProductUnitManageController(ProductUnitManageLogic productUnitManageLogic) : base(PermissionEmun.ProductSetup)
        {
            _logic = productUnitManageLogic;
        }     

        #region GetAll

        public ActionResult Index()
        {
            return View("All");
        }

        public ActionResult All()
        {
            return View();
        }

        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAll()
        {
            return Json(_logic.GetAll(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_logic.NameUsed(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(ProductUnit productUnit)
        {
            productUnit.AddedByUserId = GetUserSessionModel().Id;
            productUnit.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAdd(productUnit), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult IsNameUsedExceptProductUnit(string name, long id)
        {
            return Json(_logic.NameUsedExceptProductUnit(name, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(ProductUnit productUnit)
        {
            productUnit.ReplacedByUserId = GetUserSessionModel().Id;
            productUnit.ReplacedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(productUnit), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete

        [HttpPost]
        public JsonResult Delete(long id)
        {
            ProductUnit productUnit = new ProductUnit()
            {
                Id = id,
                ReplacedByUserId = GetUserSessionModel().Id,
                ReplacedDateTime = DateTime.Now
            };
            _logic.Remove(productUnit);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}