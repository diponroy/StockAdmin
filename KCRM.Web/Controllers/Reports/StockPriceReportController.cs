﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class StockPriceReportController : KcrmReportController
    {
        private readonly StockPriceReportLogic _logic;
        private const string TempModel = "StockPriceReportModel";

        public StockPriceReportController()
            : this(new StockPriceReportLogic())
        {

        }
        public StockPriceReportController(StockPriceReportLogic logic)
            : base(PermissionEmun.StockPriceReport)
        {
            _logic = logic;
        }

        private StockPriceReportModel GetTempData()
        {
            StockPriceReportModel model = (TempData[TempModel] == null)
                ? new StockPriceReportModel()
                : (StockPriceReportModel) TempData[TempModel];
            return model;
        }

        private ICollection<ProductStockPrice> GetStockPrice(StockPriceReportModel model)
        {
            return _logic.GetStockPrice(model.ProductCategory, model.ProductId);
        }

        public ActionResult Index()
        {
            return View("Search");
        }
        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SearchStockPrice(int productCategory, long productId)
        {
            var model = new StockPriceReportModel()
            {
                ProductCategory = productCategory,
                ProductId = productId
            };
            return Json(GetStockPrice(model), JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetTempModel(int productCategory, long productId, DateTime reportDate)
        {
            var model = new StockPriceReportModel()
            {
                ProductCategory = productCategory,
                ProductId = productId,
                ReportDate = reportDate
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PricePdf()
        {
            var model = GetTempData();
            model.StockPrices = GetStockPrice(model);
            return ViewPdf(" ", "PriceList", model);
        }
        public ActionResult Price()
        {
            var model = GetTempData();
            model.StockPrices = GetStockPrice(model);
            return View("PriceList", model);
        }

        [HttpGet]
        public JsonResult GetProductCategories()
        {
            return Json(_logic.GetProductCategories(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAllProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }
	}
}