﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.Expressions;
using iTextSharp.tool.xml.html;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models.Reports;
using KCRM.Web.Utility.PdfReport;

namespace KCRM.Web.Controllers.Reports
{
    public class StockReportController : KcrmReportController
    {
        private readonly StockReportLogic _logic;
        private const string TempModel = "StockReportModel";
        private StockReportModel GetTempData()
        {
            StockReportModel model = (TempData[TempModel] == null)
                ? new StockReportModel()
                : (StockReportModel) TempData[TempModel];

            return model; 
        }
        private ICollection<CurrentStock> GetCurrentStocks(StockReportModel model)
        {
            return _logic.GetCurrentStocks(model.CategoryId, model.ProductId);
        }

        public StockReportController()
            : this(new StockReportLogic())
        {
         
        }

        public StockReportController(StockReportLogic logic)
            : base(PermissionEmun.CurrentStockReport)
        {
            _logic = logic;
        }

        #region Views
        public ActionResult Index()
        {
            return View("Search");
        }
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SearchCurrentStock(int productCategory, long productId)
        {
            var model = new StockReportModel()
            {
                CategoryId = productCategory,
                ProductId = productId
            };
            return Json(GetCurrentStocks(model), JsonRequestBehavior.AllowGet);
        }
        public JsonResult SetTempModel(int productCategory, long productId, DateTime reportDate)
        {
            var model = new StockReportModel()
            {
                CategoryId = productCategory,
                ProductId = productId,
                ReportDate = reportDate
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CurrentStockPdf()
        {
            var model = GetTempData();
            model.CurrentStocks = GetCurrentStocks(model);
            return ViewPdf("", "CurrentStockList", model);
        }
        public ActionResult CurrentStock()
        {
            var model = GetTempData();
            model.CurrentStocks = GetCurrentStocks(model);
            return View("CurrentStockList", model);
        }
        #endregion

        [HttpGet]
        public JsonResult GetProductCategories()
        {
            return Json(_logic.GetProductCategories(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        
    }
}