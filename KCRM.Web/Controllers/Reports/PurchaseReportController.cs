﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;
using KCRM.Model.Models.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class PurchaseReportController : KcrmReportController
    {
        private readonly PurchaseReportLogic _logic;
        private const string TempModel = "PurchaseReportModel";

        public PurchaseReportController()
            : this(new PurchaseReportLogic())
        {
        }

        public PurchaseReportController(PurchaseReportLogic logic)
            : base(PermissionEmun.PurchaseReport)
        {
            _logic = logic;
        }

        private PurchaseReportModel GetTempData()
        {
            PurchaseReportModel model = (TempData[TempModel] == null)
                ? new PurchaseReportModel()
                : (PurchaseReportModel)TempData[TempModel];
            return model;
        }

        private ICollection<PurchaseModel> GetSearchResult(PurchaseReportModel model)
        {
            return _logic.GetSearchResult(model.SupplierId, model.CustomerId, model.UserId, model.FromDate, model.ToDate);
        }

        public ActionResult Index()
        {
            return View("Search");
        }

        public ActionResult Search()
        {
            return View();
        }
        
        #region GetAll

        [HttpGet]
        public JsonResult GetCustomers(long id)
        {
            return Json(_logic.GetCustomersBySupplierId(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetUsers(long customerId, long supplierId)
        {
            return Json(_logic.GetPurchaserByCustomerAndSupplier(customerId, supplierId), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSuppliers()
        {
            return Json(_logic.GetSuppliers(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public JsonResult SearchResult(long supplierId, long customerId, long userId, DateTime fromDate, DateTime toDate)
        {
            var model = new PurchaseReportModel()
            {
                SupplierId = supplierId,
                CustomerId = customerId,
                UserId = userId,
                FromDate = fromDate,
                ToDate = toDate
            };
            return Json(GetSearchResult(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTempModel(long supplierId, long customerId, long userId, DateTime fromDate, DateTime toDate, DateTime reportDate)
        {
            var model = new PurchaseReportModel()
            {
                SupplierId = supplierId,
                CustomerId = customerId,
                UserId = userId,
                FromDate = fromDate,
                ToDate = toDate,
                ReportDate = reportDate
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PurchasePdf()
        {
            var model = GetTempData();
            model.Purchases = GetSearchResult(model);
            return ViewPdf("", "PurchaseList",model);
        }
        public ActionResult Purchase()
        {
            var model = GetTempData();
            model.Purchases = GetSearchResult(model);
            return View("PurchaseList", model);
        }
    }
}