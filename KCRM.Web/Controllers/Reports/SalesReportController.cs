﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models;
using KCRM.Model.Models.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class SalesReportController : KcrmReportController
    {
        private readonly SalesReportLogic _logic;
        private const string TempModel = "SalesReportModel";

        public SalesReportController()
            : this(new SalesReportLogic())
        {

        }
        public SalesReportController(SalesReportLogic logic)
            : base(PermissionEmun.SalesReport)
        {
            _logic = logic;
        }

        private SalesReportModel GetTempData()
        {
            SalesReportModel model = (TempData[TempModel] == null)
                ? new SalesReportModel()
                : (SalesReportModel)TempData[TempModel];
            return model;
        }

        private ICollection<SaleModel> GetAllSales(SalesReportModel model)
        {

            return _logic.GetSales(model.FromDate, model.ToDate, model.CustomerId);
        }


        #region Views
        public ActionResult Index()
        {
            return View("Search");
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult SalesPdf()
        {
            var model = GetTempData();
            model.Sales = GetAllSales(model);
            return ViewPdf("", "Sales", model);
        }
        public ActionResult Sales()
        {
            var model = GetTempData();
            model.Sales = GetAllSales(model);
            return View(model);
        }
        #endregion

        #region Get
        [HttpGet]
        public JsonResult GetCustomers()
        {
            return Json(_logic.GetCustomer(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSales(DateTime fromDate, DateTime toDate, long customerId)
        {
            var model = new SalesReportModel()
            {
                FromDate = fromDate,
                ToDate = toDate,
                CustomerId = customerId
            };
            return Json(GetAllSales(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTempModel(DateTime fromDate, DateTime toDate, DateTime reportDate, long customerId)
        {
            var model = new SalesReportModel()
            {
                FromDate = fromDate,
                ToDate = toDate,
                ReportDate = reportDate,
                CustomerId = customerId
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
