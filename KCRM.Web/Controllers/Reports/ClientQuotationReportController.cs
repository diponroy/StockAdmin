﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class ClientQuotationReportController : KcrmReportController
    {
        private readonly ClientQuotationReportLogic _logic;
        private const string TempModel = "ClientQutationReportModel";
        public ClientQuotationReportController()
            : this(new ClientQuotationReportLogic())
        {

        }
        public ClientQuotationReportController(ClientQuotationReportLogic logic)
            : base(PermissionEmun.ClientQuotationReport)
        {
            _logic = logic;
        }

        private ClientQutationReportModel GetTempData()
        {
            ClientQutationReportModel model = (TempData[TempModel] == null)
                ? new ClientQutationReportModel()
                : (ClientQutationReportModel)TempData[TempModel];
            return model;
        }

        private ClientQutationReportModel GetClientQutationModel(ClientQutationReportModel model)
        {
            return _logic.GetClientQuotations(model);
        }

        #region Views
        public ActionResult Index()
        {
            return View("Search");
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult Quotation()
        {
            var list = _logic.GetClientQuotations(GetClientQutationModel(GetTempData()));
            return View(list);
        }

        public ActionResult QuotationPdf()
        {
            var list = _logic.GetClientQuotations(GetClientQutationModel(GetTempData()));
            return ViewPdf("", "Quotation", list);
        }

        public ActionResult SingleQuotation()
        {
            var model = _logic.GetClientQuotationsById(GetClientQutationModel(GetTempData()));
            return View(model);
        }

        public ActionResult SingleQuotationPdf()
        {
            var model = _logic.GetClientQuotationsById(GetClientQutationModel(GetTempData()));
            return ViewPdf("", "SingleQuotation", model);
        }
        #endregion

        #region Get
        [HttpGet]
        public JsonResult GetCustomers()
        {
            return Json(_logic.GetCustomers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetContactPersons(long id)
        {
            return Json(_logic.GetContactPersons(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPriceInspectorByCustomer(long id)
        {
            return Json(_logic.GetPriceInspectorByCustomer(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetMarketQuotations(long customerId, long contactPersonId, long priceInspectorId)
        {
            return Json(_logic.GetMarketQuotations(customerId, contactPersonId, priceInspectorId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetClientQuotations(ClientQutationReportModel model)
        {
            return Json(GetClientQutationModel(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTempModel(ClientQutationReportModel model)
        {
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
