﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;
using KCRM.Model.Models.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class MarketQuotationReportController : KcrmReportController
    {
        private readonly MarketQuotationReportLogic _logic;
        private const string TempModel = "MarketQuotationReportModel";

        public MarketQuotationReportController()
            : this(new MarketQuotationReportLogic())
        {
        }

        public MarketQuotationReportController(MarketQuotationReportLogic logic)
            : base(PermissionEmun.MarketQuotationReport)
        {
            _logic = logic;
        }

        private MarketQuotationReportModel GetTempData()
        {
            MarketQuotationReportModel model = (TempData[TempModel] == null)
                ? new MarketQuotationReportModel()
                : (MarketQuotationReportModel)TempData[TempModel];
            return model;
        }

        private ICollection<MarketQuotationModel> GetSearchResult(MarketQuotationReportModel model)
        {
            return _logic.GetSearchResult(model.QuotationId, model.CustomerId, model.InspectorId, model.FromDate, model.ToDate);
        }

        public ActionResult Index()
        {
            return View("Search");
        }
        public ActionResult Search()
        {
            return View();
        }

        #region GetAll

        [HttpGet]
        public JsonResult GetUsers()
        {
            return Json(_logic.GetUsers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCustomers(long id)
        {
            return Json(_logic.GetCustomersByInspectorId(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetQuotationCodes(long priceInspectorId, long customerId)
        {
            return Json(_logic.GetQuotationCodesBy(priceInspectorId, customerId), JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public JsonResult SearchResult(long quotationId, long customerId, long inspectorId, DateTime fromDate, DateTime toDate)
        {
            var model = new MarketQuotationReportModel()
            {
                QuotationId = quotationId,
                CustomerId = customerId,
                InspectorId = inspectorId,
                FromDate = fromDate,
                ToDate = toDate
            };
            return Json(GetSearchResult(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTempModel(long quotationId, long customerId, long inspectorId, DateTime fromDate, DateTime toDate, DateTime reportDate)
        {
            var model = new MarketQuotationReportModel()
            {
                QuotationId = quotationId,
                CustomerId = customerId,
                InspectorId = inspectorId,
                FromDate = fromDate,
                ToDate = toDate,
                ReportDate = reportDate
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MarketQuotationReport()
        {
            var model = GetTempData();
            model.Quotations = GetSearchResult(model);
            //return View("MarketQuotationList", GetSearchResult(GetTempData()));
            return View("MarketQuotationList", model);
        }

        public ActionResult MarketQuotationReportPdf()
        {
            var model = GetTempData();
            model.Quotations = GetSearchResult(model);
            //return ViewPdf("", "MarketQuotationList", GetSearchResult(GetTempData()));
            return ViewPdf("", "MarketQuotationList", model);
        }
    }
}