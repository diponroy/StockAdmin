﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class MarketPriceReportController
        : KcrmReportController
    {
        private readonly MarketPriceReportLogic _logic;
        private const string TempModel = "MarketPriceReportModel";

        public MarketPriceReportController()
            : this(new MarketPriceReportLogic())
        {
        }

        public MarketPriceReportController(MarketPriceReportLogic logic)
            : base(PermissionEmun.MarketPriceReport)
        {
            _logic = logic;
        }

        private MarketPriceReportModel GetTempData()
        {
            MarketPriceReportModel model = (TempData[TempModel] == null)
                ? new MarketPriceReportModel()
                : (MarketPriceReportModel)TempData[TempModel];
            return model;
        }

        private ICollection<ProductMarketPrice> GetMarketPrice(MarketPriceReportModel model)
        {
            return _logic.SearchCurrentPrice(model.ProductCategory, model.ProductId);
        }

        public ActionResult Index()
        {
            return View("Search");
        }

        public ActionResult Search()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SearchCurrentPrice(int productCategory, long productId)
        {
            var model = new MarketPriceReportModel()
            {
                ProductCategory = productCategory,
                ProductId = productId
            };
            return Json(GetMarketPrice(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTempModel(int productCategory, long productId, DateTime reportDate)
        {
            var model = new MarketPriceReportModel()
            {
                ProductCategory = productCategory,
                ProductId = productId,
                ReportDate = reportDate
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PricePdf()
        {
            var model = GetTempData();
            model.MarketPrices = GetMarketPrice(model);
            return ViewPdf("", "PriceList", model);
        }
        public ActionResult Price()
        {
            var model = GetTempData();
            model.MarketPrices = GetMarketPrice(model);
            return View("PriceList", model);
        }

        [HttpGet]
        public JsonResult GetProductCategories()
        {
            return Json(_logic.GetProductCategories(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetAllProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        
    }
}