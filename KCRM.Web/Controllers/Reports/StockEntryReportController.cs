﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using KCRM.Logic.Reports;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;
using KCRM.Model.Models.Reports;
using KCRM.Web.Controllers.Reports;

namespace KCRM.Web.Controllers.Reports
{
    public class StockEntryReportController : KcrmReportController
    {
        private readonly StockEntryReportLogic _logic;
        private const string TempModel = "StockEntryReportModel";


        public StockEntryReportController()
            : this(new StockEntryReportLogic())
        {

        }

        public StockEntryReportController(StockEntryReportLogic logic)
            : base(PermissionEmun.StockEntryReport)
        {
            _logic = logic;
        }

        private StockEntryReportModel GetTempData()
        {
            StockEntryReportModel model = (TempData[TempModel] == null)
                ? new StockEntryReportModel()
                : (StockEntryReportModel)TempData[TempModel];
            return model;
        }

        private ICollection<StockModel> GetAllStock(StockEntryReportModel model)
        {
            return _logic.GetStockEntry(model.FromDate, model.ToDate, model.ProductCategoryId, model.ProductId);
        }

        #region views
        public ActionResult Index()
        {
            return View("Search");
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult StockEnrty()
        {
            var model = GetTempData();
            model.Stocks = GetAllStock(model);
            return View("StockEnrtyList", model);
        }

        public ActionResult StockEnrtyPdf()
        {
            var model = GetTempData();
            model.Stocks = GetAllStock(model);
            return ViewPdf("", "StockEnrtyList", model);
        }
        #endregion

        #region GET

        [HttpGet]
        public JsonResult GetProductCategories()
        {
            return Json(_logic.GetProductCategories(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetAllProducts()
        {
            return Json(_logic.GetAllProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SearchtStockEntry(DateTime fromDate, DateTime toDate, int productCategoryId, long productId)
        {
            var model = new StockEntryReportModel()
            {
                FromDate = fromDate,
                ToDate = toDate,
                ProductCategoryId = productCategoryId,
                ProductId = productId
            };
            return Json(GetAllStock(model), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetTempModel(DateTime fromDate, DateTime toDate, DateTime reportDate, int productCategoryId, long productId)
        {
            var model = new StockEntryReportModel()
            {
                FromDate = fromDate,
                ToDate = toDate,
                ReportDate = reportDate,
                ProductCategoryId = productCategoryId,
                ProductId = productId
            };
            TempData[TempModel] = model;
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}

    