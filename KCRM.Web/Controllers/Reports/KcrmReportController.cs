﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Web.Utility.PdfReport;

namespace KCRM.Web.Controllers.Reports
{
    public class KcrmReportController : KcrmController
    {
        protected KcrmReportController()
            : base()
        {
        }
        protected KcrmReportController(PermissionEmun permission) 
            : base(permission)
        {
        }
        protected ActionResult ViewPdf(string pageTitle, string viewName, object model)
        {
            var htmlText = new HtmlViewRenderer().RenderViewToString(this, viewName, model);
            byte[] buffer = new StandardPdfRenderer().Render(htmlText, pageTitle);
            return new BinaryContentResult(buffer, "application/pdf");
        }
	}
}