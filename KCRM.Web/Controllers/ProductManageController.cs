﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class ProductManageController : KcrmController
    {
        private readonly ProductManageLogic _logic;

        public ProductManageController()
            : this(new ProductManageLogic())
        {
            
        }
        public ProductManageController(ProductManageLogic productManageLogic) :base(PermissionEmun.ProductSetup)
        {
            _logic = productManageLogic;
        }   

        #region Get

        public ActionResult Index()
        {
            return View("All");
        }

        public ActionResult All()
        {
            return View();
        }

        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAll()
        {
            return Json(_logic.GetAll(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductCategories()
        {
            return Json(_logic.GetProductCategories(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductUnits()
        {
            return Json(_logic.GetProductUnits(), JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_logic.NameUsed(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Product product)
        {   
            product.AddedByUserId = GetUserSessionModel().Id;
            product.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAdd(product), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update

        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult IsNameUsedExceptProduct(string name, long id)
        {
            return Json(_logic.NameUsedExceptProduct(name, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(Product product)
        {
            product.ReplacedByUserId = GetUserSessionModel().Id;
            product.ReplacedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(product), JsonRequestBehavior.AllowGet);
        }
        #endregion   
    }
}