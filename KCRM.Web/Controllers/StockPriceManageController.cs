﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class StockPriceManageController: KcrmController
    {
        private readonly StockPriceManageLogic _logic;
        public StockPriceManageController()
            : this(new StockPriceManageLogic())
        {
            
        }
        public StockPriceManageController(StockPriceManageLogic logic)
            : base(PermissionEmun.StockPrice)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("Update");
        }

        public ActionResult Update()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetPrice(long id)
        {
            return Json(_logic.GetPrice(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToUpdate(StockPriceList stockPriceList)
        {
            UserSessionModel model = GetUserSessionModel();
            stockPriceList.AddedByUserId = model.Id;
            stockPriceList.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(stockPriceList), JsonRequestBehavior.AllowGet);
        }   
        [HttpGet]
        public JsonResult GetStockPrices()
        {
            return Json(_logic.GetStockPrices(), JsonRequestBehavior.AllowGet);
        }
	}
}