﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class MarketPriceManageController : KcrmController
    {
        private readonly MarketPriceManageLogic _logic;
        public MarketPriceManageController()
            : this(new MarketPriceManageLogic())
        {
            
        }
        public MarketPriceManageController(MarketPriceManageLogic logic)
            : base(PermissionEmun.MarketPrice)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("Update");
        }

        public ActionResult Update()
        {
            return View();
        }


        /*Get Data in Product Name DropDown Box*/
        [HttpGet]
        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToUpdate(MarketPriceList marketPriceList)
        {
            UserSessionModel model = GetUserSessionModel();
            marketPriceList.AddedByUserId = model.Id;
            marketPriceList.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(marketPriceList), JsonRequestBehavior.AllowGet);
        }

        /*Price in list*/
        [HttpGet]
        public JsonResult GetMarketPrice()
        {
            return Json(_logic.GetMarketPrice(), JsonRequestBehavior.AllowGet);
        }

        /*Price in txt box for update*/
        [HttpGet]
        public JsonResult GetPriceForUpdate(long id)
        {
            return Json(_logic.GetPriceForUpdate(id), JsonRequestBehavior.AllowGet);
        }

        
	}
}