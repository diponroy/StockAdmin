﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;
using KCRM.Db;


namespace KCRM.Web.Controllers
{
    public class LoginManageController : KcrmController
    {
        private readonly LoginManageLogic _logic;

        public LoginManageController() : this(new LoginManageLogic())
        {
        }

        public LoginManageController(LoginManageLogic loginManageLogic) : base()
        {
            _logic = loginManageLogic;
        }

        public ActionResult Index()
        {
            return Login();
        }

        public ActionResult Login()
        {
            new DbBuildInitializer().CreateDatabaseIfNotExistsWithSeedData();
            return View();
        }

        public ActionResult LogOut()
        {
            base.AbandonSession();
            return View("Login");
        }

        [HttpPost]
        public JsonResult TryToCreateSession(string loginName, string password)
        {
            bool isAthorized = _logic.IsAuthorizedToLogin(loginName, password);
            if (isAthorized)
            {
                UserSessionModel userSession = _logic.GetUser(loginName, password);
                base.SetUserSessionModel(userSession);

                List<PermissionEmun> permissions = _logic.GetPermissions(userSession.Id, userSession.UserType);
                base.SetMenuSessionModel(permissions);
            }
            return Json(isAthorized, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToRecover(string loginName, string email)
        {
            return Json(_logic.TryToRecover(loginName, email), JsonRequestBehavior.AllowGet);
        }
	}
}