﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class ClientQuotationManageController : KcrmController
    {
        private readonly ClientQuotationManageLogic _logic;
        public ClientQuotationManageController()
            : this(new ClientQuotationManageLogic())
        {
            
        }
        public ClientQuotationManageController(ClientQuotationManageLogic customerManageLogic)
            : base(PermissionEmun.ClientQuotation)
        {
            _logic = customerManageLogic;
        } 

        public ActionResult Index()
        {
            return View("Create");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetCustomers()
        {
            return Json(_logic.GetCustomers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetContactPersons(long id)
        {
            return Json(_logic.GetContactPersons(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetMarketQuotations(long id)
        {
            return Json(_logic.GetMarketQuotations(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToCreate(ClientQuotation quotation, List<ClientQuotationOrder> orders)
        {
            quotation.AddedByUserId = GetUserSessionModel().Id;
            quotation.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToCreate(quotation, orders), JsonRequestBehavior.AllowGet);
        }
	}
}