﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class CustomerManageController : KcrmController
    {
        private readonly CustomerManageLogic _logic;
        public CustomerManageController()
            : this(new CustomerManageLogic())
        {

        }
        public CustomerManageController(CustomerManageLogic customerManageLogic)
            : base(PermissionEmun.CustomerSetup)
        {
            _logic = customerManageLogic;
        }

        #region GetAll

        public ActionResult Index()
        {
            return View("All");
        }

        public ActionResult All()
        {
            return View();
        }

        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAll()
        {
            return Json(_logic.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetContactPersons(long id)
        {
            return Json(_logic.GetContactPersons(id), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IsNameUsed(string name)
        {
            return Json(_logic.NameUsed(name), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsed(string email)
        {
            return Json(_logic.EmailUsed(email), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(Customer customer, List<CustomerContactPerson> contactPersons)
        {
            customer.DateOfCreation = DateTime.Now.Date;
            customer.AddedByUserId = GetUserSessionModel().Id;
            customer.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAdd(customer, contactPersons), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateContactPerson(CustomerContactPerson contactPerson)
        {
            contactPerson.AddedByUserId = GetUserSessionModel().Id;
            contactPerson.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAddContactPerson(contactPerson), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update

        [HttpGet]
        public ActionResult Update(long id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult IsNameUsedExceptCustomer(string name, long id)
        {
            return Json(_logic.NameUsedExceptCustomer(name, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsedExceptCustomer(string email, long id)
        {
            return Json(_logic.EmailUsedExceptCustomer(email, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(Customer customer, List<CustomerContactPerson> createdPersons, List<CustomerContactPerson> updatedPersons, List<long> removedPersons)
        {
            customer.ReplacedByUserId = GetUserSessionModel().Id;
            customer.ReplacedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(customer, createdPersons, updatedPersons, removedPersons), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}