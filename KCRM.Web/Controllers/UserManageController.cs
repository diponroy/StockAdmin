﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class UserManageController : KcrmController
    {
        private readonly UserManageLogic _logic;

        public UserManageController() : this(new UserManageLogic())
        {
            
        }
        public UserManageController(UserManageLogic userManageLogic) : base(PermissionEmun.UserSetup)
        {
            this._logic = userManageLogic;
        }

        #region Views
        public ActionResult Index()
        {
            return View("ALL");
        }

        public ActionResult All()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Update(long id)
        {
            return View(id);
        }
        #endregion

        [HttpPost]
        public JsonResult GetUserTypes()
        {
            return Json(_logic.GetUserTypes(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAll()
        {
            return Json(_logic.GetAll(), JsonRequestBehavior.AllowGet);
        }

        #region Create
        [HttpPost]
        public JsonResult Create(User user)
        {
            user.AddedByUserId = GetUserSessionModel().Id;
            user.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToAdd(user), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsLoginNameUsed(string loginName)
        {
            return Json(_logic.LoginNameUsed(loginName), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailUsed(string email)
        {
            return Json(_logic.EmailUsed(email), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Update
        public JsonResult Get(long id)
        {
            return Json(_logic.Get(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsLoginNameUsedExceptUser(string loginName, long id)
        {
            return Json(_logic.LoginNameUsedExceptUser(loginName, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult IsEmailExceptUser(string email, long id)
        {
            return Json(_logic.EmailExceptUser(email, id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Update(User user)
        {
            user.ReplacedByUserId = GetUserSessionModel().Id;
            user.ReplacedDateTime = DateTime.Now;
            return Json(_logic.TryToReplace(user), JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public JsonResult Delete(long id)
        {
            User user = new User()
            {
                Id = id,
                ReplacedByUserId = GetUserSessionModel().Id,
                ReplacedDateTime = DateTime.Now
            };
            _logic.Remove(user);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}