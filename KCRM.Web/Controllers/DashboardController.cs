﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KCRM.Web.Controllers
{
    public class DashboardController : KcrmController
    {
        public DashboardController() : base()
        {
            
        }
        public ActionResult Index()
        {
            return View("All");
        }

        public ActionResult All()
        {
            return View();
        }
	}
}