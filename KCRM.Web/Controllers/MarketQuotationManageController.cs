﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class MarketQuotationManageController : KcrmController
    {
        private readonly MarketQuotationManageLogic _logic;

        public MarketQuotationManageController()
            : this(new MarketQuotationManageLogic())
        {
            
        }

        public MarketQuotationManageController(MarketQuotationManageLogic marketQuotationManageLogic)
            : base(PermissionEmun.MarketQuotation)
        {
            _logic = marketQuotationManageLogic;
        }

        public ActionResult Index()
        {
            return View("Create");
        }

        public ActionResult Create()
        {
            return View();
        }

        #region GetAll

        [HttpGet]
        public JsonResult GetCustomers()
        {
            return Json(_logic.GetCustomers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            return Json(_logic.GetUsers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSuppliers()
        {
            return Json(_logic.GetSuppliers(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public JsonResult IsCodeUsed(string code)
        {
            return Json(_logic.CodeUsed(code), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNextCode()
        {
            return Json(_logic.GetNextCode(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToCreate(MarketQuotation quotation, List<MarketQuotationInspection> inspections)
        {
            UserSessionModel model = GetUserSessionModel();
            quotation.AddedByUserId = model.Id;
            quotation.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToCreate(quotation, inspections), JsonRequestBehavior.AllowGet);
        }
    }
}