﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class SaleManageController : KcrmController
    {
        private readonly SaleManageLogic _logic;

        public SaleManageController() : this(new SaleManageLogic()){}
        public SaleManageController(SaleManageLogic logic)
            : base(PermissionEmun.Sale)
        {
            _logic = logic;
        }

        public ActionResult Index()
        {
            return View("Create");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCustomers()
        {
            return Json(_logic.GetCustomer(), JsonRequestBehavior.AllowGet);           
        }

        [HttpPost]
        public JsonResult HasEnoughStock(long productId, long quantity)
        {
            return Json(_logic.HasEnoughStock(productId, quantity), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TryToCreate(Sale sale, List<SoldProducts> products)
        {
            UserSessionModel model = GetUserSessionModel();
            sale.AddedByUserId = model.Id;
            sale.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToCreate(sale, products), JsonRequestBehavior.AllowGet);
        }
	}
}