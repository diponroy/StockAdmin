﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KCRM.Logic;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;

namespace KCRM.Web.Controllers
{
    public class PurchaseManageController : KcrmController
    {
        private readonly PurchaseManageLogic _logic;

        public PurchaseManageController()
            : this(new PurchaseManageLogic())
        {
            
        }

        public PurchaseManageController(PurchaseManageLogic purchaseManageLogic)
            : base(PermissionEmun.Purchase)
        {
            _logic = purchaseManageLogic;
        }

        public ActionResult Index()
        {
            return View("Create");
        }

        public ActionResult Create()
        {
            return View();
        }

        #region GetAll

        [HttpGet]
        public JsonResult GetCustomers()
        {
            return Json(_logic.GetCustomers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            return Json(_logic.GetUsers(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProducts()
        {
            return Json(_logic.GetProducts(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSuppliers()
        {
            return Json(_logic.GetSuppliers(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Create

        [HttpPost]
        public JsonResult TryToCreate(Purchase purchase, List<PurchasedProducts> purchasedProducts)
        {
            UserSessionModel model = GetUserSessionModel();
            purchase.AddedByUserId = model.Id;
            purchase.AddedDateTime = DateTime.Now;
            return Json(_logic.TryToCreate(purchase, purchasedProducts), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}