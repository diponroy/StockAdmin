﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Tables.Mimics;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class ProductManageLogic
    {
        private readonly ProductData _data;
        public ProductManageLogic() 
        {
            _data = new ProductData();
        }

        public ProductModel Get(long id)
        {
            var product = _data.GetWithUnit(id);
            return new ProductModel()
            {
                Id = product.Id,
                Name = product.Name,
                Category = product.Category,
                UnitName = product.Unit.Name,
                Description = product.Description,
                DateOfCreation = product.DateOfCreation
            };
        }

        public ICollection<ProductModel> GetAll()
        {
            return _data.GetAll();           
        }

        public bool TryToAdd(Product product)
        {
            if (IsValidToAdd(product))
            {
                _data.Add(product);
                _data.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidToAdd(Product product)
        {
            bool validToAdd = !NameUsed(product.Name);
            return validToAdd;
        }

        public bool NameUsed(string name)
        {
            bool isUsed = _data.AnyName(name);
            return isUsed;
        }

        public bool NameUsedExceptProduct(string name, long id)
        {
            bool isUsed = _data.AnyNameExceptProduct(name, id);
            return isUsed;
        }

        public bool TryToReplace(Product product)
        {
            if (IsValidToReplace(product))
            {
                _data.Replace(product);
                _data.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsValidToReplace(Product product)
        {
            bool validToUpdate = !NameUsedExceptProduct(product.Name, product.Id);
            return validToUpdate;
        }

        public List<ProductCategory> GetProductCategories()
        {
            return new ProductCategoryData().GetAll().ToList();
        }

        public ICollection<ProductUnit> GetProductUnits()
        {
            return new ProductUnitData().GetAll().ToList();
        }
    }
}
