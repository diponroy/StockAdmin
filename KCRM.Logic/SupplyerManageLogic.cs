﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data;
using KCRM.Data.Tables;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class SupplyerManageLogic
    {
        private readonly SupplierData _data;

        public SupplyerManageLogic()
        {
            _data = new SupplierData();
        }

        public bool IsValidToAdd(Supplier supplier)
        {
            return !NameUsed(supplier.Name) && !EmailUsed(supplier.Email);
        }

        public bool NameUsed(string name)
        {
            return _data.AnyName(name);
        }


        public bool EmailUsed(string email)
        {
            if (!HasEmail(email))
            {
                return false;
            }               
            return _data.AnyEmail(email);
        }

        private static bool HasEmail(string email)
        {
            return !string.IsNullOrEmpty(email);
        }

        public bool TryToAdd(Supplier supplier)
        {
            if (!IsValidToAdd(supplier))
            {
                return false;              
            }
            _data.Add(supplier);
            _data.SaveChanges();
            return true;
        }

        public bool IsValidToReplace(Supplier supplier)
        {
            return !EmailUsedExceptUser(supplier.Email, supplier.Id) && !NameUsedExceptUser(supplier.Name, supplier.Id);
        }

        public bool EmailUsedExceptUser(string email, long id)
        {
            if (!HasEmail(email))
            {
                return false;
            } 
            return _data.AnyEmailExceptUser(email, id);
        }

        public bool NameUsedExceptUser(string name, long id)
        {
            return _data.AnyNameExceptUser(name, id);
        }

        public bool TryToReplace(Supplier supplier)
        {
            if (!IsValidToReplace(supplier))
            {
                return false;
            }
            _data.Replace(supplier);
            _data.SaveChanges();
            return true;
        }

        public SupplierModel Get(long id)
        {
            var supplier = _data.Get(id);
            return new SupplierModel()
            {
                Id = supplier.Id,
                Name = supplier.Name,
                Email = supplier.Email,
                DateOfCreation = supplier.DateOfCreation,
                ContactNumber = supplier.ContactNumber,
                Address = supplier.Address
            };
        }

        public ICollection<SupplierModel> GetAll()
        {
            var supplyers = _data.Get()
            .Select(x => new SupplierModel()
            {
                Id = x.Id,
                Name = x.Name,
                Email = x.Email,
                DateOfCreation = x.DateOfCreation,
                ContactNumber = x.ContactNumber,
                Address = x.Address
            }).ToList();
            return supplyers;
        }
    }
}
