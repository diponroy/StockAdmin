﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class StockPriceManageLogic
    {
        private readonly StockPriceListData _data;
        public StockPriceManageLogic()
        {
            _data = new StockPriceListData();
        }

        public bool Add(StockPriceList stockPriceList)
        {
            _data.Add(stockPriceList);
            _data.SaveChanges();
            return true;
        }

        public bool Replace(StockPriceList stockPriceList)
        {
            _data.Replace(stockPriceList);
            _data.SaveChanges();
            return true;
        }

        public bool TryToReplace(StockPriceList stockPriceList)
        {
            if (_data.AnyByProductId(stockPriceList.ProductId))
            {
                stockPriceList.ReplacedByUserId = stockPriceList.AddedByUserId;
                stockPriceList.ReplacedDateTime = stockPriceList.AddedDateTime;
                Replace(stockPriceList);
            }
            else
            {
                Add(stockPriceList);
            }
            return true;
        }

        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return
                data.GetWithUnit()
                .Select(x => new ProductModel() { Id = x.Id, Name = x.Name, UnitName = x.Unit.Name })
                .ToList();
        }
        public ICollection<StockPriceListModel> GetStockPrices()
        {
            var data = new ProductStockPriceData();
            return
                data.Get()
                .Select(x => new StockPriceListModel() { Id = x.Id, Price = x.Price, ProductName = x.Name })
                .ToList();
        }

       
        public StockPriceList GetPrice(long productId)
        {
            var value = _data.FindByProductId(productId);
            return value ?? new StockPriceList();
        }


    }
}
