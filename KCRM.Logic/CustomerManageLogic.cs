﻿using System;
using System.Collections.Generic;
using System.Linq;
using KCRM.Data.Tables;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Logic
{
    public class CustomerManageLogic
    {
        private readonly CustomerData _data;
        private CustomerContactPersonData _dataPerson;
        public CustomerManageLogic()
        {
            _data = new CustomerData();
        }

        public Customer Get(long id)
        {
            var customer = _data.Get(id);

            return new Customer()
            {
                Name = customer.Name,
                Email = customer.Email,
                ContactNumber = customer.ContactNumber,
                Address = customer.Address,
                DateOfCreation = customer.DateOfCreation,
                AddedDateTime = customer.AddedDateTime,
                AddedByUserId = customer.AddedByUserId,
                ReplacedDateTime = customer.ReplacedDateTime,
                ReplacedByUserId = customer.ReplacedByUserId
            };
        }
        public ICollection<Customer> GetAll()
        {
            return _data.GetAll();
        }
        public bool TryToAdd(Customer customer, List<CustomerContactPerson> contactPersons)
        {
            if (!IsValidToAdd(customer))
            {
                return false;
            }
            _data.Add(customer);
            _data.SaveChanges();

            contactPersons = contactPersons ?? new List<CustomerContactPerson>();
            _dataPerson = new CustomerContactPersonData(_data.DbContext);
            contactPersons.ForEach(
                obj =>
                {
                    obj.CustomerId = customer.Id;
                    obj.AddedByUserId = customer.AddedByUserId;
                    obj.AddedDateTime = DateTime.Now;
                    _dataPerson.Add(obj);
                });
            _dataPerson.SaveChanges();

            return true;
        }

        public bool TryToAddWhileUpdate(Customer customer, List<CustomerContactPerson> contactPersons)
        {
            contactPersons = contactPersons ?? new List<CustomerContactPerson>();
            _dataPerson = new CustomerContactPersonData(_data.DbContext);
            contactPersons.ForEach(
                obj =>
                {
                    obj.CustomerId = customer.Id;
                    obj.AddedByUserId = customer.ReplacedByUserId;
                    obj.AddedDateTime = DateTime.Now;
                    _dataPerson.Add(obj);
                });
            _dataPerson.SaveChanges();

            return true;
        }
        public bool IsValidToAdd(Customer customer)
        {
            bool validToAdd = !NameUsed(customer.Name) && !EmailUsed(customer.Email);
            return validToAdd;
        }
        public bool NameUsed(string name)
        {
            bool isUsed = _data.AnyName(name);
            return isUsed;
        }
        public bool EmailUsed(string email)
        {
            bool isUsed = _data.AnyEmail(email);
            return isUsed;
        }
        public bool NameUsedExceptCustomer(string name, long id)
        {
            bool isUsed = _data.AnyNameExceptCustomer(name, id);
            return isUsed;
        }

        public bool EmailUsedExceptCustomer(string email, long id)
        {
            bool isUsed = _data.AnyEmailExceptCustomer(email, id);
            return isUsed;
        }
        public bool TryToReplace(Customer customer, List<CustomerContactPerson> createdPersons, List<CustomerContactPerson> updatedPersons, List<long> removedPersons)
        {
            if (!IsValidToReplace(customer))
            {
                return false;
            }
            _data.Replace(customer);

            _dataPerson = new CustomerContactPersonData(_data.DbContext);

            TryToAddWhileUpdate(customer, createdPersons);

            updatedPersons = updatedPersons ?? new List<CustomerContactPerson>();
            updatedPersons.ForEach(obj =>
            {
                obj.ReplacedByUserId = customer.ReplacedByUserId;
                obj.ReplacedDateTime = customer.ReplacedDateTime;

                _dataPerson.Replace(obj);
            });

            removedPersons = removedPersons ?? new List<long>();
            removedPersons.ForEach(obj =>
            {
                var removedPerson = new CustomerContactPerson()
                {
                    Id = obj,
                    ReplacedByUserId = customer.ReplacedByUserId,
                    ReplacedDateTime = customer.ReplacedDateTime
                };
                _dataPerson.Remove(removedPerson);
            });

            _data.SaveChanges();
            return true;
        }
        private bool IsValidToReplace(Customer customer)
        {
            bool validToUpdate = !NameUsedExceptCustomer(customer.Name, customer.Id) && !EmailUsedExceptCustomer(customer.Email, customer.Id);
            return validToUpdate;
        }

        public IQueryable<CustomerContactPerson> GetContactPersons(long id)
        {
            CustomerContactPersonData _data = new CustomerContactPersonData();
            IQueryable<CustomerContactPerson> lists = _data.GetNotRemovedByCustomerId(id);
            return lists;
        }

        public bool TryToAddContactPerson(CustomerContactPerson contactPerson)
        {
            _dataPerson = new CustomerContactPersonData();
            _dataPerson.Add(contactPerson);
            _dataPerson.SaveChanges();
            return true;
        }
    }
}
