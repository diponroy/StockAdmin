﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class StoctManageLogic
    {
        public ICollection<CurrentStock> GetCurrentStocks()
        {
            return new CurrentStockData().Get().ToList();
        }
        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return
                data.GetWithUnit()
                .Select(x => new ProductModel() { Id = x.Id, Name = x.Name, UnitName = x.Unit.Name })
                .OrderBy(x=>x.Name)
                .ToList();
        }

        public bool TryToAdd(Stock stock)
        {
            var data = new StockData();
            data.Add(stock);
            data.SaveChanges();
            return true;
        }
    }
}
