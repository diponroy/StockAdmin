﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class ClientQuotationManageLogic
    {
        private readonly ClientQuotationData _data;
        public ClientQuotationManageLogic() 
        {
            _data = new ClientQuotationData();
        }
        public ICollection<CustomerModel> GetCustomers()
        {
            CustomerData data = new CustomerData();
            return data.Get().Select(x => new CustomerModel(){ Id = x.Id, Name = x.Name}).ToList();
        }
        public ICollection<CustomerContactPersonModel> GetContactPersons(long id)
        {
            CustomerContactPersonData data = new CustomerContactPersonData();
            return
                data.GetNotRemovedByCustomerId(id)
                    .Select(x => new CustomerContactPersonModel() { Id = x.Id, Name = x.Name, ContactNumber = x.ContactNumber })
                    .OrderBy(x=>x.Name)
                    .ToList();
        }

        public ICollection<MarketQuotationModel> GetMarketQuotations(long customerId)
        {
            MarketQuotationData data = new MarketQuotationData();
            return
                data.GetByCustomerId(customerId)
                    .Select(x => new MarketQuotationModel() { Id = x.Id, QuotationCode = x.QuotationCode })
                    .ToList();
        }

        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return 
                data.GetWithUnit()
                .Select(x => new ProductModel() { Id = x.Id, Name = x.Name, UnitName = x.Unit.Name })
                .OrderBy(x=>x.Name)
                .ToList();
        }

        public bool TryToCreate(ClientQuotation quotation, List<ClientQuotationOrder> orders)
        {
            if (orders == null || orders.Count == 0)
            {
                throw new NullReferenceException("Orders list is null");
            }

            /*Add only quotation*/
            _data.Add(quotation);
            _data.SaveChanges();

            /*Add quotation orders*/
            orders = orders ?? new List<ClientQuotationOrder>();
            ClientQuotationOrderData data = new ClientQuotationOrderData(_data.DbContext);
            orders.ForEach(obj =>
            {
                obj.ClientQuotationId = quotation.Id;
                data.Add(obj);
            });
            data.SaveChanges();

            return true;
        }
    }
}
