﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Tables.Mimics;
using KCRM.Data.Views;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models;

namespace KCRM.Logic.Reports
{
    public class StockPriceReportLogic
    {
       public ICollection<ProductStockPrice> GetStockPrice(int productCategory, long productId)
        {
            var list = new ProductStockPriceData().Get()
                        .AsQueryable()
                        .WhereIf(productCategory > 0, x => x.Category == (ProductCategoryEnum)productCategory)
                        .WhereIf(productId > 0, x => x.Id == productId)
                        .ToList();
           var data = new ProductCategoryData();
           list.ForEach(obj =>
           {
               obj.CategoryName = data.GetName(obj.Category);
           });
           return list;
        }


        public List<ProductCategory> GetProductCategories()
        {
            return new ProductCategoryData().GetAll().ToList();
        }
        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return data.Get()
                        .Select(x => new ProductModel()
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Category = x.Category,
                            CategoryName = x.Category.ToString(),
                            UnitName = x.Unit.Name,
                            Description = x.Description,
                            DateOfCreation = x.DateOfCreation
                        })
                        .OrderBy(x=>x.Name)
                        .ToList();
        }
    }
}
