﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Db.Configurations;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic.Reports
{
    public class MarketQuotationReportLogic
    {
        private readonly MarketQuotationData _data;

        public MarketQuotationReportLogic() 
        {
            _data = new MarketQuotationData();
        }

        public ICollection<UserModel> GetUsers()
        {
            var data = new UserData();
            List<UserModel> users
                = data.GetNotRemoved().Select(x => new UserModel()
                {
                    Id = x.Id,
                    LoginName = x.LoginName,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                })
                .OrderBy(x=>x.LoginName)
                .ToList();
            return users;
        }

        public ICollection<CustomerModel> GetCustomersByInspectorId(long priceInspectorId)
        {
            var data = new MarketQutationDetailData();
            return data.Get()
                .Where(x => x.PriceInspectorUserId == priceInspectorId)
                .GroupBy(x=>x.Customer)
                .Select(x => new CustomerModel()
                {
                    Id = x.Key.Id,
                    Name = x.Key.Name
                })
                .OrderBy(x=>x.Name)
                .ToList();
        }

        public ICollection<MarketQuotationModel> GetQuotationCodesBy(long priceInspectorId, long customerId)
        {
            var data = new MarketQutationDetailData();
            return data.Get()
                .WhereIf(priceInspectorId > 0, x => x.PriceInspectorUserId == priceInspectorId)
                .WhereIf(customerId > 0, x => x.CustomerId == customerId)
                .Select(x => new MarketQuotationModel()
                {
                    Id = x.Id,
                    QuotationCode = x.QuotationCode
                }).ToList();
        }

        public ICollection<MarketQuotationModel> GetSearchResult(long quotationId, long customerId, long inspectorId, DateTime? fromDate, DateTime? toDate)
        {
            ICollection<MarketQuotationModel> lists = null;
            lists = new MarketQuotationData().Get()
                .WhereIf(fromDate != null, x => x.DateOfCreation >= ((DateTime)fromDate).Date)
                .WhereIf(toDate != null, x => x.DateOfCreation <= ((DateTime)toDate).Date)
                .WhereIf(quotationId > 0 , x => x.Id == quotationId)
                .WhereIf(customerId > 0, x => x.CustomerId == customerId)
                .WhereIf(inspectorId > 0, x => x.PriceInspectorUserId == inspectorId)
                .Select(x => new MarketQuotationModel()
                {
                    DateOfCreation = x.DateOfCreation,
                    QuotationCode = x.QuotationCode,
                    Customer = x.Customer.Name,
                    PriceInspector = x.PriceInspectorUser.LoginName,
                    Inspections = x.Inspections
                        .Select(p => new MarketQuotationInspectionModel()
                        {
                            SupplierName = p.Supplier.Name,
                            ProductName = p.Product.Name,
                            ProductUnit = p.Product.Unit.Name,
                            ProductQuantity = p.Quantity,
                            ProductPrice = p.Price                           
                        })
                }).ToList();

            return lists;
        }

        public ICollection<MarketQuotationModel> GetAll()
        {
            ICollection<MarketQuotationModel> lists = null;

            lists = new MarketQuotationData().Get()
                .Select(x => new MarketQuotationModel()
                {
                    DateOfCreation = x.DateOfCreation,
                    QuotationCode = x.QuotationCode,
                    Customer = x.Customer.Name,
                    PriceInspector = x.PriceInspectorUser.LoginName,
                    Inspections = x.Inspections
                        .Select(p => new MarketQuotationInspectionModel()
                        {
                            SupplierName = p.Supplier.Name,
                            ProductName = p.Product.Name,
                            ProductUnit = p.Product.Unit.Name,
                            ProductQuantity = p.Quantity,
                            ProductPrice = p.Price
                        })
                }).ToList();

            return lists;
        }
    }
}
