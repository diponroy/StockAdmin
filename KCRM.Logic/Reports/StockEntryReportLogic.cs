﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using KCRM.Data.Tables;
using KCRM.Data.Tables.Mimics;
using KCRM.Data.Views;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models;

namespace KCRM.Logic.Reports
{
    public class StockEntryReportLogic
    {
        public IList<StockModel> GetStockEntry(DateTime? fromDate, DateTime? toDate, int productCategoryId,long productId)
        {
            return new StockDetailData().Get()
                .WhereIf(fromDate != null, x => x.DateOfCreation >= ((DateTime)fromDate).Date)
                .WhereIf(toDate != null, x => x.DateOfCreation <= ((DateTime)toDate).Date)
                .WhereIf(productCategoryId > 0, x => x.Product.Category == (ProductCategoryEnum)productCategoryId)
                .WhereIf(productId > 0, x => x.ProductId == productId)
                .Select(x => new StockModel()
                {
                    Quantity = x.Quantity,
                    DateOfCreation = x.DateOfCreation,
                    Product = new ProductModel()
                    {
                        Name = x.Product.Name,
                        Description = x.Product.Description
                    }
                })
                .ToList();

        }
        public List<ProductCategory> GetProductCategories()
        {
            return new ProductCategoryData().GetAll().ToList();
        }
        public ICollection<ProductModel> GetAllProducts()
        {
            ProductData data = new ProductData();
            return data.Get()
                        .Select(x => new ProductModel()
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Category = x.Category,
                            CategoryName = x.Category.ToString(),
                            UnitName = x.Unit.Name,
                            Description = x.Description,
                            DateOfCreation = x.DateOfCreation
                        })
                        .OrderBy(x=>x.Name)
                        .ToList();
        }
    }
}
