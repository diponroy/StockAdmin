﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity;
using System.Text;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;


namespace KCRM.Logic.Reports
{
    public class SalesReportLogic
    {
        public ICollection<CustomerModel> GetCustomer()
        {
            var data = new CustomerData();
            return data.Get().Select(x => new CustomerModel() { Id = x.Id, Name = x.Name }).ToList();
        }

        public ICollection<SaleModel> GetSales(DateTime? fromDate, DateTime? toDate, long customerId)
        {
            var list = new SalesDetailData().Get()
                .WhereIf(fromDate != null, x => x.DateOfCreation >= ((DateTime) fromDate).Date)
                .WhereIf(toDate != null, x => x.DateOfCreation <= ((DateTime) toDate).Date)
                .WhereIf(customerId > 0, x => x.CustomerId == customerId)
                .Select(x => new SaleModel()
                {
                    CustomerId = x.CustomerId,
                    DateOfCreation = x.DateOfCreation,
                    Customer = new CustomerModel()
                    {
                        Name = x.Customer.Name
                    },
                    SoldProducts = x.SoldProducts.Select(y => new SoldProductModel()
                    {
                        HasStockConsumption = y.HasStockConsumption,
                        Quantity = y.Quantity,
                        Price = y.Price,
                        Product = new ProductModel()
                        {
                            Name = y.Product.Name,
                            UnitName = y.Product.Unit.Name
                        },
                        StockConsumptions = y.StockConsumptions.Select(s => new StockConsumptionModel()
                        {
                            Quantity = s.Quantity
                        }).ToList()
                        
                    }).ToList()
                }).ToList();

            return list;
        }
    }
}
