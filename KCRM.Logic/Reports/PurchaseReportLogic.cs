﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic.Reports
{
    public class PurchaseReportLogic
    {
        public ICollection<PurchaseModel> GetAll()
        {
            ICollection<PurchaseModel> lists;

            lists = new PurchaseData().Get()
                .Select(x => new PurchaseModel()
                {
                    DateOfCreation = x.DateOfCreation,
                    Supplier = x.Supplier.Name,
                    Customer = x.Customer.Name,
                    Purchaser = x.Adder.LoginName,
                    Products = x.PurchasedProductList
                                .Select(p => new ProductModel()
                                {
                                    Id = p.ProductId,
                                    Name = p.Product.Name,
                                    UnitName = p.Product.Unit.Name,
                                    Quantity = p.Quantity,
                                    Price = p.Price
                                })
                }).ToList();

            return lists;
        }

        public ICollection<CustomerModel> GetCustomers()
        {
            CustomerData data = new CustomerData();
            return data.Get().Select(x => new CustomerModel() { Id = x.Id, Name = x.Name }).ToList();
        }

        public ICollection<Supplier> GetSuppliers()
        {
            return new SupplierData().GetAll();
        }

        public ICollection<UserModel> GetUsers()
        {
            var data = new UserData();
            List<UserModel> users
                = data.GetNotRemoved().Select(x => new UserModel()
                {
                    Id = x.Id,
                    LoginName = x.LoginName,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                })
                .OrderBy(x=>x.LoginName)
                .ToList();
            return users;
        }

        public ICollection<PurchaseModel> GetSearchResult(long supplierId, long customerId, long purchaseByUserId, DateTime? fromDate, DateTime? toDate)
        {
            ICollection<PurchaseModel> lists = null;
            lists = new PurchaseData().Get()
                .WhereIf(fromDate != null, x => x.DateOfCreation >= ((DateTime)fromDate).Date)
                .WhereIf(toDate != null, x => x.DateOfCreation <= ((DateTime)toDate).Date)
                .WhereIf(supplierId > 0, x => x.SupplierId == supplierId)
                .WhereIf(customerId > 0, x => x.CustomerId == customerId)
                .WhereIf(purchaseByUserId > 0, x => x.PurchaseByUserId == purchaseByUserId)
                .Select(x => new PurchaseModel()
                {
                    DateOfCreation = x.DateOfCreation,
                    Supplier = x.Supplier.Name,
                    Customer = x.Customer.Name,
                    Purchaser = x.Purchaser.LoginName,
                    Products = x.PurchasedProductList
                        .Select(p => new ProductModel()
                        {
                            Id = p.ProductId,
                            Name = p.Product.Name,
                            UnitName = p.Product.Unit.Name,
                            Quantity = p.Quantity,
                            Price = p.Price
                        })
                }).ToList();
            return lists;
        }

        public ICollection<CustomerModel> GetCustomersBySupplierId(long supplierId)
        {
            var data = new PurchaseDetailData();
            return data.Get()
                .Where(x => x.SupplierId == supplierId)
                .GroupBy(x => x.Customer)
                .Select(x => new CustomerModel()
                {
                    Id = x.Key.Id,
                    Name = x.Key.Name
                })
                .OrderBy(x=>x.Name)
                .ToList();
        }

        public ICollection<UserModel> GetPurchaserByCustomerAndSupplier(long customerId, long supplierId)
        {
            var data = new PurchaseDetailData();
            return data.Get()
                .WhereIf(customerId > 0, x => x.CustomerId == customerId)
                .WhereIf(supplierId > 0, x => x.SupplierId == supplierId)
                .GroupBy(x => x.Purchaser)
                .Select(x => new UserModel()
                {
                    Id = x.Key.Id,
                    LoginName = x.Key.LoginName,
                    FirstName = x.Key.FirstName,
                    LastName = x.Key.LastName,
                })
                .OrderBy(x=>x.LoginName)
                .ToList();
        }
    }
}
