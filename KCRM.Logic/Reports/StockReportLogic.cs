﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Tables.Mimics;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.Models;
using KCRM.Model.DbModels.Views;
using KCRM.Data.Views;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables.Enums;

namespace KCRM.Logic.Reports
{
    public class StockReportLogic
    {
        public List<ProductCategory> GetProductCategories()
        {
            return new ProductCategoryData().GetAll().ToList();
        }
        public ICollection<ProductModel> GetProducts()
        {
            ProductData data=new ProductData();
            return data.Get()
                        .Select(x => new ProductModel()
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Category = x.Category,
                            CategoryName = x.Category.ToString(),
                            UnitName = x.Unit.Name,
                            Description = x.Description,
                            DateOfCreation = x.DateOfCreation
                        })
                        .OrderBy(x=>x.Name)
                        .ToList();
        }
        public ICollection<CurrentStock> GetCurrentStocks(int productCategory, long productId)
        {
            return new CurrentStockData().Get()
                .AsQueryable()
                .WhereIf(productCategory > 0, x => x.Category == (ProductCategoryEnum)productCategory)
                .WhereIf(productId > 0, x => x.ProductId == productId)
                .ToList();
        }
    }
}
