﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Logic.Utilities;
using KCRM.Model.Models;
using KCRM.Model.Models.Reports;

namespace KCRM.Logic.Reports
{
    public class ClientQuotationReportLogic
    {
        public ICollection<CustomerModel> GetCustomers()
        {
            CustomerData data = new CustomerData();
            return data.Get().Select(x => new CustomerModel() { Id = x.Id, Name = x.Name }).ToList();
        }

        public ICollection<CustomerContactPersonModel> GetContactPersons(long id)
        {
            CustomerContactPersonData data = new CustomerContactPersonData();
            return
                data.GetNotRemovedByCustomerId(id)
                    .Select(x => new CustomerContactPersonModel() { Id = x.Id, Name = x.Name, ContactNumber = x.ContactNumber })
                    .OrderBy(x=>x.Name)
                    .ToList();
        }

        public ICollection<UserModel> GetPriceInspectorByCustomer(long customerId)
        {
            MarketQuotationData data = new MarketQuotationData();            
            var list = data.GetByCustomerId(customerId)
                    .GroupBy(x => x.PriceInspectorUser)
                    .Select(y => new UserModel()
                    {
                        Id = y.Key.Id, 
                        LoginName = y.Key.LoginName,
                        FirstName = y.Key.FirstName,
                        LastName = y.Key.LastName
                    })
                    .OrderBy(x=>x.LoginName)
                    .ToList();
            return list;
        }

        public ICollection<MarketQuotationModel> GetMarketQuotations(long customerId, long contactPersonId, long priceInspectorId)
        {
            var list = new ClientQutationDetailData().Get()
                .WhereIf(customerId > 0, x => x.CustomerId == customerId)
                .WhereIf(contactPersonId > 0, x => x.CustomerContactPersonId == contactPersonId)
                .WhereIf(priceInspectorId > 0, x => x.MarketQuotation.PriceInspectorUserId == priceInspectorId)
                .GroupBy(x => x.MarketQuotation)
                .Select(y => new MarketQuotationModel()
                {
                    Id = y.Key.Id,
                    QuotationCode = y.Key.QuotationCode
                }).ToList();
            return list;
        }

        public ClientQutationReportModel GetClientQuotations(ClientQutationReportModel model)
        {
            var data = new ClientQutationDetailData();
            model.ClientQuotations = data.Get()
                .WhereIf(model.FromDate != null, x => x.DateOfCreation >= ((DateTime)model.FromDate).Date)
                .WhereIf(model.ToDate != null, x => x.DateOfCreation <= ((DateTime)model.ToDate).Date)
                .WhereIf(model.CustomerId > 0, x => x.CustomerId == model.CustomerId)
                .WhereIf(model.ContactPersonId > 0, x => x.CustomerContactPersonId == model.ContactPersonId)
                .WhereIf(model.PriceInspectorId > 0, x => x.MarketQuotation.PriceInspectorUserId == model.PriceInspectorId)
                .WhereIf(model.MarketQuotationId > 0, x => x.MarketQuotation.Id == model.MarketQuotationId)
                .Select(x => new ClientQuotationModel()
                {
                    DateOfCreation = x.DateOfCreation,
                    MarketQuotation = new MarketQuotationModel()
                    {
                        QuotationCode = x.MarketQuotation.QuotationCode,
                        DateOfCreation = x.MarketQuotation.DateOfCreation,
                        PriceInspectorUser = new UserModel()
                        {
                            FirstName = x.MarketQuotation.PriceInspectorUser.FirstName,
                            LastName = x.MarketQuotation.PriceInspectorUser.LastName,
                            LoginName = x.MarketQuotation.PriceInspectorUser.LoginName,
                            FullName = x.MarketQuotation.PriceInspectorUser.FirstName +" " +x.MarketQuotation.PriceInspectorUser.LastName,
                            FullNameWithLoginName = x.MarketQuotation.PriceInspectorUser.FirstName + " " + x.MarketQuotation.PriceInspectorUser.LastName + "(" + x.MarketQuotation.PriceInspectorUser.LoginName + ")"
                        }
                    },
                    Customer = new CustomerModel()
                    {
                        Name = x.Customer.Name,
                    },
                    CustomerContactPerson = new CustomerContactPersonModel()
                    {
                        Name = x.CustomerContactPerson.Name
                    },
                    ClientQuotationOrders = x.ClientQuotationOrders.Select(y => new ClientQuotationOrderModel()
                    {
                        Quantity = y.Quantity,
                        Vat = y.Vat,
                        Price = y.Price,
                        Total = y.Vat + y.Price,
                        Product = new ProductModel()
                        {
                            Name = y.Product.Name,
                            UnitName = y.Product.Unit.Name
                        }
                    }).ToList()
                }).ToList();
            return model;
        }

        public ClientQutationReportModel GetClientQuotationsById(ClientQutationReportModel model)
        {
            var data = new ClientQutationDetailData();
            model.ClientQuotation = data.Get()
                .Where(x => x.MarketQuotation.Id == model.MarketQuotationId)
                .Select(x => new ClientQuotationModel()
                {
                    DateOfCreation = x.DateOfCreation,
                    MarketQuotation = new MarketQuotationModel()
                    {
                        QuotationCode = x.MarketQuotation.QuotationCode,
                        DateOfCreation = x.MarketQuotation.DateOfCreation,
                        PriceInspectorUser = new UserModel()
                        {
                            FirstName = x.MarketQuotation.PriceInspectorUser.FirstName,
                            LastName = x.MarketQuotation.PriceInspectorUser.LastName,
                            LoginName = x.MarketQuotation.PriceInspectorUser.LoginName,
                            FullName = x.MarketQuotation.PriceInspectorUser.FirstName + " " + x.MarketQuotation.PriceInspectorUser.LastName,
                            FullNameWithLoginName = x.MarketQuotation.PriceInspectorUser.FirstName + " " + x.MarketQuotation.PriceInspectorUser.LastName + "(" + x.MarketQuotation.PriceInspectorUser.LoginName + ")"
                        }
                    },
                    Customer = new CustomerModel()
                    {
                        Name = x.Customer.Name,
                        Address = x.Customer.Address
                    },
                    CustomerContactPerson = new CustomerContactPersonModel()
                    {
                        Name = x.CustomerContactPerson.Name
                    },
                    ClientQuotationOrders = x.ClientQuotationOrders.Select(y => new ClientQuotationOrderModel()
                    {
                        Quantity = y.Quantity,
                        Vat = y.Vat,
                        Price = y.Price,
                        Total = y.Vat + y.Price,
                        Product = new ProductModel()
                        {
                            Name = y.Product.Name,
                            UnitName = y.Product.Unit.Name,
                            Description = y.Product.Description
                        }
                    }).ToList()
                }).Single();
            return model;
        }
    }
}
