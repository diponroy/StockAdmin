﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.Models;


namespace KCRM.Logic
{
    public class LoginManageLogic
    {
        private readonly UserData _userData;

        public LoginManageLogic()
        {
            _userData = new UserData();
        }

        public bool IsAuthorizedToLogin(string loginName, string password)
        {
            password = PasswordUtility.Encode(password);
            var user = _userData.FindNotDeleted(loginName, password);
            return (user != null) ? true : false;
        }

        public UserSessionModel GetUser(string loginName, string password)
        {
            password = PasswordUtility.Encode(password);
            User user = _userData.FindNotDeleted(loginName, password);
            UserSessionModel model = new UserSessionModel()
            {
                Id = user.Id,
                LoginName = user.LoginName,
                UserType = user.UserType,
            };
            return model;
        }

        public List<PermissionEmun> GetPermissions(long id, UserTypeEnum userType)
        {
            List<PermissionEmun> permissions = new List<PermissionEmun>();
            if (userType == UserTypeEnum.Administrator)
            {
                permissions = Enum.GetValues(typeof (PermissionEmun)).OfType<PermissionEmun>().ToList();
            }
            else
            {
                permissions = new UserPermissionData().GetNotRemoved(id).Select(x => x.Permission).ToList();
            }
            return permissions;
        }

        public bool TryToRecover(string loginName, string email)
        {
            bool recovered = false;
            var user = _userData.FindNotDeletedByLonginNameAndEmail(loginName, email);
            if (user == null)
            {
                return false;
            }

            using (var dbContextTransaction = _userData.DbContext.Database.BeginTransaction())
            {
                try
                {
                    /*update password*/
                    var newPassword = PasswordUtility.GenerateRandomPassword(6);
                    user.Password = PasswordUtility.Encode(newPassword);
                    user.ReplacedByUserId = user.Id;
                    user.ReplacedDateTime = DateTime.Now;
                    _userData.SaveChanges();

                    /*send email*/
                    MailMessage mailMessage = new MailMessage();
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = "Password Changed";
                    mailMessage.Body = MessageBodyForLogIn(user, newPassword);
                    mailMessage.To.Add(email);
                    SmtpClient smtpClient = new SmtpClient();
                    smtpClient.Send(mailMessage);

                    dbContextTransaction.Commit();
                    recovered = true;
                }
                catch (Exception exception)
                {
                    dbContextTransaction.Rollback();
                    throw new Exception("Password recovered failed", exception);
                }
            }
            return recovered;
        }

        public string MessageBodyForLogIn(User aUser, string password)
        {
            string msg = "<html>";
            msg += "<head>";
            msg += "<title></title>";
            msg += "</head>";
            msg += "<body>";
            msg += "Dear " + aUser.FirstName + " " + aUser.LastName + ",";
            msg += "<br />";
            msg += "<br />";

            msg += "You password is been changed <br>";
            msg += "You Login name  " + "<b>" + aUser.LoginName + "</b>";
            msg += " and the new password is <b>" + password + "</b>";
            msg += "<br />";

            //string logInLink = ConfigurationManager.AppSettings["longInLink"];
            //msg += "<a href='" + logInLink + "'>" + logInLink + "</a>";
            //msg += "<br />";
            msg += "<br />";

            msg += "Regards,";
            msg += "<br />";
            msg += "Kcrm Addmin";
            msg += "<br />";

            msg += "This is computer generated mail. Please do not reply.";
            msg += "</body>";
            msg += "</html>";
            return msg;
        }
    }
}
