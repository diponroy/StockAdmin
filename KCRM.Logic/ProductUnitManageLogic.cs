﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Logic
{
    public class ProductUnitManageLogic
    {
        private readonly ProductUnitData _data;

        public ProductUnitManageLogic() 
        {
            _data = new ProductUnitData();
        }

        public ProductUnit Get(long id)
        {
            var productUnit = _data.Get(id);

            return new ProductUnit()
            {
                Name = productUnit.Name,
                Description = productUnit.Description,
                AddedDateTime = productUnit.AddedDateTime,
                AddedByUserId = productUnit.AddedByUserId,
                ReplacedDateTime = productUnit.ReplacedDateTime,
                ReplacedByUserId = productUnit.ReplacedByUserId
            };
        }

        public ICollection<ProductUnit> GetAll()
        {
            return _data.GetAll();
        }

        public bool TryToAdd(ProductUnit productUnit)
        {
            if (IsValidToAdd(productUnit))
            {
                _data.Add(productUnit);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidToAdd(ProductUnit productUnit)
        {
            bool validToAdd = !NameUsed(productUnit.Name);
            return validToAdd;
        }

        public bool NameUsed(string name)
        {
            bool isUsed = _data.AnyName(name);
            return isUsed;
        }

        public bool NameUsedExceptProductUnit(string name, long id)
        {
            bool isUsed = _data.AnyNameExceptProductUnit(name, id);
            return isUsed;
        }

        public bool TryToUpdate(ProductUnit productUnit)
        {
            if (IsValidToUpdate(productUnit))
            {
                _data.Replace(productUnit);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidToUpdate(ProductUnit productUnit)
        {
            bool validToUpdate = !NameUsedExceptProductUnit(productUnit.Name, productUnit.Id);
            return validToUpdate;
        }


        public bool TryToReplace(ProductUnit productUnit)
        {
            if (IsValidToReplace(productUnit))
            {
                _data.Replace(productUnit);
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsValidToReplace(ProductUnit productUnit)
        {
            bool validToUpdate = !NameUsedExceptProductUnit(productUnit.Name, productUnit.Id);
            return validToUpdate;
        }

        public void Remove(ProductUnit productUnit)
        {
            _data.Remove(productUnit);
        }
    }
}
