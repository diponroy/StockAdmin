﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class MarketQuotationManageLogic
    {
        private readonly MarketQuotationData _data;

        public MarketQuotationManageLogic() 
        {
            _data = new MarketQuotationData();
        }

        public ICollection<UserModel> GetUsers()
        {
            var data = new UserData();
            List<UserModel> users
                = data.GetNotRemoved().Select(x => new UserModel()
                {
                    Id = x.Id,
                    LoginName = x.LoginName,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                })
                .OrderBy(x=>x.LoginName)
                .ToList();
            return users;
        }

        public ICollection<CustomerModel> GetCustomers()
        {
            CustomerData data = new CustomerData();
            return data.Get().Select(x => new CustomerModel() { Id = x.Id, Name = x.Name }).ToList();
        }


        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return data.GetWithUnit()
                .Select(x => new ProductModel() {Id = x.Id, Name = x.Name, UnitName = x.Unit.Name})
                .OrderBy(x=> x.Name)
                .ToList();
        }

        public ICollection<Supplier> GetSuppliers()
        {
            return new SupplierData().GetAll();
        }

        public bool CodeUsed(string code)
        {
            bool isUsed = _data.AnyName(code);
            return isUsed;
        }

        public string GetNextCode()
        {
            return _data.GetNextCode();
        }

        public bool TryToCreate(MarketQuotation quotation, List<MarketQuotationInspection> inspections)
        {
            if (inspections == null || inspections.Count == 0)
            {
                throw new NullReferenceException("Inspections list is null");
            }
                
            /*Add only quotation*/
            _data.Add(quotation);
            _data.SaveChanges();

            /*Add quotation inspections*/
            inspections = inspections ?? new List<MarketQuotationInspection>();
            MarketQuotationInspectionData data = new MarketQuotationInspectionData(_data.DbContext);
            inspections.ForEach(obj =>
            {
                obj.MarketQuotationId = quotation.Id;
                data.Add(obj);
            });
            data.SaveChanges();

            return true;
        }
    }
}
