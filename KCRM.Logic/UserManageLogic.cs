﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Tables.Mimics;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class UserManageLogic
    {

        private readonly UserData _data;

        public UserManageLogic() 
        {
            _data = new UserData();
        }

        public User Get(long id)
        {
            var user =  _data.Get(id);
            return new User()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                LoginName = user.LoginName,
                Password = user.Password,
                Email = user.Email,
                ContactNumber = user.ContactNumber,
                UserType = user.UserType,
                Address = user.Address,
                DateOfCreation = user.DateOfCreation,
                IsRemoved = user.IsRemoved,
                AddedDateTime = user.AddedDateTime,
                AddedByUserId = user.AddedByUserId,
                ReplacedDateTime = user.ReplacedDateTime,
                ReplacedByUserId = user.ReplacedByUserId
            };
        }

        public ICollection<UserModel> GetAll()
        {
            var users = _data.GetNotRemoved()
                .OrderByDescending(x => x.AddedDateTime)
            .Select(x => new UserModel()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                LoginName = x.LoginName,
                Password = x.Password,
                Email = x.Email,
                ContactNumber = x.ContactNumber,
                UserType = x.UserType,
                UserTypeString = x.UserType.ToString(),
                Address = x.Address,
                DateOfCreation = x.DateOfCreation.ToString()
            }).ToList();

            return users;
        }

        public bool TryToAdd(User user)
        {
            if (!IsValidToAdd(user))
            {
                return false;
            }
            _data.Add(user);
            _data.SaveChanges();
            return true;
        }

        public bool IsValidToAdd(User user)
        {
            bool validToAdd = !LoginNameUsed(user.LoginName) && !EmailUsed(user.Email);
            user.Password = PasswordUtility.Encode(user.Password);
            return validToAdd;
        }

        public bool LoginNameUsed(string loginName)
        {
            bool isUsed = _data.AnyLoginName(loginName);
            return isUsed;
        }

        public bool EmailUsed(string email)
        {
            bool isUsed = _data.AnyEmail(email);
            return isUsed;
        }

        public bool TryToReplace(User user)
        {
            if (!IsValidToReplace(user))
            {
                return false;
            }
            user.Password = (!String.IsNullOrEmpty(user.Password)) ? PasswordUtility.Encode(user.Password) : user.Password;
            _data.Replace(user);
            _data.SaveChanges();
            return true;
        }

        public bool IsValidToReplace(User user)
        {
            bool validToUpdate = !LoginNameUsedExceptUser(user.LoginName, user.Id) && !EmailExceptUser(user.Email, user.Id);
            return validToUpdate;
        }

        public bool LoginNameUsedExceptUser(string loginName, long id)
        {
            bool isUsed = _data.AnyLoginNameExceptUser(loginName, id);
            return isUsed;
        }

        public bool EmailExceptUser(string email, long id)
        {
            bool isUsed = _data.AnyEmailExceptUser(email, id);
            return isUsed;
        }

        public void Remove(User user)
        {
            _data.RemoveById(user);
            _data.SaveChanges();
        }

        public List<UserType> GetUserTypes()
        {
            return new UserTypeData().GetAll().ToList();
        }

    }
}