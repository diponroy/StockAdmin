﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class MarketPriceManageLogic
    {
        private readonly MarketPriceListData _data;
        public MarketPriceManageLogic()
        {
            _data = new MarketPriceListData();
        }

        public bool Add(MarketPriceList marketPriceList)
        {
            _data.Add(marketPriceList);
            _data.SaveChanges();
            return true;
        }

        public bool Replace(MarketPriceList marketPriceList)
        {
            _data.Replace(marketPriceList);
            _data.SaveChanges();
            return true;
        }

        public bool TryToReplace(MarketPriceList marketPriceList)
        {
            if (_data.AnyByProductId(marketPriceList.ProductId))
            {
                marketPriceList.ReplacedByUserId = marketPriceList.AddedByUserId;
                marketPriceList.ReplacedDateTime = marketPriceList.AddedDateTime;
                Replace(marketPriceList);
            }
            else
            {
                Add(marketPriceList);
            }
            return true;
        }
        
        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return
                data.GetWithUnit()
                .Select(x => new ProductModel() { Id = x.Id, Name = x.Name, UnitName = x.Unit.Name })
                .ToList();
        }

        public ICollection<MarketPriceModel> GetMarketPrice()
        {
            var data = new ProductMarketPriceData();
            return
                data.Get()
                .Select(x => new MarketPriceModel() { Id = x.Id, Price = x.Price, ProductName = x.Name })
                .ToList();
        }
        public MarketPriceList GetPriceForUpdate(long productId)
        {
            var value = _data.FindByProductId(productId);
            return value ?? new MarketPriceList();
        }
    }
}
