﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using KCRM.Data.Tables;
using KCRM.Data.Views;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Views;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class SaleManageLogic
    {
        public ICollection<ProductModel> GetProducts()
        {
            var data = new ProductData();
            return
                data.GetWithUnit()
                .Select(x => new ProductModel() { Id = x.Id, Name = x.Name, UnitName = x.Unit.Name })
                .OrderBy(x=>x.Name)
                .ToList();
        }

        public ICollection<CustomerModel> GetCustomer()
        {
            var data = new CustomerData();
            return data.Get().Select(x => new CustomerModel() { Id = x.Id, Name = x.Name }).ToList();
        }

        public bool HasEnoughStock(long productId, long quantity)
        {
            return new CurrentStockData().AnyAvailableStock(productId, quantity);
        }

        public bool TryToCreate(Sale sale, List<SoldProducts> products)
        {
            if (products == null || products.Count == 0)
            {
                throw new NullReferenceException("Products list is null");
            }

            products = products ?? new List<SoldProducts>();

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var saleData = new SaleData();
                saleData.Add(sale);
                saleData.SaveChanges();

                var soldProductData = new SoldProductsData(saleData.DbContext);
                var stockConsumtionData = new StockConsumptionData(saleData.DbContext);
                foreach (var aProduct in products)
                {
                    var stockConsumptions = aProduct.StockConsumptions ?? new List<StockConsumptions>();
                    aProduct.SaleId = sale.Id;
                    aProduct.StockConsumptions = null;

                    /*Add sold products*/
                    soldProductData.Add(aProduct);
                    soldProductData.SaveChanges();

                    foreach (var consumption in stockConsumptions)
                    {
                        if (!HasEnoughStock(aProduct.ProductId, consumption.Quantity))
                        {
                            throw new Exception("no enought stock.");
                        }

                        /*Add product consumption*/
                        consumption.SoldProductId = aProduct.Id;
                        stockConsumtionData.Add(consumption);
                        stockConsumtionData.SaveChanges();
                    }
                }
                scope.Complete();
                return true;
            }
        }
    }
}
