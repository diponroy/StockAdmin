﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KCRM.Data.Tables;
using KCRM.Logic.Utilities;
using KCRM.Model.DbModels.Tables;

namespace KCRM.Logic
{
    public class ProfileManageLogic
    {
        public bool TryToUpdate(User user)
        {
            var data = new UserData();
            var oldUser = data.Get(user.Id);
            oldUser.Password = PasswordUtility.Encode(user.Password);
            oldUser.ReplacedByUserId = user.ReplacedByUserId;
            oldUser.ReplacedDateTime = user.ReplacedDateTime;
            data.Replace(oldUser);
            data.SaveChanges();
            return true;
        }
    }
}
