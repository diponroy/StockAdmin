﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data;
using KCRM.Data.Tables;
using KCRM.Data.Tables.Mimics;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.DbModels.Tables.Enums;
using KCRM.Model.DbModels.Tables.Mimics;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class UserPermissionManageLogic
    {
        private UserPermissionData _data;
        public UserPermissionManageLogic()
        {
            _data = new UserPermissionData();
        }
        public ICollection<UserModel> GetUsers()
        {
            var data = new UserData();
            List<UserModel> users
                = data.GetNotRemodedNonAdministrators().Select(x => new UserModel()
                    {
                        Id = x.Id,
                        LoginName = x.LoginName,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                    }).ToList();
            return users;
        }
        public ICollection<Permission> GetNotRemovedUserPermissions(long userId)
        {
            var data = new UserPermissionData();
            var userPermissions
                = data.GetNotRemoved(userId)
                .Select(x => new Permission()
                {
                    PermissionEmun = x.Permission
                }).ToList();
            return userPermissions;
        }
        public ICollection<Permission> GetPermissions()
        {
            return new PermissionData().GetAll();
        }

        public bool UpdatePermission(UserPermissionModel model)
        {
            var permissions = model.Permissions ?? new List<int>();
            List<PermissionEmun> assignedPermissions = permissions.Select(x => (PermissionEmun)x).ToList();
            List<PermissionEmun> oldPermissions = new UserPermissionData().GetNotRemoved(model.UserId).Select(x => x.Permission).ToList();

            var addedPermissions = assignedPermissions.Except(oldPermissions).ToList();
            var removedPermissions = oldPermissions.Except(assignedPermissions).ToList();
            var userPermissionIdsToRemove = _data.GetNotRemoved(model.UserId).Where(x => removedPermissions.Contains(x.Permission)).Select(x => x.Id).ToList();

            addedPermissions.ForEach(obj =>
            {
                var userPermission = new UserPermission()
                {
                    UserId = model.UserId,
                    AddedByUserId = model.UpddatedByUserId,
                    AddedDateTime = model.UpdatedDateTime,
                    Permission = obj
                };
                _data.Add(userPermission);
            });

            userPermissionIdsToRemove.ForEach(obj =>
            {
                var userPermission = new UserPermission()
                {
                    Id = obj,
                    ReplacedByUserId = model.UpddatedByUserId,
                    ReplacedDateTime = model.UpdatedDateTime,
                };
                _data.Remove(userPermission);
            });

            _data.SaveChanges();
            return true;
        }
    }
}
