﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KCRM.Data.Tables;
using KCRM.Model.DbModels.Tables;
using KCRM.Model.Models;

namespace KCRM.Logic
{
    public class PurchaseManageLogic
    {
        private readonly PurchaseData _data;

        public PurchaseManageLogic() 
        {
            _data = new PurchaseData();
        }

        public ICollection<CustomerModel> GetCustomers()
        {
            CustomerData data = new CustomerData();
            return data.Get().Select(x => new CustomerModel() { Id = x.Id, Name = x.Name }).ToList();
        }


        public ICollection<ProductModel> GetProducts()
        {
            ProductData data = new ProductData();
            return data.GetWithUnit()
                .Select(x => new ProductModel() { Id = x.Id, Name = x.Name, UnitName = x.Unit.Name })
                .OrderBy(x=>x.Name)
                .ToList();
        }

        public ICollection<Supplier> GetSuppliers()
        {
            return new SupplierData().GetAll();
        }

        public bool TryToCreate(Purchase purchase, List<PurchasedProducts> purchasedProducts)
        {
            if (purchasedProducts == null || purchasedProducts.Count == 0)
            {
                throw new NullReferenceException("Purchased Products list is null");
            }

            _data.Add(purchase);
            _data.SaveChanges();

            purchasedProducts = purchasedProducts ?? new List<PurchasedProducts>();
            PurchasedProductsData data = new PurchasedProductsData(_data.DbContext);
            purchasedProducts.ForEach(obj =>
            {
                obj.PurchaseId = purchase.Id;
                data.Add(obj);
            });
            data.SaveChanges();

            return true;
        }

        public ICollection<UserModel> GetUsers()
        {
            var data = new UserData();
            List<UserModel> users
                = data.GetNotRemoved().Select(x => new UserModel()
                {
                    Id = x.Id,
                    LoginName = x.LoginName,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                })
                .OrderBy(x=>x.LoginName)
                .ToList();
            return users;
        }
    }
}
